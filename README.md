# Libro.studio - Hybrid App

Technologies:

- Angular 4.x
- Typescript 2.3.x
- Webpack 3.10.x
- Cordova 7.0.1
# Init project
1. Clean your project with <code>git clean -dfx</code>
2. Execute <code>node changeConfig.js <ftd|alfacon|libro> (version)</code>
3. Ex: <code>node changeConfig.js <ftd|alfacon|libro> 2.0.13</code>
4. <code>npm install</code> in desktop folder 
5. Build the project in root project folder with <code>webpack --watch</code>. 

Obs.: To build the project to another client, execute <code>npm run reinit <client_name></code>

Obs2.: THE PROJECT WILL GO <b>XABLAU</b> IF U DONT DO THAT.

# Help me mister, i would love to help you too :B

- https://cordova.apache.org/docs/en/latest/
- https://angular.io/guide/architecture
- https://www.typescriptlang.org/docs/handbook/basic-types.html
- https://github.com/torokmark/design_patterns_in_typescript

# Getting started

- Be sure that you already have installed the priority packages to make this application runs 
- cordova ([sudo] npm install -g cordova)
- angular-cli ([sudo] npm install -g angular-cli)

"[sudo] npm install -g angular-cli cordova" (you can use this command to install both)

- Open your terminal... (windows? this tutorial don't support windows)
- mkdir Led2 && cd Led2 && git init
- git remote add (oficial/origin/bitbucket/love) {{YOURUSER}}@bitbucket.org:librostudio/led2.git
- git fetch remote
- git checkout -b develop {{REMOTE}}/develop

# Install all packages
- npm install

# Run grunt to make copy of yours environments.ts.example and wait yours ng build
- grunt

# Prepare to build
- webpack --watch

# Make yours first build
- npm run start:electron (DEKSTOP)

- cordova prepare (MOBILE)
- cordova run android (ANDROID)
- cordova run ios (IOS)


* Now, wait grunt be trigged by your build and execute cordova priority commands (MOBILE)
