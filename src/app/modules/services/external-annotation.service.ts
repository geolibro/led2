import { Injectable, Inject } from '@angular/core';
import { Ws021Request } from '../../requests/ws021.request';
import { BookService } from './book.service';
import { Ws021Contract } from '../../requests/contracts/ws/ws021.contract';
import { environment } from '../../../environments/environment';
import { WS021ResponseContract } from '../../requests/contracts/ws/ws021-response.contract';
import { LogProvider } from '../../providers/log.provider';
import { CallerProvider } from '../../providers/caller.provider';
import { JsonObject } from '../../providers/json-object.provider';
import { AuthGuardService } from './guard/auth-guard.service';
import { LedFileSystemContract } from './contracts/file-system/led-file-system.contract';
import { NetworkContract } from './contracts/network/network.contract';
import { PlatformContract } from './contracts/platform/platform.contract';

export class ExternalAnnotationService {
    private annotationCountBySummaryTopic: any = [];
    private summaryItems: any;
    private readonly SUMMARY_ITEM_END_MARKER = '@@';
    private readonly APP_TOKEN = '0c6aaaa87e47b8ef2e67b7ffd3eee75b';
    private readonly EXTERNAL_APP_TOKEN = '15fbf1555b049b06f6823a3f909fbbb8';
    private pageIdVsNumberMap: any = [];
    private externalNotesFilePath: string;
    private Log: LogProvider = LogProvider.getInstance('ExternalAnnotationService');

    constructor(
        private bookInstalledManifest: any,
        private bookId: string,
        private bookService: BookService,
        private caller: CallerProvider,
        private network: NetworkContract,
        private fileSystem: LedFileSystemContract,
        private auth?: AuthGuardService
    ) {
        this.externalNotesFilePath = this.bookId + '-external-notes.json';

        for (let i = 1; i < 1000; i++) {
            this.pageIdVsNumberMap[i] = i;
        }

        this.loadTopicData();
    }

    private loadTopicData() {
        // TODO: check expected data structure compatibility
        this.summaryItems = this.getChaptersAsList(
            this.bookInstalledManifest.chapters
        );

        // TODO: load sync JSON from storage
        this.fileSystem.request(this.externalNotesFilePath)
            .readFile()
            .toJSON(syncJSON => {
                let annotationsByTopicJSON = syncJSON['quantity_array'];
                this.setNotesData(annotationsByTopicJSON);
            }, error => {
                //
            });
    }

    private getChaptersAsList(chapters: any) {
        let out = [];
        toArrayList.call(chapters);

        function toArrayList() {
            out.push(this);

            if (this.children != null) {
                for (let child of this.children) {
                    toArrayList.call(child);
                }
            }
        }
        return out;
    }

    private setNotesData(annotationsByTopic: any[]) {
        this.annotationCountBySummaryTopic = {};
        let count = annotationsByTopic.length;
        for (let i = 0; i < count; i++) {
            let info = annotationsByTopic[i];

            let code: string = info.qr_code;

            if (code.length > 0) {
                if (code.endsWith('.json')) {
                    code = code.substring(0, code.length - 5);
                }

                let quantity = info['quantity'];
                this.annotationCountBySummaryTopic[code] = quantity;
            }
        }
        // refresh UI when this happens
    }

    public pageHasAssociatedTopic(pageNumber: number): boolean {
        let pageTopics = this.getTopicsForPage(pageNumber);
        return pageTopics.length > 0;
    }

    getTopicsForPage(pageNumber: number): string[] {
        let topics = [];

        let lastAddedItemPageNumber = -1;
        let summaryItemCount = this.summaryItems.length;

        for (let i = summaryItemCount - 1; i >= 0; i--) {
            let summaryItem = this.summaryItems[i];

            let summaryItemPage = this.pageIdVsNumberMap[
                summaryItem.chapter_first_page_id
            ];
            if (pageNumber >= summaryItemPage) {
                topics.unshift(summaryItem.chapter_name);

                lastAddedItemPageNumber = summaryItemPage;

                while (
                    --i >= 0 &&
                    this.pageIdVsNumberMap[this.summaryItems[i].chapter_first_page_id] ===
                    lastAddedItemPageNumber
                ) {
                    topics.unshift(this.summaryItems[i].chapter_name);
                }

                break;
            }
        }

        return topics;
    }

    onGetExternalAppSyncData(syncData: string) {
        let annotationsByTopicJSON;
        let syncJSON;

        syncJSON = JSON.parse(syncData);
        annotationsByTopicJSON = syncJSON['quantity_array'];

        if (annotationsByTopicJSON != null) {
            let appToken: string = syncJSON['app_token'];

            if (
                appToken === '' ||
                appToken === undefined ||
                this.EXTERNAL_APP_TOKEN === appToken
            ) {

                this.fileSystem
                    .request(this.externalNotesFilePath)
                    .writeFile(JSON.stringify(syncJSON), () => { }, error => {
                        let errorStr = 'Erro gravando anotações externas!:\n' + JSON.stringify(error);
                        alert(errorStr);
                    });

                this.setNotesData(annotationsByTopicJSON);
            }
        }
    }

    openAnnotationOnExternalApp(topicName: string) {
        let topicId = this.getTopicId(topicName);

        this.callExternalApp('createNote', topicId);
    }

    openAllAnnotationsForBook() {
        let summaryCount = this.summaryItems.length;

        if (summaryCount > 0) {
            // any book identifier is valid for this operation, so we use the first
            let firstTopicId = '';
            for (let i = 0; i < summaryCount; i++) {
                let firstBookItem = this.summaryItems[i];
                if (firstBookItem.chapter_name) {
                    firstTopicId = this.getTopicId(firstBookItem.chapter_name);
                }

                if (firstTopicId != null && firstTopicId !== '') {
                    break;
                }
            }

            this.callExternalApp('showProduct', firstTopicId);
        }
    }

    getAnnotationsCountForPage(pageNumber: number): number {
        let count = 0;

        let pageTopics = this.getTopicsForPage(pageNumber);

        for (let topic of pageTopics) {
            let topicId = this.getTopicId(topic);
            let value = this.annotationCountBySummaryTopic[topicId];
            if (value !== undefined) {
                count += value;
            }
        }

        return count;
    }

    private getTopicId(summaryItemText: string): string {
        let itemIdentifier: string;
        let separatorIndex = summaryItemText.lastIndexOf(
            this.SUMMARY_ITEM_END_MARKER
        );

        if (separatorIndex != -1) {
            itemIdentifier = summaryItemText.substring(separatorIndex + 2);
            itemIdentifier = itemIdentifier.replace('[/b]', '');
        }
        return itemIdentifier;
    }

    /**
     * Método responsável por relizar chamada a uma activity externa
     * @param actionType
     * @param topicId
     */
    private callExternalApp(actionType: string, topicId: string) {
        const JSONObject: JsonObject = new JsonObject(
            {
                qrcode: topicId,
                app_token: environment.app_token,
                user_app_token: this.auth.user().token
            },
            'data'
        );

        this.caller
            .call(actionType, JSONObject)
            .then(() => { })
            .catch(error => {
                let errorStr = 'Erro ao chamar leitor externo de aplicações: \n' + JSON.stringify(error);
                alert(errorStr);
            });
    }

    syncWebService(platform?: PlatformContract) {
        const JSONData: Ws021Contract = {
            operation: 'J375',
            app_token: environment.app_token,
            user_app_token: this.auth.user().token,
            book_publisher_id: this.bookId
        };

        if (this.network.isConnected()) {
            const ws021Request: Ws021Request = new Ws021Request(
                JSONData,
                this.bookService
            );

            ws021Request
                .getHttpRequest()
                .then((response: WS021ResponseContract) => {
                    this.onGetExternalAppSyncData(JSON.stringify(response));
                })
                .catch((error: any) => {
                    this.Log.save(error);
                });
        }
    }
}
