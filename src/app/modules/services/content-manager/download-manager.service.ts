import { Injectable, Inject } from '@angular/core';
import { InstallerService } from './installer.service';
import { AppEvent } from '../../events/app.event';
import { DownloadableObject } from "./downloadable-object";
import { BookDao } from '../../daos/book.dao';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { DownloadContract } from '../contracts/download/download.contract';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
import { DeviceContract } from '../contracts/device/device.contract';
import { VaultToolsService } from '../vault/vault-tools.service';
import { PlatformEnum } from '../contracts/platform/platform.enum';

export enum DownloadStopReason {
    NO_DISK_SPACE,
    NO_CONNECTION,
    PAUSED
}

@Injectable()
export class DownloadManagerService {
    public static BOOK_SALT: string = "_DEV_DACCORD_PLURI";
    private downloadPriorityQueue: Array<DownloadableObject> = [];

    private isDownloading: boolean = false;
    private bookKeys: any;
    private booksToDownload: any;
    private lastSpaceRequestTime: number;
    private queuePollingTimerId: number;
    private diskSpacePollingTimerId: number;

    constructor(@Inject('PlatformService') private platform: PlatformContract,
        private installer: InstallerService,
        private appEvent: AppEvent,
        @Inject('DownloadService') private downloadService: DownloadContract,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        @Inject('DeviceService') private device: DeviceContract,
        private book: BookDao) {
        this.bookKeys = {};
        this.booksToDownload = {};
        this.lastSpaceRequestTime = 0;

    }

    private startDownloadPolling() {

        if (this.platform.is(PlatformEnum.DESKTOP)) this.startQueueProcess();
        else if (this.platform.is(PlatformEnum.MOBILE)) this.platform.ready().then(() => this.startQueueProcess());
    }

    private startQueueProcess() {
        this.queuePollingTimerId = window.setInterval(() => {
            this.processDownloadQueue();
        }, 200);

        this.diskSpacePollingTimerId = window.setInterval(() => {
            this.installer.updateDiskSpaceInfo();
        }, 180000);
    }

    private stopDownloadPolling() {
        // if there's nothing on download queue, there's no reason for polling
        window.clearInterval(this.queuePollingTimerId);
        window.clearInterval(this.diskSpacePollingTimerId);

        this.queuePollingTimerId = null;
        this.diskSpacePollingTimerId = null;
    }

    addBookObjectsToDownload(bookId: string, cdnURLs: string[], bookObjects: DownloadableObject[]) {

        // serão adicionados apenas aqueles objetos que não estejam já na fila de download
        bookObjects = bookObjects.filter((obj, index) => {
            return (this.downloadPriorityQueue.indexOf(obj) == -1);
        });

        this.booksToDownload[bookId] = {
            cdnURLs: cdnURLs,
            objects: bookObjects
        };

        this.downloadPriorityQueue = this.downloadPriorityQueue.concat(bookObjects);

        this.prioritize(bookId, 1);

        this.startDownloadPolling();

    }

    public prioritize(bookId: string, pageNumber: number) {

        this.downloadPriorityQueue = this.downloadPriorityQueue.sort((obj1, obj2) => {

            let n1 = obj1.page_number;
            let n2 = obj2.page_number;

            if (obj1.book_id == bookId) {
                if (obj1.page_number == pageNumber) {
                    n1 = Number.MIN_SAFE_INTEGER;
                }
                else {
                    n1 = Number.MIN_SAFE_INTEGER + obj1.page_number;
                }
            }

            if (obj2.book_id == bookId) {
                if (obj2.page_number == pageNumber) {
                    n2 = Number.MIN_SAFE_INTEGER;
                }
                else {
                    n2 = Number.MIN_SAFE_INTEGER + obj2.page_number;
                }
            }

            if (obj1.obj_type == 'O') {
                n1 = Number.MAX_SAFE_INTEGER - 1;
            }

            if (obj2.obj_type == 'O') {
                n2 = Number.MAX_SAFE_INTEGER - 1;
            }

            if (obj1.obj_type == 'T') {
                n1 = Number.MAX_SAFE_INTEGER;
            }

            if (obj2.obj_type == 'T') {
                n2 = Number.MAX_SAFE_INTEGER;
            }

            return n1 - n2;
        });
    }

    async processDownloadQueue() {
        if (this.isDownloading) {
            return;
        }

        let downloadableObject: DownloadableObject;
        let cdnURL: string;
        let extraSeparator = "";

        if (this.downloadPriorityQueue.length > 0) {
            this.isDownloading = true;

            downloadableObject = this.downloadPriorityQueue.shift();

            let currentTimeMillis = Date.now();

            let hasSpace = await this.installer.hasSpaceForObject(downloadableObject);
            if (!hasSpace) {

                if ((currentTimeMillis - this.lastSpaceRequestTime) > 10000) {
                    this.appEvent.EchoSpaceRequestForObject.emit(downloadableObject);
                    this.lastSpaceRequestTime = currentTimeMillis;
                }

                this.downloadPriorityQueue.unshift(downloadableObject);
                this.isDownloading = false;

                return;
            }

            let currentKey = this.bookKeys[downloadableObject.book_id];
            if (currentKey == null) {

                try {
                    this.bookKeys[downloadableObject.book_id] = await this.book.getKeyById(downloadableObject.book_id);
                } catch (error) {
                    console.log("DownloadManagerService -> Erro acessando a chave do livro! [" + JSON.stringify(error) + "]");
                }

                this.downloadPriorityQueue.unshift(downloadableObject);
                this.isDownloading = false;
                return;
            }

            cdnURL = this.booksToDownload[downloadableObject.book_id].cdnURLs[0];

            if (!cdnURL.endsWith("/")) {
                extraSeparator = "/";
            }

            let urlToDownload =
                cdnURL +
                extraSeparator +
                downloadableObject.book_id +
                "/" +
                downloadableObject.book_version +
                "/" +
                downloadableObject.exported_filename;

            console.log('DownloadManagerService -> URL da vez: ' + urlToDownload + ' (obj_type = ' + downloadableObject.obj_type + ')');

            if (
                downloadableObject.status == "completo" ||
                downloadableObject.status == "instalado"
            ) {
                this.isDownloading = false;
                console.log('DownloadManagerService -> Objeto já baixado! (' + downloadableObject.exported_filename + ')');

                return;
            }

            const targetURI = this.fileSystem.getDirectory() + downloadableObject.exported_filename;
            downloadableObject.status = 'baixando';

            this.appEvent.EchoDownloadObjectStart.emit(downloadableObject);

            // TODO: gerenciar progresso abaixo
            // this.fileTransfer.onprogress = progressEvent => {
            //   // TODO: implement incremental progress notification
            // };

            this.downloadService.download(
                urlToDownload,
                targetURI,
                async (entry) => {
                    // atualiza progresso / status
                    downloadableObject.compressed_downloaded_size = downloadableObject.compressed_size;
                    downloadableObject.status = "completo";

                    console.log('DownloadManagerService -> objeto baixado -> encaminhando p/ o InstallerService + (' + downloadableObject.exported_filename + ')');

                    try {
                        await this.installer.installObject(downloadableObject, currentKey);

                        try {
                            await this.fileSystem.request(downloadableObject.exported_filename).delete();
                        }
                        catch (error) {
                            const errorStr =
                                'DownloadManagerService -> Erro ao tentar remover arquivo temporário de instalação de livro (' +
                                downloadableObject.book_id +
                                ' - ' +
                                targetURI +
                                '):\n' +
                                JSON.stringify(error);


                            console.log(errorStr);
                        }
                        finally {
                            // mesmo que arquivo temporário de download não tenha sido removido,
                            // o objeto foi instalado com sucesso
                            console.log('DownloadManagerService -> objeto instalado! (' + downloadableObject.exported_filename + ')');
                            downloadableObject.status = "instalado";
                            this.appEvent.EchoDownloadObjectFinish.emit(downloadableObject);
                        }

                    }
                    catch (error) {
                        downloadableObject.status = 'parado';

                        let errorStr = "DownloadManagerService -> Erro ao instalar arquivo " +
                                        downloadableObject.exported_filename + ": " + error;
                        this.downloadPriorityQueue.unshift(downloadableObject);

                        console.log(errorStr);
                    }
                    finally {
                        this.isDownloading = false;
                    }
                },
                error => {
                    this.isDownloading = false;
                    downloadableObject.status = "parado";
                    this.downloadPriorityQueue.unshift(downloadableObject);

                    console.log(
                        "DownloadManagerService -> Erro baixando arquivo (" + urlToDownload + "):" + JSON.stringify(error)
                    );
                },
                false,
                undefined,
                currentKey + DownloadManagerService.BOOK_SALT
            );
        }
        else {
            this.stopDownloadPolling();
        }
    }

    public hasObjectsInQueue() : boolean {
        return (this.downloadPriorityQueue.length > 0);
    }



}
