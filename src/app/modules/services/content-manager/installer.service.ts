import { Injectable, Inject, ReflectiveInjector } from '@angular/core';
import { DownloadableObject } from './downloadable-object';
import { UserSettingsService } from '../user-settings.service';
import { VaultDecrypterService } from '../vault/vault-decrypter.service';
import { HotspotTypeEnum } from '../reader/hotspot-type.enum';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
import { ZipContract } from '../contracts/zip/zip.contract';
import { DiskUsageContract } from '../contracts/file-system/disk-usage.contract';
import { VaultToolsService } from '../vault/vault-tools.service';
import { LayoutFileSystemEnum } from "../contracts/file-system/layout-file-system.enum";

const sql = require('sql.js');
declare var DiskSpacePlugin;

export interface Positionable {
    x: number;
    y: number;
    w: number;
    h: number;
}

export interface OED extends Positionable {
    name: string;
    description: string;
    id: number;
    type: string;
    parameters: string;
    autoplay: number;
    url: string;
    menu_class: string;
}

@Injectable()
export class InstallerService {
    private ledUsedSpace: number;
    private deviceFreeSpace: number;
    private wordHotspotsMap: {} = {};

    constructor(@Inject('PlatformService') private cordovaPlatform: PlatformContract,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        @Inject('ZipService') private zip: ZipContract,
        private userSettings: UserSettingsService,
        private vault: VaultDecrypterService,
        private tools: VaultToolsService) {
        this.ledUsedSpace = 0;
        this.deviceFreeSpace = Number.MAX_SAFE_INTEGER;
    }

    installObject(downloadableObject: any, bookKey: string): Promise<any> {
        if (downloadableObject.obj_type == 'P') {
            return this.installPage(downloadableObject, bookKey);
        } else if (downloadableObject.obj_type == 'O') {
            return this.installOed(downloadableObject);
        } else if (downloadableObject.obj_type == 'E') {
            return this.installEPUB(downloadableObject);
        } else if (downloadableObject.obj_type == 'T') {
            return this.installTeacherLayer(downloadableObject, bookKey);
        }
    }

    public installEPUB(downloadableObject): Promise<any> {
        return this.unzipEPUB(downloadableObject.exported_filename, downloadableObject.book_id);
    }

    installPage(downloadableObject: DownloadableObject, bookKey) {
        return new Promise((resolve, reject) => {
            this.extractDbBytes(downloadableObject.exported_filename, false)
                .then((dbBytes: any) => {
                    // open existing manifest JSON file or create new
                    this.getBookInstalledManifest(downloadableObject)
                        .then(async bookInstalledManifest => {

                            if (bookInstalledManifest['pages_manifest_urls'] == null) {
                                bookInstalledManifest['pages_manifest_urls'] = [];
                            }

                            let pagesJSONFolder = this.getBookBasePath(downloadableObject.book_id) + 'pages-json/';

                            let jsonName = 'page-' + downloadableObject.page_id + '.json';

                            let pageData = {
                                page_id: downloadableObject.page_id,
                                url: pagesJSONFolder + jsonName
                            };
                            bookInstalledManifest['pages_manifest_urls'].push(pageData);

                            let db = new sql.Database(new Uint8Array(dbBytes));
                            let res = db.exec(
                                "SELECT page_number, page_file_content FROM page WHERE page_id = " + downloadableObject.page_id
                            );

                            if (downloadableObject.page_number == 1) {
                                let pageIdToNumberMap = null;
                                if (downloadableObject.extra_install_data) {
                                    pageIdToNumberMap = downloadableObject.extra_install_data.pageIdToNumberMap;
                                }

                                bookInstalledManifest.chapters = this.getSummaryTree(db, pageIdToNumberMap);
                                bookInstalledManifest.book = this.getBookInfo(db);

                            }

                            bookInstalledManifest.version_number = Math.max(bookInstalledManifest.version_number | 0, downloadableObject.book_version);

                            let oeds = await this.getOEDs(downloadableObject.page_id, db, downloadableObject.book_id, bookKey);
                            let pageWordHotspots = this.getPageWordHotspots(downloadableObject.page_number, db, downloadableObject.book_id, bookKey);

                            if (bookInstalledManifest['oeds'] == null) {
                                bookInstalledManifest['oeds'] = [];
                            }
                            for (let oedItem of oeds) {
                                bookInstalledManifest['oeds'].push({
                                    id: oedItem.id,
                                    page_id: downloadableObject.page_id,
                                    page_number: downloadableObject.page_number,
                                    name: oedItem.name,
                                    menu_class: oedItem.menu_class,
                                    type: oedItem.type
                                })

                            }

                            let sqlRow = res[0].values[0];
                            let pageNumber = sqlRow[0];
                            let pageBytes = sqlRow[1];

                            db.close();

                            this.savePage(
                                downloadableObject.book_id,
                                pageNumber,
                                downloadableObject.page_id,
                                pageBytes, oeds
                            ).then(
                                async (result) => {
                                    try {
                                        await this.saveWordData(downloadableObject.book_id, pageWordHotspots);
                                        this.saveInstalledManifest(
                                            bookInstalledManifest,
                                            downloadableObject.book_id
                                        ).then(() => {
                                            delete downloadableObject['extra_install_data'];
                                            downloadableObject.status = 'instalado';
                                            resolve(downloadableObject);
                                        }).catch(error => {
                                            let errorStr = 'installPage -> erro ao salvar manifesto do livro em instalação: ' +
                                                JSON.stringify(error);
                                            console.log(errorStr);
                                            downloadableObject.status = 'parado';
                                            reject(downloadableObject);
                                        });
                                    } catch (error) {
                                        let str = 'installPage-> Error no saveWordData: ' + JSON.stringify(error);
                                        console.log(str);
                                        return reject(str);
                                    }
                                }).catch((error) => {
                                    let errorStr = 'Installer -> InstallPage -> erro ao chamar savePage (' + JSON.stringify(error);
                                    reject(errorStr);
                                })
                        })
                        .catch(error => {
                            downloadableObject.status = 'parado';
                            reject(downloadableObject);
                        });
                })
                .catch(reject);
        });
    }

    installOed(downloadableObject: DownloadableObject): Promise<any> {
        let pageZipURI = downloadableObject.exported_filename;
        let extractionURI = downloadableObject.book_id + '/';


        return new Promise((resolve, reject) => {

            this.zip.unzip(
                {
                    from: pageZipURI,
                    to: extractionURI
                },
                function (result) {
                    resolve()
                },
                (error) => {
                    let errorStr = "Installer - installOed - erro na extração do ZIP (" + JSON.stringify(error) + ")";
                    reject(errorStr);
                }
            );
        });
    }

    public uninstallPage(pageImgObj: DownloadableObject) : Promise<any> {
        return new Promise<any>(async (resolve, reject) => {
            let pageBlobsFolder = this.getBookBasePath(pageImgObj.book_id) + 'pages/';
            let pageJSONsFolder = this.getBookBasePath(pageImgObj.book_id) + 'pages-json/';

            let pageBlobName = 'page-' + pageImgObj.page_id + '.imgblob';
            let pageJSONFileName = 'page-' + pageImgObj.page_id + '.json';

            let pageBlobPath = pageBlobsFolder + pageBlobName;
            let pageJSONPath = pageJSONsFolder + pageJSONFileName;
            try {
                await this.fileSystem.request(pageBlobPath).delete();
                await this.fileSystem.request(pageJSONPath).delete();

                resolve();
            }
            catch (error) {
                let errorStr = 'uninstallPage -> erro removendo arquivos da página! (' + JSON.stringify(error) + ')';
                reject(errorStr);
            }
        });
    }

    public uninstallOED(oedObj: DownloadableObject) : Promise<any> {
        return new Promise<any>((resolve, reject) => {

            if (oedObj.resource_filename != null) {
                let oedFolder = oedObj.resource_filename.substring(1).replace(/\\/g, "/");
                let pathToDelete = this.getBookBasePath(oedObj.book_id) + oedFolder;
                this.fileSystem.request(pathToDelete).delete().then(resolve).catch(reject);
            }
            else {
                reject("uninstallOED -> couldn't determine oed path to delete!");
            }
        });
    }

    public uninstallTeacherLayer(tlayerObj: DownloadableObject): Promise<any> {

        let teacherPageFolder = this.getBookBasePath(tlayerObj.book_id) + 'teacher_layer/';
        return this.fileSystem.request(teacherPageFolder).delete();
    }

    getBookBasePath(bookId) {
        return bookId + '/';
    }

    public async getAvailableSpace() {
        if (this.deviceFreeSpace == Number.MAX_SAFE_INTEGER) {
            await this.updateDiskSpaceInfo();
        }

        let quotaRemainingSpace = this.userSettings.getUserDefinedSpaceLimit() - this.getCurrentLEDUsedSpace();

        return Math.min(quotaRemainingSpace, this.deviceFreeSpace);
    }

    public async hasSpaceForObject(downloadableObj: DownloadableObject): Promise<boolean> {
        let availableSpace = await this.getAvailableSpace();
        let spaceRequiredForObj = (downloadableObj.compressed_size | 0) +
            (downloadableObj.exported_filesize_uncompressed | 0) + 1000000;
        return (spaceRequiredForObj < availableSpace);
    }

    public updateDiskSpaceInfo() {

        return new Promise<any>((resolve, reject) => {
            this.fileSystem.getDiskUsage().then((usageInfo: DiskUsageContract) => {
                this.ledUsedSpace = usageInfo.app;
                this.deviceFreeSpace = usageInfo.free;
                resolve();
            }).catch(() => {
                console.log('Installer - updateDiskSpaceInfo() - erro obtendo informações de espaço em disco');
                reject();
            });
        });
    }

    private async getOEDs(pageId: Number, db: any, bookId: string, bookKey) {
        let oeds = new Array<OED>();
        let res = db.exec("SELECT h.hotspot_id, h.hotspot_x, h.hotspot_y, h.hotspot_width, h.hotspot_height, h.hotspot_type, h.hotspot_name, " +
            "h.hotspot_description, h.hotspot_menu_class, r.resource_id, r.resource_internal_storage, r.resource_file_name, " +
            "r.resource_file_content, r.resource_parameters, r.resource_autoplay, p.page_number FROM hotspot h LEFT JOIN resource r " +
            "ON h.hotspot_resource_id = r.resource_id LEFT JOIN page p ON h.hotspot_page_id = p.page_id " +
            "WHERE ((h.hotspot_type != 'WORD' AND h.hotspot_type !='WORD_TEACHER') OR h.hotspot_description IS NOT NULL) AND h.hotspot_page_id = " + pageId);

        if (res.length == 0) {
            return oeds;
        }
        let hotspotCount = res[0].values.length;
        let hotspotFields = res[0].columns;

        let pageIdsToLoad = [];
        for (let i = 0; i < hotspotCount; i++) {

            let sqlRow = res[0].values[i];

            let oed: OED = {

                id: sqlRow[hotspotFields.indexOf('hotspot_id')],
                name: sqlRow[hotspotFields.indexOf('hotspot_name')],
                x: sqlRow[hotspotFields.indexOf('hotspot_x')],
                y: sqlRow[hotspotFields.indexOf('hotspot_y')],
                w: sqlRow[hotspotFields.indexOf('hotspot_width')],
                h: sqlRow[hotspotFields.indexOf('hotspot_height')],
                description: sqlRow[hotspotFields.indexOf('hotspot_description')],
                type: sqlRow[hotspotFields.indexOf('hotspot_type')],
                url: sqlRow[hotspotFields.indexOf('resource_file_name')],
                parameters: sqlRow[hotspotFields.indexOf('resource_parameters')],
                autoplay: sqlRow[hotspotFields.indexOf('resource_autoplay')],
                menu_class: sqlRow[hotspotFields.indexOf('hotspot_menu_class')]
            };

            if (sqlRow[hotspotFields.indexOf("resource_internal_storage")] == 1) {

                let extension = '.oedres';
                if (HotspotTypeEnum.AUDIO_AUTOPLAY == HotspotTypeEnum[oed.type]) {
                    extension = '.mp3';
                }
                else if (HotspotTypeEnum.IMAGE_ZOOM == HotspotTypeEnum[oed.type]) {
                    extension = '.png';
                }

                oed.url = 'internal/' + oed.name + '.' + oed.id + extension;

                let filename = this.getBookBasePath(bookId) + oed.url;
                try {
                    await this.fileSystem.request(this.getBookBasePath(bookId) + 'internal/').createDir();
                    let resourceBytes = sqlRow[hotspotFields.indexOf("resource_file_content")];
                    this.vault.prepare(bookKey + this.tools.salt, bookId);
                    resourceBytes = this.vault.bitwiseX(resourceBytes, bookId);
                    try {
                        await this.saveBlob(filename, resourceBytes, '');
                    }
                    catch (error) {
                        console.log('Installer -> getOEDS -> error calling saveBlob! (' + JSON.stringify(error) + ')');
                    }
                }
                catch (error) {
                    console.log('Installer -> getOEDS: error creating resource internal folder')
                }
            }

            oeds.push(oed);

        }
        return oeds;
    }

    private getPageWordHotspots(pageNumber: number, db: any, bookId: string, bookKey, hotspot_type = 'WORD') {
        try {
            let hotspotsByWord = {};
            let res = db.exec("SELECT h.hotspot_id, h.hotspot_x, h.hotspot_y, h.hotspot_width, h.hotspot_height, " +
                "h.hotspot_type, h.hotspot_name, h.hotspot_description, h.hotspot_menu_class, " +
                "p.page_number FROM hotspot h LEFT JOIN page p ON h.hotspot_page_id = p.page_id " +
                "WHERE h.hotspot_type = 'WORD' OR h.hotspot_type = 'WORD_TEACHER';");

            if (res.length == 0) {
                return hotspotsByWord;
            }
            let hotspotCount = res[0].values.length;
            let hotspotFields = res[0].columns;

            let pageIdsToLoad = [];
            for (let i = 0; i < hotspotCount; i++) {

                let sqlRow = res[0].values[i];
                let hotspotId = sqlRow[hotspotFields.indexOf('hotspot_id')];
                let hotspotName = sqlRow[hotspotFields.indexOf('hotspot_name')];
                let hotspotType = HotspotTypeEnum[sqlRow[hotspotFields.indexOf('hotspot_type')]];

                let hotspot = {
                    id: hotspotId,
                    x: sqlRow[hotspotFields.indexOf('hotspot_x')],
                    y: sqlRow[hotspotFields.indexOf('hotspot_y')],
                    w: sqlRow[hotspotFields.indexOf('hotspot_width')],
                    h: sqlRow[hotspotFields.indexOf('hotspot_height')],
                    type: hotspotType
                };

                if (!hotspotsByWord[hotspotName]) {
                    hotspotsByWord[hotspotName] = {};
                }

                if (hotspotsByWord[hotspotName][pageNumber] == null) {
                    hotspotsByWord[hotspotName][pageNumber] = [];
                }

                hotspotsByWord[hotspotName][pageNumber].push(hotspot);
            }
            return JSON.stringify(hotspotsByWord);

        } catch (e) {
            console.log(e);
        }
    }

    private async accessWordHotspotsFile(bookId: string, type?: string): Promise<any> {
        const bookFolderPath = this.getBookBasePath(bookId);
        let file_name = '';
        if (type === 'default') {
            file_name = bookFolderPath + 'word_hotspot_map.json';
        } else {
            file_name = bookFolderPath + 'word_hotspot_map_' + type + '.json';
        }
        return await new Promise(async (resolve, reject) => {
            this.fileSystem.exists(file_name, exists => {
                if (exists) {
                    this.fileSystem
                        .request(file_name)
                        .readFile()
                        .toJSON(json => {
                            if (json) return resolve(json);
                            else return resolve({})
                        }, error => {
                            console.log(
                                'Installer - erro criando/acessando arquivo do livro!'
                            );
                            return error;
                        });
                } else {
                    return resolve({});
                }
            });
        });
    }

    private installTeacherLayer(downloadableObject: any, bookKey: any) {
        return new Promise(async (resolve, reject) => {
            let allPagesOk = true;
            let bookInstalledManifest;
            try {
                let dbBytes = await this.extractDbBytes(downloadableObject.exported_filename, true);
                bookInstalledManifest = await this.getBookInstalledManifest(downloadableObject);

                if (bookInstalledManifest['teacher_layers_urls'] == null) {
                    bookInstalledManifest['teacher_layers_urls'] = [];
                }

                let db = new sql.Database(new Uint8Array(dbBytes));
                let res = db.exec(
                    "SELECT page_id FROM 'page' WHERE page_file_teacher_content IS NOT NULL ");

                let teacherPageCount = res[0].values.length;

                let pageIdsToLoad = [];
                for (let i = 0; i < teacherPageCount; i++) {
                    let sqlRow = res[0].values[i];
                    let pageId = sqlRow[0];
                    pageIdsToLoad.push(pageId);
                }
                let savedPageCount = 0;


                for (let pageId of pageIdsToLoad) {

                    res = db.exec(
                        "SELECT page_file_teacher_content FROM 'page' WHERE page_id = " + pageId);

                    let sqlRow = res[0].values[0];

                    let pageBytes = sqlRow[0];

                    let teacherPageFolder = this.getBookBasePath(downloadableObject.book_id) + 'teacher_layer/';

                    let blobName = 'tlayer-p' + pageId + '.imgblob';
                    try {
                        await this.saveBlob(blobName, pageBytes, teacherPageFolder);

                        let teacherPageInfo = {
                            page_id: pageId,
                            url: teacherPageFolder + blobName
                        };
                        bookInstalledManifest['teacher_layers_urls'].push(teacherPageInfo);
                    }
                    catch (error) {
                        allPagesOk = false;
                        break;
                    }
                }
                db.close();


            }
            catch (error) {
                allPagesOk = false;
            }
            finally {
                if (allPagesOk) {
                    try {
                        await this.saveInstalledManifest(bookInstalledManifest, downloadableObject.book_id);
                        downloadableObject.status = 'instalado';
                        resolve(downloadableObject);
                    }
                    catch (error) {
                        reject(error);
                    }
                } else {
                    downloadableObject.status = 'parado';
                    reject(downloadableObject);
                }
            }


        });
    }

    private getBookInstalledManifest(downloadableObject): Promise<any> {
        return new Promise((resolve, reject) => {
            let bookFolderPath = this.getBookBasePath(downloadableObject.book_id);

            this.fileSystem
                .request(bookFolderPath)
                .createDir().then(() => {
                    this.fileSystem.exists(bookFolderPath + 'installed_book_manifest.json', exists => {

                        if (exists) {
                            this.fileSystem
                                .request(bookFolderPath + 'installed_book_manifest.json')
                                .readFile()
                                .toJSON(json => {
                                    let manifestObj = json;
                                    manifestObj.book_id = downloadableObject.book_id;
                                    return resolve(manifestObj);
                                }, error => {
                                    console.log(
                                        'Installer - erro criando/acessando arquivo do livro!'
                                    );
                                    return reject(error)
                                });
                        }
                        else {
                            resolve({ book_id: downloadableObject.book_id });
                        }

                    });
                }).catch(error => {
                    let errorStr = "getBookInstalledManifest -> erro na obtenção de manifesto-base do livro instalado (createDir): " +
                        JSON.stringify(error);
                    reject(errorStr);
                });
        });
    }

    private savePage(bookId, pageNumber, pageId, pageImageBytes, oeds): Promise<any> {
        return new Promise((resolve, reject) => {
            let pagesPath = this.getBookBasePath(bookId) + 'pages-json/';
            let pageJSONFileName = 'page-' + pageId + '.json';
            let pageBlobName = 'page-' + pageId + '.imgblob';

            this.fileSystem
                .request(pagesPath)
                .createDir()
                .then(scope => {
                    const data = JSON.stringify({
                        page: {
                            id: pageId,
                            number: pageNumber,
                            url: pageBlobName,
                        },
                        oeds: oeds
                    });

                    this.fileSystem.request(pagesPath + pageJSONFileName)
                        .writeFile(new Buffer(data),
                            success => {

                                let pageBlobFolder = this.getBookBasePath(bookId) + 'pages/';

                                this.saveBlob(pageBlobName, pageImageBytes, pageBlobFolder)
                                    .then(() => {
                                        resolve(event);
                                    })
                                    .catch(() => {
                                        reject();
                                    });

                            }, error => {
                                console.log('Installer - erro criando página (' + pageJSONFileName + ')');
                                reject(error);
                            });

                }).catch(error => {
                    let errorStr = "savePage->createDir - Falha ao criar pasta " + JSON.stringify(error);
                    return reject(error);
                });
        });
    }

    private saveBlob(blobName, blobBytes, blobFolder): Promise<any> {
        return new Promise((resolve, reject) => {
            this.fileSystem
                .request(blobFolder)
                .createDir()
                .then(scope => {
                    this.fileSystem.request(blobFolder + blobName)
                        .writeFile(new Buffer(blobBytes), resolve, reject);
                }).catch(error => {
                    console.log(
                        'Installer - Erro ao salvar imagem da página do livro!'
                    );
                    reject(error);
                });
        });
    }

    private saveInstalledManifest(installedBookManifest, bookId): Promise<any> {
        return new Promise((resolve, reject) => {
            const manifestFileFolder = this.getBookBasePath(bookId);
            this.fileSystem
                .request(manifestFileFolder + 'installed_book_manifest.json')
                .writeFile(new Buffer(JSON.stringify(installedBookManifest)), resolve,
                    error => {
                        console.log('Installer - Erro criando/acessando arquivo do manifesto instalado do livro');
                        return reject(error);
                    });
        });
    }

    private async saveWordData(bookId, pageWordHotspots, type = 'default'): Promise<any> {
        return new Promise(async (resolve, reject) => {

            const manifestFileFolder = this.getBookBasePath(bookId);
            const filePath = manifestFileFolder + 'word_hotspots.dat';
            if (Object.keys(pageWordHotspots).length > 0) {
                this.fileSystem.request(filePath).writeFile(pageWordHotspots + ',\n', resolve,
                    error => {
                        console.log('Installer - Erro criando/acessando arquivo dicionário de palavras');
                        return reject(error);
                    }, true);

            } else {
                resolve(true);
            }
        });
    }

    private extractDbBytes(exportedFilename, isTeacherLayer): Promise<any> {
        let pageZipURI = exportedFilename;
        let extractionURI = '/';

        return new Promise((resolve, reject) => {

            this.zip.unzip(
                {
                    from: pageZipURI,
                    to: extractionURI
                },
                (result) => {

                    let fileName;
                    if (!isTeacherLayer) {
                        fileName = 'page.db';
                    }
                    else {
                        fileName = 'teacher.db';
                    }

                    this.fileSystem
                        .request(fileName)
                        .readFile()
                        .get()
                        .then((fileBytes) => {

                            this.fileSystem.request(fileName).delete().then(() => {
                                resolve(fileBytes);
                            }).catch((error) => {
                                let errorStr = 'erro removendo "' + fileName + '": ' + JSON.stringify(error);
                                console.log(errorStr);
                                resolve(fileBytes);
                            });
                        })
                        .catch(error => {
                            console.log('Installer - extractDbBytes - Error ao extrair db: ' + JSON.stringify(error));
                            return reject(error);
                        });
                },
                reject
            );
        });
    }

    private unzipEPUB(exportedFilename, bookId): Promise<any> {
        let pageZipURI = exportedFilename;
        let extractionURI = bookId + '/';

        return new Promise((resolve, reject) => {
            this.zip.unzip(
                {
                    from: pageZipURI,
                    to: extractionURI
                },
                function (result) {
                    resolve()
                },
                reject
            );
        });
    }

    private getSummaryTree(db, pageIdToNumberMap) {
        const tree = [];
        const index = {};
        const sections = [];
        let ret = {};
        const res = db.exec("SELECT * FROM chapter");

        if (res.length == 0) {
            return;
        }

        const sqlRow = res[0].values.map(row => {
            const obj = {};
            for (let i = 0; i < row.length; ++i) {
                obj[res[0].columns[i]] = row[i];
            }
            return obj;
        });

        sqlRow.forEach(el => {
            if (/^[a-zA-Z]$/.test(el.chapter_prefix)) {
                sections.push(el);
            }
            else {
                index[el.chapter_prefix] = el;
            }
        });

        let hasSections = ((pageIdToNumberMap != null) && (sections.length > 0));

        sqlRow.forEach(el => {
            el.chapter_prefix.replace(/^(.*)\.[0-9]+$/, (match, p) => {
                let parent = index[p];
                if (!parent) {
                    return '';
                }
                if (!parent.children) {
                    parent.children = [el];
                } else {
                    parent.children.push(el);
                }
            });

            if (/^[0-9]+$/.test(el.chapter_prefix)) {
                tree.push(el);
            }

        });

        for (let chapter of tree) {
            for (let i = 0; i < sections.length; i++) {
                let currentChapterStartPageNumber = pageIdToNumberMap[chapter.chapter_first_page_id];
                let currentSectionStartPageNumber = pageIdToNumberMap[sections[i].chapter_first_page_id];

                let nextSectionStartPageNumber;
                if (i < sections.length - 1) {
                    nextSectionStartPageNumber = pageIdToNumberMap[sections[i + 1].chapter_first_page_id];
                }
                else {
                    nextSectionStartPageNumber = Number.MAX_SAFE_INTEGER;
                }

                if ((currentChapterStartPageNumber >= currentSectionStartPageNumber) &&
                    (currentChapterStartPageNumber < nextSectionStartPageNumber)) {

                    if (sections[i].children == null) {
                        sections[i].children = [chapter];
                    }
                    else {
                        sections[i].children.push(chapter);
                    }
                }
            }
        }

        ret = hasSections ? { children: sections } : { children: tree };

        return ret;
    }

    private getBookInfo(db) {


        const res = db.exec("SELECT * FROM book LIMIT 1");
        let columnNames = res[0].columns;
        let valuesFirstRow = res[0].values[0];
        let bookRow = res[0];
        let book = {};

        for (let i = 0; i < columnNames.length; i++) {
            book[columnNames[i]] = valuesFirstRow[i];
        }

        return book;
    }

    private getCurrentLEDUsedSpace(): number {
        return this.ledUsedSpace;

    }
}
