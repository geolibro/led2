export interface DownloadableObject {
    page_id?: number;
    book_id: string;
    obj_type: string;
    compressed_size: number;
    exported_filename: string;
    book_version: number;
    exported_filesize_uncompressed: number;
    compressed_downloaded_size: number;
    resource_filename?: string;
    page_number?: number;
    status: string;
    obligatory?: boolean;
    extra_install_data?: any;
}
