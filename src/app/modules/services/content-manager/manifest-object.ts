import {DownloadableObject} from "./downloadable-object";

export interface ManifestObject {
    files: Array<DownloadableObject>,
    version_number: string,
    pages: Array<DownloadableObject>,
    blocks?: any,
    teacherLayer?: any

}
