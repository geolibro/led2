import { TestBed, inject } from '@angular/core/testing';

import { DownloadManagerService } from './download-manager.service';

describe('DownloadManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DownloadManagerService]
    });
  });

  it('should be created', inject([DownloadManagerService], (service: DownloadManagerService) => {
    expect(service).toBeTruthy();
  }));
});
