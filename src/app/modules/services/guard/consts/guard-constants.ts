export class GuardConstants {
  static readonly DEFAULT_FILE_CURRENT_SESSION: string    = 'current-session.libro';
  static readonly DEFAULT_FILE_LAST_SESSION: string       = 'last-session.libro';
  static readonly DEFAULT_FILE_USER: string               = 'user-guard.libro';
  static readonly DEFAULT_FILE_BOOK: string               = 'books.libro';
  static readonly DEFAULT_GUARD: string                   = 'default';
  static readonly APP_GUARD: string                       = 'app';
  static readonly WEB_GUARD: string                       = 'web';
  static readonly PC_GUARD: string                        = 'pc';
}
