import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { AuthGuardService } from "./auth-guard.service";
import { Injectable } from "@angular/core";
import { UserLoggedFieldsContract } from "../../../contracts/user-logged-fields.contract";

@Injectable()
export class AuthResolverService implements Resolve<any> {
    constructor(private guard: AuthGuardService, private router: Router) {}
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        (async () => {
            await this.guard.getStorage().loadCurrentSession();

            const userSession: UserLoggedFieldsContract = this.guard.user();

            if (userSession && userSession.remember) this.router.navigate(["shelf"]);
        })();

        return null;
    }
}
