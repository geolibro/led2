import { UserLoggedFieldsContract } from "../../../contracts/user-logged-fields.contract";
import { UserLoginFieldsContract } from "../../../contracts/user-login-fields.contract";
import { Injectable, Host, Optional, Inject } from '@angular/core';
import { VaultDecrypterService } from "../vault/vault-decrypter.service";
import { HttpScaoResponse } from "../http/util/http-scao-response";
import { LegacyBookContract } from "../../../contracts/legacy-book.contract";
import { GuardConstants } from "./consts/guard-constants";
import { BookDao } from "../../daos/book.dao";
import { SessionGuardContract } from "./session-guard-contract";
import { AppEvent } from "../../events/app.event";
import { last } from "@angular/router/src/utils/collection";
import { PlatformContract } from '../contracts/platform/platform.contract';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
import { LayoutFileSystemEnum } from "../contracts/file-system/layout-file-system.enum";
import { UserMemoryFieldsContract } from "../../../contracts/user-memory-fields.contract";

@Injectable()
export class AuthStorageService {
    private _session: any = {};
    private guard: string;
    private _currentSession: UserLoggedFieldsContract = <UserLoggedFieldsContract>{};
    public book: BookDao;

    constructor(@Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        private vault: VaultDecrypterService,
        private appEvent: AppEvent,
        @Inject('PlatformService') private platform: PlatformContract) { }

    async destroy() {
        return await this.destroySession();
    }

    get currentSession() {
        return this._currentSession;
    }

    set currentSession(value: any) {
        Object.assign(this._currentSession, { memory: value });
    }

    setGuard(guard: string, data: any) {
        this.guard = guard;
        this._session[guard] = data;
    }

    getGuard(): SessionGuardContract {
        let guard = this.guard || GuardConstants.DEFAULT_GUARD;
        return this._session[guard] || null;
    }

    setUser(response: HttpScaoResponse) {
        let user = response.getUser();
        user['updated_at'] = (new Date()).toDateString();

        this._session[this.guard]['user'] = user;
    }

    setUserLiteraryWorks(response: HttpScaoResponse) {
        const books = response.getBookList();

        this._session[this.guard]['books'] = books;
    }

    updateUserLiteraryWorks(bookList: LegacyBookContract[]) {
        this._session[this.guard]['books'] = bookList;
    }

    updateUser(user: any) {
        this._session[this.guard]['user'] = user;
    }

    localAuth(params: UserLoginFieldsContract) {
        if (!this._currentSession) throw new Error('Não há conexão disponível.');

        const password = this.vault.get('AES').decrypt(this._currentSession.password, this._currentSession.led_user_id);
        if (this._currentSession && (this._currentSession.login === params.login && password.toString(this.vault.getEncode()) === params.password))
            return new HttpScaoResponse(params);

        throw new Error('[Autenticação Offline] Login e senha não confere.');
    }

    async loadCurrentSession() {
        if (this._session && this._session[this.guard]) {
            if (!this._currentSession) this._currentSession = <UserLoggedFieldsContract>{};
            Object.assign(this._currentSession, this.getGuard().user);
        } else {
            this._currentSession = <UserLoggedFieldsContract> await this.getCurrentSession();
        }
    }

    async save() {
        return new Promise((resolve, reject) => {

            const user = this._session[this.guard].user;

            this.fileSystem
                .request(`users/${user.led_user_id}/`)
                .createDir()
                .then(async result => {
                    try {
                        await this.saveUserSession();
                        await this.saveCurrentSession();
                        await this.book.store();
                        return resolve();
                    } catch (error) {
                        return reject(error);
                    }
                });
        });
    }

    private async saveUserSession() {
        return new Promise((resolve, reject) => {
            const user = this._session[this.guard].user;
            const PATH = `users/${user.led_user_id}/${GuardConstants.DEFAULT_FILE_USER}`;

            const data = new Buffer(JSON.stringify(user));
            this.fileSystem.request(PATH)
                .writeFile(data, resolve, reject);
        });
    }

    private async saveCurrentSession() {
        return new Promise((resolve, reject) => {
            this._currentSession = this._session[this.guard];
            const data = new Buffer(JSON.stringify(this._currentSession));
            this.fileSystem.request(GuardConstants.DEFAULT_FILE_CURRENT_SESSION)
                .writeFile(data, resolve, reject);
        });
    }

    private getCurrentSession(): Promise<{}> {
        return new Promise(async (resolve, reject) => {
            await this.platform.ready();
            this.fileSystem.request(`${GuardConstants.DEFAULT_FILE_CURRENT_SESSION}`)
                .readFile()
                .toJSON(async guard => {
                    this.setGuard(GuardConstants.DEFAULT_GUARD, guard);
                    return resolve(guard.user);
                }, error => {
                    resolve(null);
                });
        });
    }

    private async destroySession() {
        delete this._session[this.guard];
        this._session = {};
        this.book.clear();
        return await this.fileSystem.request(`${GuardConstants.DEFAULT_FILE_CURRENT_SESSION}`).delete();
    }
}
