import { UserLoginFieldsContract } from "../../../contracts/user-login-fields.contract";
import { Ws015Contract } from "../../../requests/contracts/ws/ws015.contract";
import { HttpService } from "../http/http.service";
import { Injectable, Inject } from '@angular/core';
import { HttpScaoResponse } from "../http/util/http-scao-response";
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';

@Injectable()
export class DeviceGuardService {
    private RequestUuidParams: any = {};
    constructor(private http: HttpService,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract) { }

    prepareUuidRequest(data: UserLoginFieldsContract) {
        this.RequestUuidParams.operation = 'J042';
        this.RequestUuidParams.login = data.login;
        this.RequestUuidParams.password = data.password;
    }

    getUniqueIdentifier(): Promise<string> {
        return new Promise((resolve, reject) => {
            this.getDeviceIdFromFileSystem()
                .then((result: any) => {

                    if (!result) {
                        this.getUuid(this.RequestUuidParams)
                            .then(uuid => {
                                this.persistDeviceIdOnFileSystem(uuid);
                                return resolve(uuid);
                            })
                            .catch(reject);
                    } else {
                        return resolve(result.device_id);
                    }

                })
                .catch(reject);
        });
    }

    private persistDeviceIdOnFileSystem(uuid: string) {
        const buffer = new Buffer(JSON.stringify({ device_id: uuid }));
        this.fileSystem.request('device.json')
            .writeFile(buffer,
                () => { },
                error => {
                    console.log(error);
                });
    }

    private getDeviceIdFromFileSystem() {

        return new Promise((resolve, reject) => {

            this.fileSystem
                .request('device.json')
                .readFile()
                .toJSON((result) => {
                    if (result.hasOwnProperty('device_id')) {
                        return resolve(result);
                    }
                    return resolve(null);
                }, error => {
                    return resolve(null);
                });

        });
    }

    private getUuid(params: Ws015Contract): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.makeRequest(this.http.getURL('scao', 'ws015'), params)
                .then((response: HttpScaoResponse) => {
                    if (!response.hasErrors()) return resolve(response.all().UID);
                    return reject(response.getErrorMessage());
                })
                .catch(reject);
        });
    }
}
