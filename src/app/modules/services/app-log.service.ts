import { Injectable, Inject } from '@angular/core';
import { PlatformContract } from './contracts/platform/platform.contract';

export enum AppLoggingMode {
    ShowOnConsoleAndWriteToFile,
    ShowOnConsoleOnly
}
@Injectable()
export class AppLogService {

    private loggingMode;

    constructor(@Inject('PlatformService') private platform: PlatformContract) {
        this.platform.ready().then(() => {
            // TODO: setup log file request, where logging will be written to
        }).catch(() => {
            console.log("");
            this.notifyCrash();
        })

    }

    public setUserIdentity(login)  {
        // TODO: put on crashlytics current user's identity info
    }

    setLoggingMode(mode) {
        this.loggingMode = mode;
    }

    log(txt: string) {

        if (this.loggingMode == AppLoggingMode.ShowOnConsoleAndWriteToFile) {
            // TODO: persist on application log
        }
    }

    notifyNonFatalCrash() {
        // TODO
    }

    notifyCrash() {
        // TODO
    }
}
