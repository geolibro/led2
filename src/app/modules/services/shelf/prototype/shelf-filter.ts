import { List } from 'immutable';
import { Subject } from 'rxjs/Subject';
import { LegacyBookContract } from '../../../../contracts/legacy-book.contract';
import { BookDao } from "../../../daos/book.dao";

export class ShelfFilter {
    book: BookDao;
    private instance: ShelfFilter;
    private cachedResult: any;
    public bookList: Subject<List<LegacyBookContract>> = new Subject<List<LegacyBookContract>>();

    constructor(book: BookDao) {
        if (!this.instance) this.instance = this;
        this.instance.book = book;
        return this.instance;
    }

    fetch(filterList: string[])  {
        (async () => {
            try {
                const bookList: List<LegacyBookContract> = await this.book.fetchData();

                let result = bookList;

                const filteredList = bookList.filter(book => {
                    for (let i = 0; i < filterList.length; i++) {
                        const fieldSearch: string = "book_discipline";
                        const fieldSearchValue: string = (filterList[i]) ? filterList[i].toLowerCase() : undefined;

                        if (filterList[i] === undefined || book[fieldSearch].toLowerCase().indexOf(fieldSearchValue) !== -1) {
                            return true;
                        }
                    }
                    return false;
                }).toList();

                if (filteredList.size > 0) result = filteredList;

                this.cachedResult = result;
                this.bookList.next(result);

            } catch (error) {
                console.log(`[ShelfFilter][fetch][ERROR] - ${error}`);
            }
        })();
    }

    getCachedResult() {
        return this.cachedResult;
    }
}
