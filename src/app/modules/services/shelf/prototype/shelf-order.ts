import { BookDao } from "../../../daos/book.dao";
import { LegacyBookContract } from "../../../../contracts/legacy-book.contract";
import { Subject } from "rxjs/Subject";
import { ShelfContract } from "../shelf.contract";
import { List } from "immutable";
import { SortEnum } from "./sort.enum";

export class ShelfOrder implements ShelfContract {
    private instance : ShelfOrder;
    private cachedResult : any;
    public bookList : Subject<List<LegacyBookContract>> = new Subject<List<LegacyBookContract>>();
    book : BookDao;

    constructor(book: BookDao) {
        if (!this.instance) this.instance = this;
        this.instance.book = book;
        return this.instance;
    }

    fetch(term? : any) : void {
        (async () => {
            try {
                const bookList: List<LegacyBookContract>  = await this.book.fetchData();

                let result = bookList;

                const bookListFiltered = this.sort(term, bookList);

                if (bookListFiltered.size > 0) result = bookListFiltered.toList();

                this.cachedResult = result;
                this.bookList.next(result);

            } catch (error) {
                console.log(`[ShelfSearch][fetch][ERROR] - ${error}`);
            }
        })();
    }

    getCachedResult() : any {
        return this.cachedResult;
    }

    private sort(term: number, bookList: List<LegacyBookContract>) {
        switch (term) {
            case SortEnum.SORT_TITLE:
                return this.sortTitle(bookList);

            case SortEnum.SORT_AUTHOR:
                return this.sortAuthor(bookList);

            case SortEnum.SORT_DISCIPLINE:
                return this.sortDiscipline(bookList);

            default:
                return bookList;
        }
    }

    private sortTitle(bookList: List<LegacyBookContract>) {
        return bookList.sort(function (a, b) {
            if (a.book_title < b.book_title) {
                return -1;
            }

            if (a.book_title > b.book_title) {
                return 1;
            }

            return 0;
        });
    }

    private sortAuthor(bookList: List<LegacyBookContract>) {
        return bookList.sort(function (a, b) {
            if (a.book_author > b.book_author) {
                return 1;
            }
            if (a.book_author < b.book_author) {
                return -1;
            }
            return 0;
        });
    }

    private sortDiscipline(bookList: List<LegacyBookContract>) {
        return bookList.sort((a, b) => {
            return a.book_discipline > b.book_discipline ? 1 : a.book_discipline < b.book_discipline ? -1 : 0;
        });
    }

}
