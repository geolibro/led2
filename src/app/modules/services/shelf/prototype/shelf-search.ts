import { LegacyBookContract } from '../../../../contracts/legacy-book.contract';
import { Subject } from 'rxjs/Subject';
import { ShelfContract } from "../shelf.contract";
import { VaultToolsService } from "../../vault/vault-tools.service";
import { BookDao } from "../../../daos/book.dao";
import { List } from 'immutable';

export class ShelfSearch implements ShelfContract {
    private instance: ShelfSearch;
    private tools: VaultToolsService;
    private cachedResult: any;
    public bookList: Subject<List<LegacyBookContract>> = new Subject<List<LegacyBookContract>>();
    book: BookDao;

    constructor(tools: VaultToolsService, book: BookDao) {
        if (!this.instance) this.instance = this;
        this.instance.tools = tools;
        this.instance.book = book;
        return this.instance;
    }

    fetch(term?: string) {
        (async () => {
            try {
                const bookList: List<LegacyBookContract>  = await this.book.fetchData();

                let result = bookList;

                const bookListFiltered = bookList.filter(book => {
                    return this.tools.sanitizeWord(book.book_title.toLowerCase()).indexOf(this.tools.sanitizeWord(term)) !== -1;
                }).toList();

                if (bookListFiltered.size > 0) result = bookListFiltered;

                this.cachedResult = result;
                this.bookList.next(result);

            } catch (error) {
                console.log(`[ShelfSearch][fetch][ERROR] - ${error}`);
            }
        })();
    }

    getCachedResult() {
        return this.cachedResult;
    }
}
