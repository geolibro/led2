export enum SortEnum {
    SORT_DEFAULT,
    SORT_TITLE,
    SORT_AUTHOR,
    SORT_DISCIPLINE
}
