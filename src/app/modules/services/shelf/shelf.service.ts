import { Subject } from 'rxjs/Subject';
import { Injectable } from "@angular/core";
import { BookDao } from "../../daos/book.dao";
import { ShelfFilter } from './prototype/shelf-filter';
import { ShelfSearch } from "./prototype/shelf-search";
import { VaultToolsService } from "../vault/vault-tools.service";
import { ShelfOrder } from "./prototype/shelf-order";
import { SortEnum } from "./prototype/sort.enum";

@Injectable()
export class ShelfService {
    private _searching: ShelfSearch;
    private _filtering: ShelfFilter;
    private _ordering: ShelfOrder;

    private _term: Subject<string>      = new Subject<string>();
    private _filter: Subject<string[]>  = new Subject<string[]>();
    private _order: Subject<number>     = new Subject<number>();

    private _cachedTerm: string = '';
    private _cachedFilter: string[] = [];
    private _cachedOrder: number;

    constructor(private book: BookDao, private tools: VaultToolsService) {
        this.bindContainers();
    }

    get filter() {
        return this._cachedFilter;
    }

    get ordering() {
        return this._ordering.bookList;
    }

    get searching() {
        return this._searching.bookList;
    }

    get filtering() {
        return this._filtering.bookList;
    }

    get term() {
        return this._cachedTerm;
    }

    get order() {
        return this._cachedOrder;
    }

    set filter(filter: string[]) {
        this._cachedFilter = filter;
        this._filter.next(filter);
    }

    set term(event: any) {
        this._cachedTerm = event.target.value.toLowerCase();
        this._term.next(this._cachedTerm);
    }

    set order(order: number) {
        this._cachedOrder = order;
        this._order.next(order);
    }

    getCachedFilter() {
        return this._filtering.getCachedResult();
    }

    getCachedSearch() {
        return this._searching.getCachedResult();
    }

    getCachedOrder() {
        return this._ordering.getCachedResult();
    }

    getBookFilters() {
        let formattedList = [];
        this.book.getBooksDisciplineList().map((item, index) => {
            formattedList.push({ name: item, selected: false, id: index });
        });

        this._cachedFilter.map(cachedFilter => {
            formattedList.map(item => {
                if (item.name === cachedFilter) item.selected = true;
            });
        });

        return formattedList;
    }

    private bindContainers() {
        /** Start a ShelfSearch object with parent injectors */
        this._searching = new ShelfSearch(this.tools, this.book);
        /** Initialize fetch data with term search event */
        this._term.subscribe(term => this._searching.fetch(term));

        this._filtering = new ShelfFilter(this.book);
        this._filter.subscribe(filter => this._filtering.fetch(filter));

        this._ordering = new ShelfOrder(this.book);
        this._order.subscribe(order => this._ordering.fetch(order));
    }

}
