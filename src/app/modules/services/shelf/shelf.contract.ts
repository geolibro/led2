import { BookDao } from "../../daos/book.dao";

export interface ShelfContract {
    book: BookDao;
    fetch(term?: any): void
    getCachedResult(): any;
}
