import { Injectable } from '@angular/core';
import { BookDao } from '../../daos/book.dao';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { ShelfResolverContract } from './shelf-resolver.contract';
import { ContentManagerService } from '../content-manager/content-manager.service';
import { UserSettingsService } from '../user-settings.service';
import { AuthGuardService } from '../guard/auth-guard.service';
import { AppEvent } from '../../events/app.event';
import {VaultToolsService} from "../vault/vault-tools.service";

@Injectable()
export class ShelfResolverService implements Resolve<ShelfResolverContract> {
    private shelfResolver: ShelfResolverContract = <ShelfResolverContract>{};
    constructor(private book: BookDao,
                private contentManager: ContentManagerService,
                private userSettings: UserSettingsService,
                private authGuard: AuthGuardService,
                private appEvent: AppEvent,
                private tools: VaultToolsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<ShelfResolverContract> {
        return new Promise(async (resolve, reject) => {
            try {
                this.shelfResolver.bookList                 = await this.book.fetchData();
                this.shelfResolver.emptyBookImage           = await this.tools.loadNoImageCoverInMemory();
                await this.book.loadBookStatus(this.contentManager);

                this.shelfResolver.bookShelfStatusList      = this.book.getBookShelfStatusList();
                this.shelfResolver.bookUpgradeableList      = this.book.getBookUpgradeableList();
                this.userSettings.init(this.authGuard.user().led_user_id, this.appEvent);

                return resolve(this.shelfResolver);
            } catch (error) {
                return reject(error);
            }

        });
    }
}
