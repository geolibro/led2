import { LegacyBookContract } from '../../../contracts/legacy-book.contract';
import { List } from 'immutable';
export interface ShelfResolverContract {
    bookList: List<LegacyBookContract>,
    bookShelfStatusList: any
    bookUpgradeableList: Array<any>
    emptyBookImage?: string;
}
