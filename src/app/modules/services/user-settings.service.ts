import { Injectable, Inject } from '@angular/core';
import { Ws0312Contract } from '../../requests/contracts/ws/ws312.contract';
import { AbstractService } from './abstract.service';
import { AppEvent } from '../events/app.event';
import { AuthGuardService } from './guard/auth-guard.service';
import { Http } from '@angular/http';
import { PlatformContract } from './contracts/platform/platform.contract';
import { DeviceContract } from './contracts/device/device.contract';
import { LedFileSystemContract } from './contracts/file-system/led-file-system.contract';
import { DownloadContract } from './contracts/download/download.contract';
import { NetworkContract } from './contracts/network/network.contract';

interface SyncCommand { action: string; book_id: string; key: string; value: string; }

@Injectable()
export class UserSettingsService extends AbstractService {

    private currentUserSettings: any = {};
    private ledUserId: string;
    static BOOKMARKS_KEY = 'bookmarks';
    static SETTINGS_FILENAME = 'user-settings.json';

    constructor(
        protected http: Http,
        @Inject('PlatformService') protected platform: PlatformContract,
        @Inject('DeviceService') protected device: DeviceContract,
        @Inject('FileSystemService') protected fileSystem: LedFileSystemContract,
        @Inject('DownloadService') protected downloadService: DownloadContract,
        @Inject('NetworkService') protected network: NetworkContract,
        protected auth: AuthGuardService) {
        super(http, platform, device, fileSystem, downloadService, network, auth);
    }

    init(ledUserId: string, appEvent: AppEvent) {

        this.ledUserId = ledUserId;

        this.load().then(() => {
            console.log('configurações de usuário (user-settings) carregadas!');
            appEvent.EchoSettingsReady.emit();

            this.sync().then(() => { console.log('user settings synced!') }).catch(error => {
                console.log(JSON.stringify(error));
            });

        }).catch(
            () => console.log('erro ao carregar configurações de usuário (user-settings)!')
        );
    }

    setUserDefinedSpaceLimit(limit: number) {
        this.put('space-usage-limit', limit);
    }

    getUserDefinedSpaceLimit() {
        let definedSpace = this.get('space-usage-limit');
        if (definedSpace == null) {
            definedSpace = Number.MAX_SAFE_INTEGER;
        }
        return definedSpace;
    }

    setLastPage(bookId, pageNumber) {
        let lastStored = this.getLastPage(bookId);
        if (lastStored != pageNumber) {
            this.put('lastPage', pageNumber, bookId);
        }
    }

    getLastPage(bookId): number {
        let storedValue = this.get('lastPage', bookId);

        let pageNumber;
        if (storedValue != null && storedValue != '') {
            pageNumber = parseInt(storedValue);
        }

        if (!isFinite(pageNumber)) {
            pageNumber = 1;
        }
        return pageNumber;
    }
    getAllBookmarkStatus(bookId) {
        return this.get(UserSettingsService.BOOKMARKS_KEY, bookId);
    }
    getBookmarkStatus(bookId, pageNumber): boolean {
        let markedPages: Array<boolean> = this.get(UserSettingsService.BOOKMARKS_KEY, bookId);
        let marked = false;

        if (markedPages != null) {
            if (markedPages[pageNumber] != null) {
                marked = markedPages[pageNumber];
            }
        }

        return marked;
    }

    setBookmarkStatus(bookId: string, pageNumber: number, present: boolean) {
        let markers: Array<boolean> = this.get(UserSettingsService.BOOKMARKS_KEY, bookId);

        if (markers == null) {
            markers = [];
        }

        if (markers[pageNumber] != present) {
            markers[pageNumber] = present;
            this.put(UserSettingsService.BOOKMARKS_KEY, markers, bookId);
        }

    }

    private put(settingName: string, settingValue: any, bookId?: string, syncStatus?: string) {
        if (syncStatus == null) {
            syncStatus = 'N';
        }

        if (bookId == null) {
            bookId = 'general';
        }

        if (this.currentUserSettings[bookId] == null) {
            this.currentUserSettings[bookId] = {};
        }
        this.currentUserSettings[bookId][settingName] = { value: settingValue, status: syncStatus };

        this.save();
    }

    private get(settingName: string, bookId?: string): any {
        let value = null;
        if (bookId == null) {
            bookId = 'general';
        }

        if (this.currentUserSettings[bookId] == null) {
            this.currentUserSettings[bookId] = {};
        }

        let setting = this.currentUserSettings[bookId][settingName];
        if (setting != null) {
            value = setting.value;
        }

        return value;
    }

    sync(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            let settingsJSON = this.getSettingsForSync();

            let lastSyncId = 0;
            if ((this.currentUserSettings['general'] != null) && this.currentUserSettings['general'].last_settings_synced != null) {
                lastSyncId = this.currentUserSettings['general'].value.last_settings_synced;
            }

            const syncData: Ws0312Contract = {
                operation: 'J323',
                login: this.auth.user().login,
                password: this.auth.user().password,
                led_user_id: this.auth.user().led_user_id,
                device_id: this.auth.user().device_id,
                last_settings_synced: lastSyncId,
                settings: settingsJSON,
                get_updates: 1,
                led_app_type: 'A',
                use_stats: []
            };

            this.platform.online().then(() => {
                this.buildUrl('ws312').makeRequest(syncData).subscribe(response => {
                    let responseData;
                    if (typeof response._body === 'string') {
                        responseData = JSON.parse(response._body);
                    } else if (typeof response._data === 'object') {
                        responseData = response._body;
                    }
                    if (responseData.error_code == "") {
                        this.processServerSettings(responseData);
                    }
                    resolve();
                });
            }).catch(reject);

        });
    }

    private load() {
        return new Promise<any>((resolve, reject) => {

            let PATH = 'users/' + this.ledUserId + '/';

            this.fileSystem
                .request(PATH)
                .createDir().then(() => {

                    PATH += UserSettingsService.SETTINGS_FILENAME;

                    this.fileSystem.exists(PATH, exists => {
                        if (!exists) {
                            this.currentUserSettings = {};
                            resolve();
                        } else {
                            this.fileSystem.request(PATH).readFile().toJSON(result => {
                                this.currentUserSettings = result;
                                resolve();
                            });
                        }
                    });

                });
        });
    }

    private getSettingsForSync(): Array<SyncCommand> {
        let settingsToSend: Array<SyncCommand> = [];

        for (let bookId in this.currentUserSettings) {
            if (bookId == 'general' || bookId == null) continue;
            let currentBookSettings = this.currentUserSettings[bookId];
            if (currentBookSettings.bookmarks != null && currentBookSettings.bookmarks.status == 'N') {
                for (let pageNumber in currentBookSettings.bookmarks.value) {
                    let isMarked = false;

                    if (currentBookSettings.bookmarks.value[pageNumber] != null) {
                        isMarked = currentBookSettings.bookmarks.value[pageNumber];

                        settingsToSend.push({
                            action: isMarked ? 'insert' : 'delete',
                            book_id: bookId,
                            key: 'bookmark',
                            value: pageNumber
                        });
                    }
                }
            }

            if ((currentBookSettings.lastPage != null) && (currentBookSettings.lastPage.status == 'N')) {
                settingsToSend.push({
                    action: 'insert',
                    book_id: bookId,
                    key: 'lastPage',
                    value: currentBookSettings.lastPage.value
                });
            }
        }

        return settingsToSend;
    }

    private processServerSettings(serverData: any) {
        let incomingSettings: Array<SyncCommand> = serverData.settings;
        let letSyncedId = 0;
        if (incomingSettings != null) {
            for (let serverCommand of incomingSettings) {

                if (serverCommand.book_id != null && serverCommand.book_id.trim() != "" &&
                    this.currentUserSettings[serverCommand.book_id] == null) {
                    this.currentUserSettings[serverCommand.book_id] = {};
                }

                let currentBookSettings = this.currentUserSettings[serverCommand.book_id];

                if (serverCommand.key == 'bookmark') {

                    if (currentBookSettings['bookmarks'] == null) {
                        currentBookSettings['bookmarks'] = { value: [], status: 'S' };
                    }
                    let bookmarkPage = parseInt(serverCommand.value);
                    this.currentUserSettings[serverCommand.book_id]['bookmarks'].value[bookmarkPage] = (serverCommand.action == 'insert') ? true : false;
                }
                else if (serverCommand.key == 'lastPage') {
                    let pageNumber = parseInt(serverCommand.value);
                    currentBookSettings['lastPage'] = { value: pageNumber, status: 'S' };
                }
                else {
                    // TODO: outros tipos de configurações por implementar
                }
            }
            if (this.currentUserSettings['general'] == null) {
                this.currentUserSettings['general'] = {};
            }
            this.currentUserSettings['general'].last_settings_synced = { value: serverData.last_settings_synced, status: 'S' };
        }

        this.setAllToSynced();

        this.save();
    }

    private setAllToSynced() {
        if (this.currentUserSettings != null) {
            for (let bookId in this.currentUserSettings) {
                let currentBookSettings = this.currentUserSettings[bookId];
                for (let settingKey in currentBookSettings) {
                    if (currentBookSettings[settingKey] != null) {
                        currentBookSettings[settingKey].status = 'S';
                    }
                }
            }
        }
    }

    private save() {
        return new Promise<any>((resolve, reject) => {
            const FILE_PATH = 'users/' + this.ledUserId + '/' + UserSettingsService.SETTINGS_FILENAME;

            this.fileSystem.request(FILE_PATH).writeFile(new Buffer(JSON.stringify(this.currentUserSettings)), resolve, reject);
            // LedFileSystem.request('users/' + this.ledUserId + '/').then((dirEntry) => {
            //   LedFileSystem.writer(, dirEntry, ).then(resolve).catch(reject);
            // }).catch(reject);
        });
    }
}
