export interface NetworkContract {
    isConnected(): boolean
    isLimitedConnection(): boolean
}
