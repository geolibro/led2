import {PageReaderService} from "../../reader/page-reader.service";

export interface EpubContract {
   bootstrapEpubPage(bytesArray: Array<any>, directory: string, iframeContainer: any);
   setStyle(style: any);
   generateDocument(pageReader: PageReaderService, blob?: boolean);
   updateEpubContainer();
   addSummaryEventListener(anchor, platform);
   navigateDirection?(direction);
   addSwipeEventListener?();
}
