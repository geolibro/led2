export interface DownloadContract {
  download(source: string, target: string, successCallback?: (fileEntry: any) => void,
           errorCallback?: (error: any) => void, trustAllHosts?: boolean,
           options?: any, xorKey?: string): void;

  showProgress(received, total, progressEvent?);
}
