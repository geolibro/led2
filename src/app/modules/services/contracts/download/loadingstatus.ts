export class Loadingstatus {
    private status: number;

    setPercentage(percentage) {
      this.status = percentage;
    }
    increment() {
      this.status++;
    }
    getStatus() {
      return this.status;
    }

}
