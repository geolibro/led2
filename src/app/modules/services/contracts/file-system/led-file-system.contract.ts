import { LayoutFileSystemEnum } from "./layout-file-system.enum";
import { DiskUsageContract } from './disk-usage.contract';
import { ReadFileOptionsContract } from './read-file-options.contract';
import { NativeFSFunctionsContract } from './native-fs-functions.contract';

export interface LedFileSystemContract extends ReadFileOptionsContract, NativeFSFunctionsContract {

    /** Led and rewrite functions */
    request(path?: string, layout?: LayoutFileSystemEnum);
    exists(path: string, handler: Function): void;
    readDir(options?: string | object): Promise<string[] | Buffer[]>;
    writeFile(data: string | Buffer | Uint8Array, successHandler: Function, errorHandler: Function, append: boolean);
    rename(oldPath: string, newPath: string): void;

    readFile();
    createDir(): Promise<any>
    delete(): Promise<any>;

    /** Aux Functions */
    getDirectory(): string;
    getRequestPath(): string;
    getDiskUsage(): Promise<DiskUsageContract>;

    openFileExternally(filePath: string, mimeType: string): Promise<any>;
    openLinkExternally?(link: string);
}
