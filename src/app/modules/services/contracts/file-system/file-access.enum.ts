export enum FileAccessEnum {
    NO_ACCESS,
    CAN_READ, 
    CAN_WRITE, 
    CAN_READ_WRITE
}