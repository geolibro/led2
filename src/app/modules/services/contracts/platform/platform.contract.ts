import { PlatformEnum } from "./platform.enum";

export interface PlatformContract {
    currentPlatform?: PlatformEnum;
    ready(): Promise<Event>;
    pause(): Promise<Event>;
    resume(): Promise<Event>;
    online(): Promise<Event>;
    offline(): Promise<Event>;
    backbutton?(): Promise<Event>;
    is(platform: PlatformEnum): boolean;
}
