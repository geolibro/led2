import { DownloadManagerService } from '../content-manager/download-manager.service';
import { ReaderContract } from './contracts/reader.contract';
import { Injectable, Inject } from '@angular/core';
import { WebWorkerService } from 'angular2-web-worker';

import { PageReaderNavigationContract } from './contracts/page-reader-navigation.contract';
import { EPubItemContract } from './contracts/e-pub-item.contract';
import { EPubSpineContract } from "./contracts/e-pub-spine.contract";
import { VaultDecrypterService } from '../vault/vault-decrypter.service';
import { BookDao } from '../../daos/book.dao';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { VaultToolsService } from '../vault/vault-tools.service';

@Injectable()
export class PageReaderService {
    private _worker: WebWorkerService = new WebWorkerService();
    private process: Promise<any>;
    private navigation: PageReaderNavigationContract = <PageReaderNavigationContract>{};
    private bookItemList: Array<any> = [];
    private teacherLayerPageList: Array<any> = [];

    private isReady: boolean = false;
    private pageNumber: number;
    private currentBookId: string;
    public reader: ReaderContract;

    constructor(
        @Inject('PlatformService') private platform: PlatformContract,
        private vault: VaultDecrypterService,
        private book: BookDao,
        private tools: VaultToolsService) {}

    load(reader: ReaderContract,
        bookId: string,
        finishHandler?: any) {

        this.reader = reader;
        this.reader.init(bookId, (pageList: Array<string>, teacherLayerPageList?: Array<String>) => {
            this.bookItemList = pageList;
            this.teacherLayerPageList = teacherLayerPageList;
            this.isReady = true;
            this.currentBookId = bookId;
            // tslint:disable-next-line:curly
            if (finishHandler) finishHandler();
        });
    }

    destroy() {
        this.reader.destroy();
        delete this.reader;
        delete this.bookItemList;
        this.isReady = false;
    }

    getPageNumber(): number {
        return this.pageNumber;
    }

    processPage(pageId?: number): Promise<any> {
        this.calculateNavigation(pageId);
        return new Promise(async (resolve, reject) => {

            const BOOK_KEY = await this.book.getKeyById(this.currentBookId);
            const xorKey = BOOK_KEY + this.tools.salt;
            this.vault.prepare(xorKey, this.currentBookId);

            if (!this.bookItemList[this.pageNumber]) {
                return reject({ error: 'NOT_FOUND', msg: 'Navigation not found. [pageId: ' + pageId + ']' });
            }
            this.reader.getFile(
                this.bookItemList[this.pageNumber],
                (file: any) => {
                    resolve(this.vault.bitwiseX(file, this.currentBookId));
                },
                reject
            );
        });
    }

    loadLegacyPageData(pageId: number, loadTeacherLayer: boolean) {

        return new Promise(async (resolve, reject) => {
            try {

                const BOOK_KEY = await this.book.getKeyById(this.currentBookId);
                const xorKey = BOOK_KEY + this.tools.salt;


                let loadTeacherLayerAndResolve = (pageBytes) => {

                    this.reader.getFile(
                        this.teacherLayerPageList[pageId],
                        (file: any) => {
                            this.vault.prepare(xorKey, this.currentBookId);
                            this.vault.bitwiseX(file, this.currentBookId)
                            return resolve([pageBytes, file]);
                        },
                        reject
                    );
                };

                let simpleResolve = (pageBytes) => {
                    return resolve([pageBytes, null]);
                };

                this.reader.getFile(
                    this.bookItemList[pageId],
                    (file: any) => {
                        let callBack: Function;

                        if (loadTeacherLayer) {
                            callBack = loadTeacherLayerAndResolve;
                        }
                        else {
                            callBack = simpleResolve;
                        }
                        this.vault.prepare(xorKey, this.currentBookId);
                        this.vault.bitwiseX(file, this.currentBookId)
                        callBack(file);
                    },
                    reject
                );
            } catch (error) {
                console.log(error);

                reject(error);
            }
        });
    }

    getPageByHref(pageName: string): Promise<any> {

        return new Promise((resolve, reject) => {
            const pageData = this.reader.findPage(pageName, this.bookItemList);

            this.reader.getFile(
                pageData.spineItem,
                async (file: any) => {
                    this.calculateNavigation(pageData.pageNumber);
                    const BOOK_KEY = await this.book.getKeyById(this.currentBookId);
                    const xorKey = BOOK_KEY + this.tools.salt;
                    this.vault.prepare(xorKey, this.currentBookId);
                    this.vault.bitwiseX(file, this.currentBookId);
                    return resolve({ bytes: file, anchor: pageData.anchor });
                },
                reject
            );
        });
    }

    forward(): Promise<any> {

        this.calculateNavigation(this.navigation.forward);

        return new Promise((resolve, reject) => {

            if (!this.bookItemList[this.navigation.forward]) {
                return reject({ error: 'NOT_FOUND', msg: 'Navigation not found. [pageId: ' + this.navigation.forward + ']' });
            }

            this.reader.getFile(
                this.bookItemList[this.navigation.forward],
                (file: any) => {
                    resolve(this.vault.bitwiseX(file, this.currentBookId));
                },
                reject
            );
        });
    }

    backward(): Promise<any> {

        this.calculateNavigation(this.navigation.backward);

        return new Promise((resolve, reject) => {

            if (!this.bookItemList[this.navigation.backward]) {
                return reject({ error: 'NOT_FOUND', msg: 'Navigation not found. [pageId: ' + this.navigation.backward + ']' });
            }

            this.reader.getFile(
                this.bookItemList[this.navigation.backward],
                (file: any) => {
                    this.calculateNavigation(this.navigation.backward);
                    resolve(this.vault.bitwiseX(file, this.currentBookId));
                },
                reject
            );
        });
    }

  getPage(bytes: Uint8Array, type: { type: string }, blob?: boolean): any {
    const file: Blob = new Blob([bytes], type);
    return !blob ? window.URL.createObjectURL(file) : file;
  }

    ready(): Promise<any> {
        return new Promise((resolve, reject) => {

            if (this.isReady) {
                return resolve(this.isReady);
            } else {
                setTimeout(() => {
                    return resolve(this.isReady);
                }, 5000);
            }

        });
    }

    /**
     * Calculate navigation
     * @param pageId
     */
    private calculateNavigation(pageId: number): void {
        let backward, forward;

        backward = pageId > 0 ? pageId - 1 : 0;
        forward = pageId + 1;

        this.pageNumber = pageId < 0 ? 0 : pageId;

    this.navigation['current']  = this.pageNumber;
    this.navigation['backward'] = backward;
    this.navigation['forward']  = forward;
  }
}
