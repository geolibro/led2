import { TestBed, inject } from '@angular/core/testing';

import { HotspotLegacyService } from './hotspot-legacy.service';

describe('HotspotLegacyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HotspotLegacyService]
    });
  });

  it('should be created', inject([HotspotLegacyService], (service: HotspotLegacyService) => {
    expect(service).toBeTruthy();
  }));
});
