import { Injectable, Inject } from '@angular/core';
import { HotspotContract } from './contracts/hotspot.contract';
import { AppEvent } from '../../events/app.event';
import { BookEvent } from '../../events/book/book.event';
import { HotspotTypeEnum } from './hotspot-type.enum';
import { AuthGuardService } from '../guard/auth-guard.service';
import { HttpService } from '../http/http.service';
import { HttpScaoResponse } from '../http/util/http-scao-response';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';

export interface AnnotationCommandContract {
    annotation_id: number;
    annotation_height: number;
    annotation_local_id: number;
    annotation_width: number;
    action: string;
    annotation_parent_id: number;
    annotation_parent_local_id: number;
    annotation_group_id: number;
    annotation_sender_id: number;
    annotation_book_id: string;
    annotation_page_id: number;
    annotation_x: number;
    annotation_receiver_id: number;
    annotation_y: number;
    annotation_type: string;
    annotation_description: string;
    annotation_book_version: number;
}

@Injectable()
export class UserNotesService {

    private userNoteHotspots;
    private currentBookId: string;
    private currentUser: string;
    private currentBookVersion: number;

    constructor(
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        private bookEvent: BookEvent,
        private authService: AuthGuardService,
        private http: HttpService) { }


    init(bookBaseManifest: any, userId: string): Promise<any> {

        this.currentUser = userId;
        this.currentBookId = bookBaseManifest.book_id;

        // seta número de versão p/ 1 caso livro tenha sido instalado anteriormente e não possua;
        // pode amenizar problemas em alguns casos. Após esta versão, os livros instalados já
        // possuem este campo corretamente
        this.currentBookVersion = bookBaseManifest.version_number ? bookBaseManifest.version_number : 1;

        return new Promise<any>((resolve, reject) => {
            this.loadData().then(() => {
                this.sync(false, true).then(resolve).catch((error) => {
                    let errorStr = 'error on initial notes synchronization! error: ' + JSON.stringify(error);
                    console.log(errorStr);
                    resolve();
                });
            }).catch(() => {
                reject();
            });
        });

    }

    public getPageHotspots(pageId: number): Array<HotspotContract> {
        let hotspots = new Array<HotspotContract>();

        let statusToCheck = ['synced', 'unsynced'];

        for (let status of statusToCheck) {
            let pageHotspots = this.userNoteHotspots[status][pageId];
            if (pageHotspots != null) {
                for (let hotspotId in pageHotspots) {
                    if (hotspotId != null) {
                        let hotspot = pageHotspots[hotspotId];
                        if (hotspot != null) {
                            hotspots.push(hotspot);
                        }
                    }
                }
            }
        }

        return hotspots;
    }

    public getAllUserHotspots(): Array<HotspotContract> {

        let statusToCheck = ['synced', 'unsynced'];

        return this.getUserHotspotsSet(statusToCheck);
    }


    public saveHotspot(hotspot: HotspotContract, shouldSync: boolean = true): Promise<any> {
        if (this.userNoteHotspots['unsynced'][hotspot.pageId] == null) {
            this.userNoteHotspots['unsynced'][hotspot.pageId] = {};
        }

        if (hotspot.id == null) {
            hotspot.id = this.getNextUserHotspotId();
        }
        else {
            // TODO: save as edition?
        }

        this.userNoteHotspots['unsynced'][hotspot.pageId][hotspot.id] = hotspot;

        return new Promise<any>((resolve, reject) => {
            if (shouldSync) {
                this.sync(true, true).then(resolve).catch(reject);
            }
            else {
                this.persist().then(resolve).catch(reject);
            }

        });
    }

    public deleteHotspot(hotspotId: number, pageId: number): Promise<any> {

        let hotspotsByPageId = this.userNoteHotspots;

        let hotspot: HotspotContract;

        if (this.userNoteHotspots['unsynced'][pageId] != null) {
            hotspot = this.userNoteHotspots['unsynced'][pageId][hotspotId];
            delete this.userNoteHotspots['unsynced'][pageId][hotspotId];
        }

        if (hotspot == null) {
            if (this.userNoteHotspots['synced'][pageId] != null) {
                hotspot = this.userNoteHotspots['synced'][pageId][hotspotId];
                delete this.userNoteHotspots['synced'][pageId][hotspotId];
            }
        }

        if (this.userNoteHotspots['deleted'] == null) {
            this.userNoteHotspots['deleted'] = [];
        }

        if (hotspot != null && ((hotspot.server_id | 0) != 0)) {
            if (this.userNoteHotspots['deleted'][pageId] == null) {
                this.userNoteHotspots['deleted'][pageId] = {};
            }
            this.userNoteHotspots['deleted'][pageId][hotspot.id] = hotspot;
        }

        return this.sync(true, false);
    }

    private loadData(): Promise<any> {
        return new Promise<any>((resolve, reject) => {

            let path = 'users/' + this.currentUser + '/';
            this.fileSystem.request(path + '/user-notes-' + this.currentBookId + '.json').readFile().toJSON(json => {
                this.userNoteHotspots = json;
                resolve();
            }, error => {
                this.userNoteHotspots = this.getinitialUserNoteHotpots();
                this.persist().then(resolve).catch((error) => {
                    let errorStr = 'Erro ao salvar versão inicial dos hotspots de usuários! (' + JSON.stringify(error) + ')';
                    reject(errorStr);
                });
            });
        });
    }

    private getinitialUserNoteHotpots() {
        let userNotes = {
            unsynced: {},
            synced: {},
            deleted: {}
        };
        return userNotes;
    }

    private getUserHotspotsSet(desiredStatuses: Array<string>): Array<HotspotContract> {
        let hotspots = new Array<HotspotContract>();

        for (let status of desiredStatuses) {
            for (let pageId in this.userNoteHotspots[status]) {

                let pageHotspots = this.userNoteHotspots[status][pageId];

                if (pageHotspots != null) {
                    for (let hotspotId in pageHotspots) {
                        let hotspot: HotspotContract = pageHotspots[hotspotId];
                        if (hotspot != null) {
                            hotspots.push(hotspot);
                        }
                    }
                }
            }
        }

        return hotspots;
    }

    sync(sendNotes: boolean, receiveNotes: boolean): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            if (sendNotes) {

                this.sendNotes().then(() => {
                    if (receiveNotes) {
                        this.receiveNotes().then(resolve).catch(reject);
                    }
                    else {
                        resolve();
                    }
                }).catch(reject);
            }
            else {
                if (receiveNotes) {
                    this.receiveNotes().then(resolve).catch(reject);
                }
            }
        });
    }

    private sendNotes(): Promise<any> {

        return new Promise<any>(async (resolve, reject) => {
            let errorText: string;
            try {
                let hotspotsToSend = this.getUserHotspotsSet(['unsynced']);
                let hotspotsToDelete = this.getUserHotspotsSet(['deleted']);

                let commandsToSend = new Array<AnnotationCommandContract | { action: string, annotation_id: number }>();

                for (let hotspot of hotspotsToSend) {
                    let serverCommand = this.getCommandFromHotspot(hotspot, 'annotation_add');

                    commandsToSend.push(serverCommand);
                }

                for (let hotspot of hotspotsToDelete) {
                    let serverCommand = {
                        action: 'annotation_del',
                        annotation_id: hotspot.server_id
                    };
                    commandsToSend.push(serverCommand);
                }

                let ws309Params = {
                    operation: 'J318',
                    login: this.authService.user().login,
                    password: this.authService.user().password,
                    device_id: await this.authService.getDevice().getUniqueIdentifier(),
                    last_annotation_synced: '0',
                    get_updates: '0',
                    led_user_id: this.authService.user().led_user_id,
                    led_book_id: this.currentBookId,
                    annotations: commandsToSend
                };

                const ws309Data = JSON.stringify(ws309Params);

                console.log('sync -> sending: ' + ws309Data);

                const ws309URL = this.http.getURL('scao', 'ws309');

                const ws309Response: HttpScaoResponse = await this.http.makeRequest(ws309URL, ws309Data);
                if (ws309Response.hasErrors()) {
                    errorText = 'sync -> sendNotes: erro na chamada do WS309: ' + ws309Response.getErrorMessage();
                }
                else {
                    let savedAnnotations = ws309Response['body']['saved_annotations'];

                    if (savedAnnotations != null) {

                        console.log('server -> savedAnnotations: ' + JSON.stringify(savedAnnotations));
                        this.userNoteHotspots['deleted'] = {};

                        for (let savedAnnotation of savedAnnotations) {
                            if (savedAnnotation.action == 'annotation_add') {
                                let localHospotId = parseInt(savedAnnotation.local_id);
                                let serverId = parseInt(savedAnnotation.server_id);
                                let hotspotPageId = parseInt(savedAnnotation.annotation_page_id);

                                let syncedHotspot: HotspotContract = this.userNoteHotspots['unsynced'][hotspotPageId][localHospotId];
                                if (syncedHotspot != null) {
                                    if (this.userNoteHotspots['synced'][hotspotPageId] == null) {
                                        this.userNoteHotspots['synced'][hotspotPageId] = {};
                                    }

                                    syncedHotspot.server_id = serverId;
                                    this.userNoteHotspots['synced'][hotspotPageId][localHospotId] = syncedHotspot;
                                    delete this.userNoteHotspots['unsynced'][hotspotPageId][localHospotId];
                                }
                            }
                        }
                    }
                    else {
                        console.log('server -> no saved_annotations received!');
                    }
                }
            }
            catch (error) {
                errorText = 'sync -> sendNotes: excecao no envio de notas: ' + JSON.stringify(error);
            }
            finally {
                if (errorText != null) {
                    reject(errorText);
                }
                else {
                    this.persist().then(resolve).catch(reject);
                }
            }

        });
    }

    private receiveNotes(): Promise<any> {
        return new Promise(async (resolve, reject) => {

            let errorText: string;
            try {
                let ws316Params = {
                    operation: 'J377',
                    login: this.authService.user().login,
                    password: this.authService.user().password,
                    led_user_id: this.authService.user().led_user_id,
                    device_id: await this.authService.getDevice().getUniqueIdentifier(),
                    book_led_id: this.currentBookId
                };

                const ws316Data = JSON.stringify(ws316Params);
                const ws316URL = this.http.getURL('scao', 'ws316');

                const ws316Response: HttpScaoResponse = await this.http.makeRequest(ws316URL, ws316Data);
                if (ws316Response.hasErrors()) {
                    errorText = 'sync -> receive notes: erro na chamada do WS316: ' + ws316Response.getErrorMessage();
                } else {
                    let serverAnnotations: Array<{ content: AnnotationCommandContract }> = ws316Response['body']['private_annotations'];

                    if (serverAnnotations != null) {
                        this.userNoteHotspots['synced'] = {};
                    }

                    for (let i = 0; i < serverAnnotations.length; i++) {
                        let annotationCommand = serverAnnotations[i];
                        let hotspot = this.getHotspotFromServerAnnotation(annotationCommand);

                        this.storeHotspot(hotspot, 'synced');

                    }
                }
            }
            catch (error) {
                errorText = 'sync -> receiveNotes: excecao no envio de notas: ' + JSON.stringify(error);
            }
            finally {
                if (errorText != null) {
                    reject(errorText);
                }
                else {
                    this.persist().then(resolve).catch(reject);
                }
            }
        });
    }

    private getHotspotFromServerAnnotation(annotationCommand: { content: AnnotationCommandContract }): HotspotContract {
        let annotationContent = annotationCommand.content;
        let hotspot = {
            server_id: annotationContent.annotation_id,
            id: annotationContent.annotation_local_id,
            book_id: annotationContent.annotation_book_id,
            x: annotationContent.annotation_x,
            y: annotationContent.annotation_y,
            width: annotationContent.annotation_width,
            height: annotationContent.annotation_height,
            pageId: annotationContent.annotation_page_id,
            description: annotationContent.annotation_description,
            type: HotspotTypeEnum[annotationContent.annotation_type]
        };
        return hotspot;
    }

    private storeHotspot(hotspot: HotspotContract, syncStatus: string) {

        let hotspotsByPageId = this.userNoteHotspots[syncStatus];

        if (hotspotsByPageId[hotspot.pageId] == null) {
            hotspotsByPageId[hotspot.pageId] = {};
        }

        if (hotspot.id == null) {
            hotspot.id = this.getNextUserHotspotId();
        }

        hotspotsByPageId[hotspot.pageId][hotspot.id] = hotspot;
    }


    private persist() {
        return new Promise<any>((resolve, reject) => {
            const path = 'users/' + this.currentUser + '/user-notes-' + this.currentBookId + '.json';
            this.fileSystem.request(path).writeFile(new Buffer(JSON.stringify(this.userNoteHotspots)), resolve, (error) => {
                let errorStr = 'Error while persisting user annotations: ' + JSON.stringify(error);
                reject(errorStr);
            });
        });
    }


    private getNextUserHotspotId() {

        let maxId = 0;
        let allHotspots = this.getAllUserHotspots();
        for (let hotspot of allHotspots) {
            let hotspotId = hotspot.id | 0;
            maxId = (hotspotId > maxId) ? hotspotId : maxId;
        }

        return maxId + 1;
    }

    private getCommandFromHotspot(hotspot: HotspotContract, actionCmd: string): AnnotationCommandContract {

        let serverCommand: AnnotationCommandContract = {

            annotation_id: hotspot.server_id ? hotspot.server_id : 0,
            annotation_height: hotspot.height,
            annotation_parent_local_id: 0,
            annotation_local_id: hotspot.id,
            annotation_width: hotspot.width,
            action: actionCmd,
            annotation_parent_id: 0,
            annotation_group_id: null,
            annotation_sender_id: parseInt(this.authService.user().led_user_id),
            annotation_book_id: this.currentBookId,
            annotation_page_id: hotspot.pageId,
            annotation_x: hotspot.x,
            annotation_receiver_id: parseInt(this.authService.user().led_user_id),
            annotation_y: hotspot.y,
            annotation_type: HotspotTypeEnum[hotspot.type],
            annotation_description: hotspot.description,
            annotation_book_version: this.currentBookVersion
        };

        return serverCommand;
    }


}
