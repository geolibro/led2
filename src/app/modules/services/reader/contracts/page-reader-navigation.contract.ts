export interface PageReaderNavigationContract {
    backward?: number;
    current: number;
    forward: number;
}
