import { EPubDocumentCreatorContract } from './e-pub-document-creator.contract';
export interface EPubMetadataContract {
    dc: EPubDocumentCreatorContract;
    meta: Array<{property?: string, value: string}>;
}
