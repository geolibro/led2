export interface EPubDocumentCreatorContract {
    identifier: {id?: string, value: string};
    title: {id?: string, value: string};
    language: {id?: string, value: string};
    date: string;
    creator: {id?: string, value: string};
    contributor: Array<string>;
    publisher: string;
    rights: string;
}
