export interface EPubItemContract {
    id: string;
    properties?: string;
    mediaType: string;
    href: string;
}
