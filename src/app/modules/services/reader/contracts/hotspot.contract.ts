import {HotspotTypeEnum} from '../hotspot-type.enum';

export interface HotspotResource {
  parameters: string,
  autoplay: number,
  url: string
}

export interface HotspotContract {
  server_id?: number,
  id?: number,
  book_id?: string,
  x: number
  y: number
  width: number
  height: number
  pageId?: number
  description: string
  type: HotspotTypeEnum;

  resource?: HotspotResource;
  


}
