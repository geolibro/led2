import { ReaderContract } from './contracts/reader.contract';
import { BookReaderCollectionContract } from './contracts/book-reader-collection.contract';
import { WebWorkerService } from 'angular2-web-worker';
import { Injectable, Inject } from '@angular/core';
import { ContentManagerService } from '../content-manager/content-manager.service';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
import { LayoutFileSystemEnum } from '../contracts/file-system/layout-file-system.enum';

@Injectable()
export class LegacyReaderService implements ReaderContract {
    booksPATH: any = '';
    private bookId: string;
    private pageList: Array<string> = [];
    private _worker: WebWorkerService;
    private installedBookManifest: any;

    init(bookId: string, handler: any): void {
        this.bookId = bookId;

        this.loadBookPageList((pageList: Array<any>, teacherPageList: Array<any>) => {
            this.pageList = this.ordenatePageListThread(pageList);
            handler(this.pageList, teacherPageList);
        });

    }

    destroy() {
        //
    }

    getBookId(): string {
        return this.bookId;
    }

    getBasePath(path?: string): string {
        return this.booksPATH + this.bookId + '/' + path;
    }

    getFile(fileName: string, fileHandler: any, errorHandler?: any): void {
        this.fileSystem.request(fileName, LayoutFileSystemEnum.NO_ROOT_PATH)
            .readFile()
            .get()
            .then(result => fileHandler(result)).catch(errorHandler);
    }

    getCollection(): BookReaderCollectionContract {
        const collection: any = {};
        collection.pages = this.pageList;
        return collection;
    }

    private loadBookPageList(handler?: any, errorHandler?: any) {
        this.contentManager.getInstalledBookManifest(this.bookId).then((bookData) => {
            this.installedBookManifest = bookData;
            let bookPageCount = parseInt(bookData.book.book_page_count);
            let pageNumberVsIdMap = bookData.page_number_id_map;

            let pageURList = [];
            for (let pageNumber = 1; pageNumber <= bookPageCount; pageNumber++) {
                let pageId = pageNumberVsIdMap[pageNumber];
                pageURList[pageId] = this.fileSystem.getDirectory() + this.bookId + '/pages/page-' + pageId + '.imgblob';
            }

            let teacherpageURLList = [];
            if (bookData.teacher_layers_urls != null) {
                for (let teacherObj of bookData.teacher_layers_urls) {
                    teacherpageURLList[teacherObj.page_id] = this.fileSystem.getDirectory() + teacherObj.url;
                }
            }

            handler(pageURList, teacherpageURLList);

        }).catch(errorHandler);
    }

    private ordenatePageListThread(input: Array<any>) {
        const pageList: Array<string> = input;
        const data: Array<any> = [];

        pageList.forEach(page => {
            const pageItem = page.match(/\d+(?=[^\d+]*$)/g);
            data[pageItem[0]] = page;
        });

        return data;
    }

    getInstalledManifest() {
        return this.installedBookManifest;
    }

    constructor(private contentManager: ContentManagerService,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract) {
            this._worker = new WebWorkerService();
        }

}
