import { Injectable, Inject } from '@angular/core';
import { BookEvent } from "../../events/book/book.event";
import { HotspotContract } from './contracts/hotspot.contract';
import { OED } from '../content-manager/installer.service';
import { HotspotTypeEnum } from './hotspot-type.enum';
import { DownloadManagerService } from '../content-manager/download-manager.service';
import { BookDao } from '../../daos/book.dao';
import { UserNotesService } from './user-notes.service';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { VaultToolsService } from '../vault/vault-tools.service';
import { ContentManagerService } from '../content-manager/content-manager.service';


@Injectable()
export class HotspotLegacyService {

    private currentUser: string;
    private currentPageId: number;
    private currentBookId: string;
    private currentBookManifest: any;
    private pageManifestURLsById: any;
    private currentPageBookHotpots: Array<HotspotContract>;
    private hotspotDecriptionKey: string;
    private wordsHotspotsByPageId: any;

    constructor(@Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        @Inject('PlatformService') private platform: PlatformContract,
        private bookEvent: BookEvent,
        private book: BookDao,
        private userNotes: UserNotesService,
        private tools: VaultToolsService,
        private contentManager: ContentManagerService) {
    }

    public init(bookBaseManifest: any, currentUser: string): Promise<any> {
        this.currentPageId = 0;
        this.currentUser = currentUser;
        this.currentBookManifest = bookBaseManifest;

        return new Promise<any>(async (resolve, reject) => {
            this.currentBookId = bookBaseManifest.book_id;
            const BOOK_KEY = await this.book.getKeyById(this.currentBookId);
            this.hotspotDecriptionKey = BOOK_KEY + this.tools.salt;

            Promise.all([
                this.loadBookHotspots(),
                this.userNotes.init(bookBaseManifest, currentUser)
            ]).then((response) => {
                console.log(response);
                this.bookEvent.EchoAnnotationsUpdated.emit(true);
                resolve(true);
            }).catch((error) => {
                reject('erro inicializando HotspotLegacyService: ' + JSON.stringify(error));
            });
        });
    }

    public setCurrentPageId(pageId: number): Promise<any> {
        this.currentPageId = pageId;

        return new Promise<any>((resolve, reject) => {
            this.fileSystem.request(this.pageManifestURLsById[this.currentPageId]).readFile().toJSON(pageManifestObj => {
                this.currentPageBookHotpots = [];
                for (let oed of pageManifestObj.oeds) {
                    this.currentPageBookHotpots.push(this.getHotspotFromOED(oed));
                }
                resolve();
            })
        });
    }

    public getAllUserHotspots(): Array<HotspotContract> {
        return this.userNotes.getAllUserHotspots();
    }

    public getUserHotspots(): Array<HotspotContract> {
        return this.userNotes.getPageHotspots(this.currentPageId);
    }

    public getOEDHotposts(): Array<HotspotContract> {
        return this.currentPageBookHotpots;
    }

    public getHotspotsForWord(word: string): any {
        const indexes = Object.keys(this.wordsHotspotsByPageId);
        let pageObjects = [];
        indexes.map((valor, index) => {
            const regWord = new RegExp(word.toLowerCase(), 'g');
            if (valor.match(regWord)) {
                const keyWord = indexes[index];
                pageObjects = pageObjects.concat(this.wordsHotspotsByPageId[keyWord]);
            }
        });
        return pageObjects;

    }

    saveUserHotspot(hotspot: HotspotContract): Promise<any> {
        hotspot.pageId = this.currentPageId;
        return this.userNotes.saveHotspot(hotspot);
    }

    public deleteUserHotspot(hotspotId: number): Promise<any> {
        return this.userNotes.deleteHotspot(hotspotId, this.currentPageId);
    }

    decryptHotspot(hotspot: any): HotspotContract {
        let decryptionIndex = hotspot.id % this.hotspotDecriptionKey.length;
        let decryptionOffset = (this.hotspotDecriptionKey.charCodeAt(decryptionIndex)) / 100.0;
        hotspot.x -= decryptionOffset;
        hotspot.y -= decryptionOffset;
        hotspot.width -= decryptionOffset;
        hotspot.height -= decryptionOffset;
        return hotspot;
    }


    public loadWordHotpots(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let dataFilePath = this.currentBookId + '/word_hotspots.dat';
            this.fileSystem.exists(dataFilePath, async (exists) => {
                if (exists) {
                    // status do livro p/ determinar se reindexação é necessária
                    let bookStatusData = await this.contentManager.getBookStatusData();
                    let bookComplete = (bookStatusData[this.currentBookId].book_status == 'baixado');

                    try {
                        this.wordsHotspotsByPageId = await this.createIndexedWordDictionary(this.currentBookId);

                        if (bookComplete) {
                            try {
                                await this.fileSystem.request(dataFilePath).delete();
                            }
                            catch (err) {
                                console.log('loadWordHotspots -> erro ao deletar arquivo-fonte do dicionário!');
                            }
                        }

                        resolve(true);

                    }
                    catch (error) {
                        let errorStr = 'loadWordHotspots -> erro ' + error.toString();
                        reject(errorStr);
                    }


                } else {
                    // raw data (word_hotspots.dat) was deleted (or never existed);
                    // this means that the json file should already have the complete data
                    // or the book has no dictionary at all

                    let wordsJSONFilePath = this.currentBookId + '/word_hotspot_map.json'
                    this.fileSystem.exists(wordsJSONFilePath, async (exists) => {
                        if (exists) {
                            this.fileSystem.request(wordsJSONFilePath).readFile().toJSON(
                                (json) => {
                                this.wordsHotspotsByPageId = json;
                                resolve(true);
                            }, reject);
                        }
                        else {
                            this.wordsHotspotsByPageId = {};
                            resolve(false);
                        }
                    });

                }
            });
        });
    }

    private loadBookHotspots(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.pageManifestURLsById = {};
            try {
                if (this.currentBookManifest.pages_manifest_urls != null) {
                    for (let i = 0; i < this.currentBookManifest.pages_manifest_urls.length; i++) {
                        let pageId = this.currentBookManifest.pages_manifest_urls[i].page_id;
                        this.pageManifestURLsById[pageId] = this.currentBookManifest.pages_manifest_urls[i].url;
                    }
                }
                resolve()
            }
            catch (err) {
                reject('Error loading book hotspots (' + JSON.stringify(err) + ')');
            }
        });
    }

    public async createIndexedWordDictionary(bookId: string = this.currentBookId) {
        let shouldCreateIndex = false;

        return new Promise<any>(async (resolve, reject) => {

            let indexedWords = {};

            this.fileSystem.request(bookId + '/word_hotspots.dat').readFile().toString((dataStr: string) => {
                if (dataStr.length > 0) {
                    dataStr = dataStr.substring(0, dataStr.length - 2);
                }
                dataStr = '[' + dataStr + ']';
                let dataJSON = JSON.parse(dataStr);

                for (let i = 0; i < dataJSON.length; i++) {
                    let dataEntry = dataJSON[i];
                    let wordKeys = Object.keys(dataEntry);
                    for (let k = 0; k < wordKeys.length; k++) {
                        let word = wordKeys[k];
                        if (indexedWords[word] == null) {
                            indexedWords[word] = {};
                        }
                        let wordPages = Object.keys(dataEntry[word]);
                        for (let pageNumber of wordPages) {
                            if (indexedWords[word][pageNumber] == null) {
                                indexedWords[word][pageNumber] = [];
                            }
                            let wordHotspots = dataEntry[word][pageNumber];
                            indexedWords[word][pageNumber] = wordHotspots;
                        }
                    }
                }
                this.fileSystem.request(bookId + '/word_hotspot_map.json').writeFile(new Buffer(JSON.stringify(indexedWords)),
                    success => {
                        resolve(indexedWords)
                    }, reject);

            }, reject);

        });
    }

    private getHotspotFromOED(oed: OED): HotspotContract {
        const oedUrl = (oed.url) ? oed.url.replace(/\\/g, "/") : '';

        let hotspot: HotspotContract = {
            id: oed.id,
            book_id: this.currentBookId,
            type: HotspotTypeEnum[oed.type],

            x: oed.x,
            y: oed.y,
            width: oed.w,
            height: oed.h,

            description: oed.description,

            resource: {
                parameters: oed.parameters,
                autoplay: oed.autoplay,
                url: oedUrl
            }
        }

        if (!hotspot.resource.url.startsWith('/')) {
            hotspot.resource.url = '/' + hotspot.resource.url;
        }

        return this.decryptHotspot(hotspot);
    }
}
