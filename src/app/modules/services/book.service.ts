
import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { Ws007Contract } from '../../requests/contracts/ws/ws007.contract';
import { Ws013Contract } from '../../requests/contracts/ws/ws013.contract';
import { Ws010Contract } from '../../requests/contracts/ws/ws010.contract';
import { Ws021Contract } from '../../requests/contracts/ws/ws021.contract';
import { LegacyBookContract } from '../../contracts/legacy-book.contract';
import { LogProvider } from '../../providers/log.provider';
import {Ws102Contract} from "../../requests/contracts/ws/ws102.contract";

@Injectable()
export class BookService extends AbstractService {
  private Log: LogProvider = LogProvider.getInstance('BookService');

  getBookCover(book: LegacyBookContract) {
    return this.fileSystem.getDirectory() + 'folders/' + book.book_led_id + '.jpg';
  }

  getBookCoverURL(book: LegacyBookContract) {
    const cdnURL = book.download_urls[0];
    let extraSeparator = '';
    if (!cdnURL.endsWith('/')) {
      extraSeparator = '/';
    }
    return (
      cdnURL +
      extraSeparator +
      book.book_led_id +
      '/' +
      book.last_version +
      '/' +
      book.book_led_id +
      '.jpg'
    );
  }

  /**
   * Process book folder download check if doesn't exists in file system
   * write book list on file system books.json
   *
   * @param {Array<LegacyBookContract>} bookList
   * @memberof BookService
   */
  persistBookList(bookList: Array<LegacyBookContract>): void {
    this.writeBooksJson(bookList);
  }

  /**
   * Webservice: (SCA) WS007
   * http://dacsrv/wiki/index.php/SCAO#WS007_-_Obter_lista_das_obras_do_usu.C3.A1rio
   *
   * @param {Ws007Contract} params
   * @returns {Promise<any>}
   * @memberof BookService
   */
  getBookList(params: Ws007Contract): Promise<any> {
    return new Promise((resolve, reject) => {
      this.buildUrl('ws007')
        .makeRequest(params)
        .subscribe((response: any) => {
          resolve(response.json());
        }, reject);
    });
  }

  /**
   * Webservice: (SCL) WS102
   * http://dacsrv/wiki/index.php/SCL#WS102_-_Obter_chave_de_criptografia_atrav.C3.A9s_de_licen.C3.A7a
   *
   * @param {Ws102Contract} params
   * @returns {Promise<any>}
   * @memberof BookService
   */
  getBookKey(params: Ws102Contract): Promise<any> {
    return new Promise((resolve, reject) => {
      this.buildUrl('ws102')
        .makeRequest(params)
        .subscribe(response => resolve(response.json()), reject);
    });
  }

  /**
     * Webservice: (SCAO) WS013
     * http://dacsrv/wiki/index.php/SCAO#WS013_-_Informar_a_instala.C3.A7.C3.A3o_de_uma_obra_no_dispositivo
     *
     * @param {Ws013Contract} params
     * @returns {Promise<any>}
     * @memberof BookService
     */
  notifyBookInstallation(params: Ws013Contract): Promise<any> {
    return new Promise((resolve, reject) => {
      this.buildUrl('ws013')
        .makeRequest(params)
        .subscribe((response: any) => {
          resolve(response.json());
        }, reject);
    });
  }

  /**
     * Webservice: (SCAO) WS010
     * http://dacsrv/wiki/index.php/SCAO#WS010_-_Desinstalar_obra_do_dispositivo
     *
     * @param {Ws010Contract} params
     * @returns {Promise<any>}
     * @memberof BookService
     */
  notifyBookUninstallation(params: Ws010Contract): Promise<any> {
    return new Promise((resolve, reject) => {
      this.buildUrl('ws010')
        .makeRequest(params)
        .subscribe((response: any) => {
          resolve(response.json());
        }, reject);
    });
  }

  /**
   * Webservice: (SCAO) WS021
   * http://dacsrv/wiki/index.php/SCAO#WS021_-_Obter_anota.C3.A7.C3.B5es_externas
   * [Utilizado no 'alfaconnotes']
   *
   * @param {Ws021Contract} params
   * @returns {Promise<any>}
   * @memberof BookService
   */
  requestExternalAnnotations(params: Ws021Contract): Promise<any> {
    return new Promise((resolve, reject) => {
      this.buildUrl('ws021')
        .makeRequest(params)
        .subscribe(resolve, reject);
    });
  }

  private downloadBooksFolder(
    booksFolder: Array<LegacyBookContract>
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        if (booksFolder.length > 0) {

          this.fileSystem.request('folders/')
          .createDir()
          .then(() => {
            booksFolder.forEach(book => {
              // const Downloader: FileTransfer = new FileTransfer();
              const URL = this.getBookCoverURL(book);
              const fileName = book.book_led_id + '.jpg';

              this.downloadService.download(
                encodeURI(URL),
                this.fileSystem.getDirectory() + '/folders/' + fileName,
                (file: any) => {},
                reject,
                true
            );
            });

          })
          .catch(exception => {
            console.log(exception);
          });
        }

        resolve(true);
      } catch (exception) {
        reject(exception);
      }
    });
  }

  private writeBooksJson(bookList: Array<LegacyBookContract>) {
    if (bookList.length === 0) {
      return;
    }

    this.fileSystem.request('books.json')
                    .writeFile(new Buffer(JSON.stringify(bookList)), success => {

                    this.downloadBooksFolder(bookList)
                          .then(() => {})
                          .catch((error: any) => {
                            this.Log.save(error);
                          });

                    }, error => {
                      this.Log.save(error);
                    });
  }

  private arrayDiff(array1, array2) {
    const a = [],
      diff = [];

    for (let i = 0; i < array1.length; i++) {
      a[array1[i]] = true;
    }

    for (let i = 0; i < array2.length; i++) {
      if (a[array2[i]]) {
        delete a[array2[i]];
      } else {
        a[array2[i]] = true;
      }
    }

    // tslint:disable-next-line:forin
    for (const k in a) {
      diff.push(k);
    }

    return diff;
  }

}
