import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";
import { WebWorkerService } from "angular2-web-worker";
import { BookService } from "./book.service";
import { ContentManagerService } from "./content-manager/content-manager.service";
import { DownloadManagerService } from "./content-manager/download-manager.service";
import { InstallerService } from "./content-manager/installer.service";
import { InputService } from "./input.service";
import { EpubReaderService } from "./reader/epub-reader.service";
import { LegacyReaderService } from "./reader/legacy-reader.service";
import { PackageOpfService } from "./reader/package-opf.service";
import { PageReaderService } from "./reader/page-reader.service";
import { HotspotLegacyService } from "./reader/hotspot-legacy.service";
import { VaultDecrypterService } from "./vault/vault-decrypter.service";
import { UserSettingsService } from "./user-settings.service";
import { VaultGeneratorService } from "./vault/vault-generator.service";
import { SystemUpdateService } from "./packager/system-update.service";
import { AuthGuardService } from "./guard/auth-guard.service";
import { DeviceGuardService } from "./guard/device-guard.service";
import { HttpService } from "./http/http.service";
import { AuthStorageService } from "./guard/auth-storage.service";
import { UserNotesService } from "./reader/user-notes.service";
import { MediaService } from "./media/media.service";
import { VaultToolsService } from "./vault/vault-tools.service";
import { ShelfService } from "./shelf/shelf.service";
import { ShelfResolverService } from './shelf/shelf-resolver.service';

@NgModule({
    imports: [CommonModule, HttpModule],
    providers: [
        BookService,
        ContentManagerService,
        InstallerService,
        DownloadManagerService,
        InputService,
        WebWorkerService,
        EpubReaderService,
        PackageOpfService,
        LegacyReaderService,
        HotspotLegacyService,
        UserNotesService,
        VaultDecrypterService,
        VaultGeneratorService,
        PageReaderService,
        UserSettingsService,
        SystemUpdateService,
        AuthStorageService,
        AuthGuardService,
        DeviceGuardService,
        MediaService,
        HttpService,
        VaultToolsService,
        ShelfService,
        ShelfResolverService
    ]
})
export class ServicesModule {}
