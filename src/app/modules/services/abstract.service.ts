import { WebServiceListContract } from './../../contracts/web-service-list.contract';
import { Injectable, Inject } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { UserLoginFieldsContract } from '../../contracts/user-login-fields.contract';
import { UserLoggedFieldsContract } from '../../contracts/user-logged-fields.contract';
import { Ws015Contract } from '../../requests/contracts/ws/ws015.contract';
import { Ws007Contract } from '../../requests/contracts/ws/ws007.contract';
import * as CryptoJS from 'crypto-js';
import { BookService } from './book.service';
import { AuthGuardService } from './guard/auth-guard.service';
import { PlatformContract } from './contracts/platform/platform.contract';
import { DeviceContract } from './contracts/device/device.contract';
import { LedFileSystemContract } from './contracts/file-system/led-file-system.contract';
import { DownloadContract } from './contracts/download/download.contract';
import { NetworkContract } from './contracts/network/network.contract';

@Injectable()
export abstract class AbstractService {
    protected LoggedUserData: UserLoggedFieldsContract = <UserLoggedFieldsContract>{};
    protected RequestUuidParams: Ws015Contract = <Ws015Contract>{};
    protected LoginRequestParams: Ws007Contract = <Ws007Contract>{};

    private webServices: Array<WebServiceListContract>;
    private params: Object;
    private requestURL: string;

    constructor(
        protected http: Http,
        @Inject('PlatformService') protected platform: PlatformContract,
        @Inject('DeviceService') protected device: DeviceContract,
        @Inject('FileSystemService') protected fileSystem: LedFileSystemContract,
        @Inject('DownloadService') protected downloadService: DownloadContract,
        @Inject('NetworkService') protected network: NetworkContract,
        protected auth: AuthGuardService) {

        this.webServices = [
            { name: 'ws001', path: 'webservice/publisher/ws001', server: 'scao' },
            { name: 'ws007', path: 'webservice/reader/ws007', server: 'scao' },
            { name: 'ws013', path: 'webservice/reader/ws013', server: 'scao' },
            { name: 'ws010', path: 'webservice/reader/ws010', server: 'scao' },
            { name: 'ws015', path: 'webservice/reader/ws015', server: 'scao' },
            { name: 'ws021', path: 'webservice/reader/ws021', server: 'scao' },
            { name: 'ws312', path: 'compartilhamento/webservice/ws312', server: 'scao' },
            { name: 'ws102', path: 'webservice/webservice/ws102', server: 'scl' }
        ];
    }

    /**
     * @param {boolean} [decryptPass=true]
     * @returns {Ws007Contract}
     * @memberof AbstractService
     */
    getLoginRequestParams(decryptPass: boolean = true): Ws007Contract {
        if (decryptPass) {
            this.LoginRequestParams.password = this.decryptUserPassword(this.LoginRequestParams.password);
        }
        return this.LoginRequestParams;
    }

    /**
     * @Helper
     * @param {string} password
     * @returns {string}
     * @memberof AbstractService
     */
    decryptUserPassword(password: string): string {
        const passwordBytes = CryptoJS.AES.decrypt(password,
            this.auth.user().led_user_id
        );

        return passwordBytes.toString(CryptoJS.enc.Utf8);
    }

    /**
     * Helper method to handle with several parameters
     *
     * Data set: LoggedUserData / RequestUuidParams / LoginRequestParams
     * build data to mount requests before send
     *
     * @public
     * @param {UserLoginFieldsContract} data
     * @memberof AuthService
     */
    prepareLoginRequest(data: UserLoginFieldsContract) {
        this.LoginRequestParams.operation = 'J017';
        this.LoginRequestParams.login = data.login;
        this.LoginRequestParams.password = data.password;
        this.LoginRequestParams.supported_book_formats = environment.supported_book_formats;
    }

    /**
     * Prepare device info for request by core device cordova implementation
     * @public
     * @memberof AuthService
     */
    prepareDeviceInfoRequest() {
        Object.assign(this.LoginRequestParams, this.device.prepareDeviceInfoRequest());
    }

    /**
     * @private
     * @param {string} webservice
     * @returns
     * @memberof AbstractService
     */
    private getBaseUrl(webservice: string) {
        const ws = this.getWebService(webservice);
        return environment.getUrl(ws.server) + ws.path;
    }

    /**
     * @private
     * @param {string} name
     * @returns {WebServiceListContract}
     * @memberof AbstractService
     */
    private getWebService(name: string): WebServiceListContract {
        const webService = this.webServices.filter(
            function (element) {
                return element.name === this.value;
            },
            { value: name }
        );

        return webService[0];
    }

    /**
     * @protected
     * @param {string} [method='POST']
     * @returns
     * @memberof AbstractService
     */
    protected getRequestOptions(method: string = 'POST') {
        const headers: any = new Headers({
            // 'Content-Type': 'multipart/form-data; charset=UTF-8'
            'Content-Type': 'application/json; charset=UTF-8'
        });
        const options: any = new RequestOptions({
            method: method,
            headers: headers
        });

        return options;
    }

    /**
     * @protected
     * @param {Object} params
     * @returns {Observable<any>}
     * @memberof AbstractService
     */
    protected makeRequest(params: Object): Observable<any> {
        if (
            this.requestURL === '' ||
            this.requestURL == null ||
            this.requestURL === undefined
        ) {
            throw new Error(
                'You have to build requestURL first. Use method buildUrl.'
            );
        }
        const body = JSON.stringify(params);

        return this.http.post(this.requestURL, body, this.getRequestOptions());
    }

    /**
     * This method can be used to build url according webservice name
     * @protected
     * @param {string} webservice
     * @returns
     * @memberof AbstractService
     */
    protected buildUrl(webservice: string) {
        this.requestURL = this.getBaseUrl(webservice);
        return this;
    }

    /**
       * Webservice: (SCAO) WS015
       * @param {Ws015Contract} params
       * @returns {Promise<any>}
       * @memberof BookService
       */
    protected getUuid(params: Ws015Contract): Promise<any> {
        return new Promise((resolve, reject) => {
            this.buildUrl('ws015')
                .makeRequest(params)
                .subscribe((response) => {
                    resolve(response.json().UID);
                }, reject);
        });
    }
}
