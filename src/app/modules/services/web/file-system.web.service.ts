import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
import { LayoutFileSystemEnum } from '../contracts/file-system/layout-file-system.enum';
import { DiskUsageContract } from '../contracts/file-system/disk-usage.contract';

export class FileSystemWebService implements LedFileSystemContract {
  request(path?: string, layout?: LayoutFileSystemEnum) {
    throw new Error("Method not implemented.");
  }
  exists(path: string, handler: Function): void {
    throw new Error("Method not implemented.");
  }
  readDir(options?: string | object): Promise<string[] | any[]> {
    throw new Error("Method not implemented.");
  }
  writeFile(data: any, successHandler: Function, errorHandler: Function) {
    throw new Error("Method not implemented.");
  }
  rename(oldPath: string, newPath: string): void {
    throw new Error("Method not implemented.");
  }
  readFile() {
    throw new Error("Method not implemented.");
  }
  createDir(): Promise<any> {
    throw new Error("Method not implemented.");
  }
  delete(): Promise<any> {
    throw new Error("Method not implemented.");
  }
  getDirectory(): string {
    throw new Error("Method not implemented.");
  }
  getRequestPath(): string {
    throw new Error("Method not implemented.");
  }
  getDiskUsage(): Promise<DiskUsageContract> {
    throw new Error("Method not implemented.");
  }
  openFileExternally(filePath: string, mimeType: string): Promise<any> {
    throw new Error("Method not implemented.");
  }
  toJSON(successHandler: Function, errorHandler: Function): void {
    throw new Error("Method not implemented.");
  }
  toString(successHandler: Function, errorHandler: Function): void {
    throw new Error("Method not implemented.");
  }
  get(): Promise<any> {
    throw new Error("Method not implemented.");
  }

}
