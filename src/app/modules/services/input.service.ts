import { Injectable, Inject } from '@angular/core';
import { BookDao } from '../daos/book.dao';
import { PlatformContract } from './contracts/platform/platform.contract';
import { LedFileSystemContract } from './contracts/file-system/led-file-system.contract';
import { List } from "immutable";
import { LegacyBookContract } from "../../contracts/legacy-book.contract";
@Injectable()
export class InputService {
    private stdIN: string;
    private jsonData: any;
    private books: List<LegacyBookContract> = List([]);

    constructor(@Inject('PlatformService') private platform: PlatformContract,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        private book: BookDao) {
        this.platform.ready().then(() => {
            this.book.fetchData().then(bookList => this.books = bookList).catch(error => console.log(error));
        });
    }

    setData(data: string) {
        this.stdIN = data.replace('alfaconbooks://viewBook?data=', '');
        this.jsonData = JSON.parse(decodeURIComponent(this.stdIN));
    }

    toString() {
        return this.stdIN;
    }

    toObject() {
        return this.jsonData;
    }

    filterData(): Promise<any> {
        return new Promise((resolve, reject) => {

            const bookArray = this.books.filter(book => {
                return book.book_publisher_id== this.jsonData.book_publisher_id;
            }).toList();

            if (bookArray.size > 0) {
                resolve(bookArray.toArray());
            } else {
                reject(
                    'Livro não encontrado. publisher_id = ' +
                    this.jsonData.book_publisher_id
                );
            }

        });
    }
}
