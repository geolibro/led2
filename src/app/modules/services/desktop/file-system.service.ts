import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
import { NativeFSFunctionsContract } from '../contracts/file-system/native-fs-functions.contract';
import { DiskUsageContract } from '../contracts/file-system/disk-usage.contract';
import { LayoutFileSystemEnum } from '../contracts/file-system/layout-file-system.enum';
import { VaultGeneratorService } from '../vault/vault-generator.service';
import { FileAccessEnum } from '../contracts/file-system/file-access.enum';
import { Injectable } from '@angular/core';
declare var window;

@Injectable()
export class FileSystemService implements LedFileSystemContract, NativeFSFunctionsContract {
    public FS;
    public USER_DATA_PATH;
    public OS;
    public PATH;
    public XML;
    public MIME;
    private DISKSPACE;

    private interactivePromise: Promise<any>;
    private requestPath: string;
    private flagAccess: string;
    private flagEnumFlag: number;
    private RIMRAF;
    private FOLDER_SIZE;

    private readonly FLAG_ACCESS = ["NO_ACCESS", "CAN_READ", "CAN_WRITE", "CAN_READ_WRITE"];

    constructor(private generator: VaultGeneratorService) {
        this.USER_DATA_PATH = window.require("electron").remote.app.getPath("userData");
        this.FS = window.require("fs");
        this.OS = window.require("os");
        this.PATH = window.require("path");
        this.MIME = window.require("mime-types");
        this.XML = window.require("read-xml");
        this.RIMRAF = window.require("rimraf");
        this.DISKSPACE = window.require("diskspace");
        this.FOLDER_SIZE = window.require("get-folder-size");
    }

    getDiskUsage(): Promise<DiskUsageContract> {
        return new Promise<DiskUsageContract>((resolve, reject) => {
            // default values
            let diskUsage: DiskUsageContract = {
                app: 0,
                free: Number.MAX_SAFE_INTEGER
            };

            this.FOLDER_SIZE(this.USER_DATA_PATH, (err, size) => {
                if (!err) {
                    diskUsage.app = size;
                }

                let rootPath = this.OS.platform() === "win32" ? "c" : "/";
                this.DISKSPACE.check(rootPath, (diskSpaceErr, diskSpaceInfo) => {
                    if (!diskSpaceErr) {
                        diskUsage.free = diskSpaceInfo.free;
                    }

                    resolve(diskUsage);
                });
            });
        });
    }

    openFileExternally(filePath: string, mimeType: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const { shell } = window.require("electron");
            // Open a local file in the default app
            if (shell.openItem(filePath)) {
                resolve();
            } else {
                reject();
            }
        });
    }

    getRequestPath() {
        return this.requestPath;
    }

    getDirectory(): string {
        return this.USER_DATA_PATH + this.PATH.sep;
    }

    request(path?: string, layout: LayoutFileSystemEnum = LayoutFileSystemEnum.ROOT_PATH) {
        const PID = this.generator.UUID();
        const returnedObject = {};

        returnedObject[PID] = new FileSystemService(this.generator);
        const _self = returnedObject[PID];

        _self.requestPath = layout === LayoutFileSystemEnum.ROOT_PATH ? _self.getDirectory() : "";

        if (path) {
            path = path.replace("/", _self.PATH.sep);
            _self.requestPath += path;
        }

        _self.FS.access(_self.requestPath, _self.FS.constants.R_OK | _self.FS.constants.W_OK, err => {
            if (err) {
                _self.flagAccess = _self.FLAG_ACCESS[FileAccessEnum.NO_ACCESS];
                _self.flagEnumFlag = FileAccessEnum.NO_ACCESS;
            } else {
                _self.flagAccess = _self.FLAG_ACCESS[FileAccessEnum.CAN_READ_WRITE];
                _self.flagEnumFlag = FileAccessEnum.CAN_READ_WRITE;
            }
        });

        return _self;
    }

    toJSON(successHandler: any, errorHandler: any) {
        this.interactivePromise
            .then(response => {
                successHandler(JSON.parse(response));
            })
            .catch(errorHandler);
    }

    toString(successHandler: any, errorHandler: any) {
        this.interactivePromise
            .then(response => {
                try {
                    let isApplicationXHTMLXML = this.MIME.lookup(this.requestPath).indexOf("application/xhtml+xml"),
                        isApplicationXML = this.MIME.lookup(this.requestPath).indexOf("application/xml"),
                        isApplicationOebpsPackageXML = this.MIME.lookup(this.requestPath).indexOf("application/oebps-package+xml");

                    if (isApplicationXHTMLXML !== -1 || isApplicationXML !== -1 || isApplicationOebpsPackageXML !== -1) {
                        this.XML.readXML(this.FS.readFileSync(this.requestPath), (err, data) => {
                            if (err) throw Error(err);

                            return successHandler(data.content);
                        });
                    }
                }
                catch (err) {
                    // nothing to do: th code above must be refactored
                }


                if (typeof response != "string") {
                    const blobTxt = new Blob([response], { type: 'text/plain' });
                    const Reader = new FileReader();
                    Reader.readAsText(blobTxt);

                    Reader.onloadend = () => {
                        return successHandler(Reader.result);
                    };

                    Reader.onerror = () => errorHandler;

                } else {
                    return successHandler(response);
                }

            })
            .catch(errorHandler);
    }

    readFile() {
        this.interactivePromise = new Promise((resolve, reject) => {
            this.FS.readFile(this.requestPath, (error, response) => {
                if (error) return reject(error);
                return resolve(response);
            });
        });

        return this;
    }

    writeFile(data: string | Buffer | Uint8Array, successHandler: any, errorHandler: any, append: boolean) {
        this.interactivePromise = new Promise((resolve, reject) => {
            if (!append) {
                this.FS.writeFile(this.requestPath, data, error => {
                    if (error) {
                        reject(error);
                        errorHandler(error);
                    }
                    return successHandler(true);
                });
            } else {
                this.FS.appendFile(this.requestPath, data, error => {
                    if (error) {
                        reject(error);
                        errorHandler(error);
                    }
                    return successHandler(true);
                });
            }
        });
        return this;
    }

    get() {
        return this.interactivePromise;
    }

    createDir() : Promise<any> {
        return new Promise((resolve, reject) => {
            const mkdirp = window.require("mkdirp");

            mkdirp(this.requestPath, response => {
                if (response !== null) return reject(response);
                else return resolve(this);
            });
        });
    }

    delete() {
        return new Promise((resolve, reject) => {
            this.RIMRAF(this.getRequestPath(), this.FS, (error) => {
                if (error) {
                    reject(error)
                } else {
                    resolve()
                }
            });
        });
    }

    readDir(options?: string | object): Promise<string[] | Buffer[]> {
        return new Promise((resolve, reject) => {
            this.FS.readdir(this.getRequestPath(), (error, files) => {
                if (error) return reject(error);
                resolve(files.map((file, index) => files[index] = `${this.getRequestPath() + file}`));
            });
        });
    }

    exists(path: string, handler: Function) {
        this.FS.exists(this.getDirectory() + path, handler);
    }


    rename(oldPath: string, newPath: string) {
        this.FS.renameSync(oldPath, newPath);
    }

    readFileSync(path: string, layout: LayoutFileSystemEnum = LayoutFileSystemEnum.ROOT_PATH) {
        let rootPath = this.getDirectory();

        if (layout === LayoutFileSystemEnum.NO_ROOT_PATH) rootPath = "";

        return this.FS.readFileSync(rootPath + path);
    }

    nativeOpen(path: string | Buffer | URL, flags: string | number, mode: number, callback: Function) {
        return this.FS.open(path, flags, mode, callback);
    }

    nativeWrite(fd: number, buffer: Buffer | Uint8Array, offset: number, length: number, position: number, callback: Function) {
        return this.FS.write(fd, buffer, offset, length, position, callback);
    }

    nativeClose(fd: any, callback: any) {
        return this.FS.close(fd, callback);
    }

}
