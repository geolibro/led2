import { Injectable, Inject } from '@angular/core';
import { DownloadContract } from '../contracts/download/download.contract';
import { Loadingstatus } from '../contracts/download/loadingstatus';
import { VaultDecrypterService } from '../vault/vault-decrypter.service';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
declare var window;

@Injectable()
export class DownloadService implements DownloadContract {
    progress: any;
    loadingstatus: Loadingstatus = new Loadingstatus();

    constructor(@Inject('FileSystemService') private fs: LedFileSystemContract, private vault: VaultDecrypterService) { }

    download(source: string, target: string, successCallback?: (fileEntry: any) => void,
        errorCallback?: (error: any) => void, trustAllHosts?: boolean,
        options?: any, xorKey?: string): void {

        let path;
        this.vault.prepare(xorKey);

        fetch(source)
            .then(response => {

                const _self = this;
                const pathObject = window.require('path');

                let pos = 0,
                    fd = 0,
                    file = pathObject.basename(source),
                    reader = response.body.getReader(),
                    keyIndex = 0;

                this.fs.request().createDir().then(scope => {
                    path = scope.getRequestPath();
                    this.fs.nativeOpen(scope.getRequestPath() + file, 'w', 0o666,
                        (err, _fd) => {
                            fd = _fd;
                            readNext();
                        });
                }).catch(error => {
                    errorCallback(error);
                });

                function close() {
                    _self.fs.nativeClose(fd, () => {
                        successCallback(true);
                    });
                }

                function readNext() {
                    reader.read().then(({ value, done }) => {
                        if (value) {
                            _self.vault.bitwiseX(value);
                            _self.fs.nativeWrite(fd, value, 0, value.byteLength, pos, () => {
                                pos += value.length;
                                if (!done) readNext();
                                else close();
                            });

                        } else if (done) close();
                    });
                }

            }).catch((err) => {
                errorCallback(err);
            });

    }

    showProgress(received, total) {
        const percentage = (received * 100) / total;
        this.loadingstatus.setPercentage(percentage);
        return this.loadingstatus.getStatus();
    }

}
