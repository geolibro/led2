import { DialogContract } from '../contracts/dialog/dialog.contract';
import { PromptResultContract } from '../contracts/dialog/promptResult.contract';
import { Injectable } from '@angular/core';
declare var window;

@Injectable()
export class DialogService implements DialogContract {

    private dialog: any;
    private prompt: any;

    constructor() {
        this.dialog = window.require('dialogs')();
        this.prompt = window.require('electron-prompt');
    }

    onConfirm(message: string, title?: string, buttons: Array<string> = ['Cancelar', 'Ok']): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                const remote = window.require('electron').remote;

                remote.dialog.showMessageBox(remote.getCurrentWindow(), {
                    'type': 'question',
                    'title': title,
                    'message': message,
                    'buttons': buttons
                }, (result) => {

                    if (result === 0) {
                        resolve(false);
                    } else if (result === 1) {
                        resolve(true);
                    } else {
                        reject(result);
                    }
                });

            } catch (e) {
                reject(e);
            }
        });
    }

    onPrompt(message: string,
        defaultText?: string,
        inputValue = '',
        inputAttrs?: {},
        inputType = 'input',
        selectOptions?: {},
        title = ''): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                this.prompt({
                    title: title,
                    label: message,
                    value: inputValue,
                    inputAttrs: inputAttrs,
                    type: inputType,
                    selectOptions: selectOptions
                })
                    .then((r) => {
                        const promptResult: PromptResultContract = {
                            input1: r,
                            buttonIndex: 1
                        };
                        resolve(promptResult);
                    })
                    .catch(console.error);
            } catch (e) {
                reject(e);
            }
        });
    }

    onAlert(description: string, title: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                this.dialog.alert(description, title, resolve);
            } catch (e) {
                reject(e);
            }
        });
    }
}
