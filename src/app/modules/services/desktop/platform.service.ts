import { PlatformContract } from '../contracts/platform/platform.contract';
import { PlatformEnum } from '../contracts/platform/platform.enum';
import { Platform } from '../contracts/platform/platform';
import { Injectable } from '@angular/core';
declare var window;

@Injectable()
export class PlatformService implements PlatformContract {
    private app;
    private os;

    constructor() {
        this.app = window.require('electron').remote.app;
        this.os = window.require('os');
    }

    ready(): Promise<Event> {
        return new Promise((resolve) => {
            window.addEventListener('load', resolve);
        });
    }

    pause(): Promise<Event> {
        throw new Error('Method not implemented.');
    }

    resume(): Promise<Event> {
        throw new Error('Method not implemented.');
    }

    online(): Promise<Event> {
        return new Promise((resolve) => {
            window.addEventListener('online', resolve);
        });
    }

    offline(): Promise<Event> {
        return new Promise((resolve) => {
            window.addEventListener('offline', resolve);
        });
    }

    backbutton(): Promise<Event> {
        return new Promise((resolve) => {
            resolve(null);
        });
    }

    is(platform: PlatformEnum): boolean {
        if (platform === PlatformEnum.DESKTOP) return true;
        else if (this.os.platform().indexOf(Platform.TYPE[platform]) !== -1) return true;
        else return false;
    }
}
