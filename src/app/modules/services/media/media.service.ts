import {Injectable} from "@angular/core";

@Injectable()
export class MediaService {

    srt2vtt(subtitleString: string): string {
        let newSubtitleString = [];
        const splitSrt = subtitleString.split('\n');
        let header = "WEBVTT\n\n";
        splitSrt.map((parseOne, i) => {
            if (parseOne.match(/[\d,\d]+/g)) {
                newSubtitleString.push(splitSrt[i].replace(/,/g, '.'));
            } else {
                newSubtitleString.push(splitSrt[i]);
            }
        });
        header += newSubtitleString.join('\n');
        return header;
    }
}
