import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response, RequestOptions  } from "@angular/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";
import { HttpScaoResponse } from "./util/http-scao-response";
import { DomSanitizer } from "@angular/platform-browser";
const api = require("../../../../environments/api.json");

@Injectable()
export class HttpService {
    constructor(private http: HttpClient, private sanitizer: DomSanitizer) {}

    private getRequestOptions(method?: string) {
        const headers: any = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };

        const request = { headers };

        if (method) Object.assign(request, { method });

        const options: any = new RequestOptions(request);
        return options;
    }

    async simpleGet(address: string) {
        return new Promise((resolve, reject) => {
            this.http.get(address, this.getRequestOptions('get')).subscribe(resolve, reject);
        });
    }

    async makeRequest(address: string, data: any, method: string = "post"): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                this.http[method](address, data, this.getRequestOptions(method))
                    .subscribe((response: Response) => {
                    let returnData = new HttpScaoResponse(response);
                    if (returnData.hasErrors()) return reject(returnData.getErrorMessage());
                    return resolve(returnData);
                }, error => reject(error));
            } catch (error) {
                reject(error);
            }
        });
    }

    getURL(server: string, ws: string) {
        const API_ADDRESS = this.getEndPoint(server, ws);
        return `${environment.getUrl(server) + API_ADDRESS}`;
    }

    private getEndPoint(server: string, ws: string) {
        const WS = api[server].filter(endpoint => endpoint.name === ws.toLowerCase());
        if (WS.length === 0) throw new Error(`Endpoint not found [${server}/${ws}]`);
        return WS[0].path;
    }
}
