import { DefaultBodyResponseContract } from "../../../../contracts/default-body-response.contract";
import { Response } from "@angular/http";
import { LegacyBookContract } from "../../../../contracts/legacy-book.contract";
import { HttpResponse } from "@angular/common/http";

export class HttpScaoResponse {
    private Instance: HttpScaoResponse;
    private body: DefaultBodyResponseContract = <DefaultBodyResponseContract>{};
    private bookList: LegacyBookContract[] = [];

    constructor(private response: Response | any) {
        this.body = (this.response instanceof Response) ? this.response.json() : response;
        Object.assign(this, this.response);
    }

    appendField(field: string, value: any) {
        if (this.body) this.body[field] = value;
    }

    all(): any {
        return this.body;
    }

    getUser() {
        if (!this.body) return;

        let user = this.body;
        const hasRequiredFileds = (user.hasOwnProperty('error_code') && user.hasOwnProperty('error_msg'));
        const validate          = (hasRequiredFileds && (user.error_code == '' && user.error_msg == ''));
        if (validate) {
            if(typeof(user.books) == 'string') user.books = [];
            this.bookList = <LegacyBookContract[]>user.books;
            delete user.books;
        }
        return user;
    }

    getBookList() {

        return this.bookList;
    }

    hasErrors(): boolean {
        return (this.body && (this.body.error_code !== "" && this.body.error_msg !== ""));
    }

    getErrorMessage(): string {
        return this.body && this.body['error_msg'];
    }

    getErrorCode(): string {
        return this.body['error_code'];
    }
}
