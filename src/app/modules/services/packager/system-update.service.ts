import { Injectable, Inject } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { BuildResponseContract } from '../../../contracts/build-response.contract';
import { HttpService } from '../http/http.service';
import { DialogContract } from '../contracts/dialog/dialog.contract';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { PlatformEnum } from '../contracts/platform/platform.enum';
declare var self;
declare var navigator;
declare var cordova;

@Injectable()
export class SystemUpdateService {
    constructor(private http: HttpService,
        @Inject('DialogService') private dialog: DialogContract,
        @Inject('PlatformService') private platform: PlatformContract) {}

    async check() {
        if (!environment.production) return;
        if (!this.platform.is(PlatformEnum.DESKTOP)) return;

        try {
            const request: any = await this.http.simpleGet(environment._url.app_version);
            this.validateVersion(request);
        } catch (exception) {
            this.dialog.onAlert(exception, '[Error]');
        }
    }

    private validateVersion(request: any) {
        const currentVersion    = environment.currentVersion.replace('.', '');
        const serverVersion     = request.version.replace('.', '');
        if (currentVersion >= serverVersion) return;

        let message = '';
            message += `Uma nova versão está disponível: \n`;
            message += `Versão: ${request.version} \n`;

        this.dialog.onAlert(message, `SystemUpdate: ${request.created_at}`);
        this.openDownloadURLInBrowser();
    }

    private openDownloadURLInBrowser() {
        if (typeof cordova !== "undefined") {
            if (cordova.platformId == "android") navigator.app.loadUrl(environment._url.app_download, { openExternal: true } );
            else window.open(environment._url.app_download, "_system");
        } else {
            const shell = self.require('electron').shell;
            shell.openExternal(environment._url.app_download);
        }
    }
}
