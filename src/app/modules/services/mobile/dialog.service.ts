import { DialogContract } from '../contracts/dialog/dialog.contract';
import { Injectable } from '@angular/core';
declare var navigator;

@Injectable()
export class DialogService implements DialogContract {

    private notification: Notification;

    onConfirm(message: string, title?: string, buttons?: Array<string>): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                navigator.notification.confirm(message, (result) => {
                    if (result === 1) {
                        resolve(true);
                    } else if (result === 2) {
                        resolve(false);
                    } else {
                        reject(result);
                    }

                }, title, buttons);
            } catch (exception) {
                reject(exception);
            }
        });
    }

    onPrompt(message: string,
        defaultText?: string,
        inputType?: string,
        inputValue?: string,
        inputAttrs?: {},
        selectOptions?: {},
        title: string = '') {
        return new Promise((resolve, reject) => {

            try {
                navigator.notification.prompt(message, resolve, title);
            } catch (Exception) {
                reject(Exception);
            }

        });
    }

    onAlert(description: string, title: string) {
        return new Promise((resolve, reject) => {

            try {
                navigator.notification.alert(description, resolve, title);
            } catch (Exception) {
                reject(Exception);
            }

        });
    }
}
