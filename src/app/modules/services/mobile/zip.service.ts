import { ZipContract } from '../contracts/zip/zip.contract';
import { Injectable, Inject } from '@angular/core';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
declare var Zeep;

@Injectable()
export class ZipService implements ZipContract {
    constructor(@Inject('FileSystemService') private fileSystem: LedFileSystemContract) { }
    unzip(options: { from: string, to: string }, successHandler: Function, errorHandler: Function) {

        let from = this.fileSystem.getDirectory() + options.from;
        let to = this.fileSystem.getDirectory() + options.to;

        let toObject = { from, to };

        Zeep.unzip(
            toObject,
            successHandler,
            errorHandler
        );
    }
}
