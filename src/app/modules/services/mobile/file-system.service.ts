import {WebWorkerService} from "angular2-web-worker";
import {LedFileSystemContract} from "../contracts/file-system/led-file-system.contract";
import {VaultGeneratorService} from "../vault/vault-generator.service";
import {LayoutFileSystemEnum} from "../contracts/file-system/layout-file-system.enum";
import {DiskUsageContract} from "../contracts/file-system/disk-usage.contract";
import {Injectable, Inject} from "@angular/core";
import {PlatformContract} from "../contracts/platform/platform.contract";
import {PlatformEnum} from "../contracts/platform/platform.enum";

declare var DiskSpacePlugin;
declare var navigator;
declare var self;

@Injectable()
export class FileSystemService implements LedFileSystemContract {
    public layout;
    public path;
    public worker: WebWorkerService;
    private requestPath: string;
    protected interactivePromise;
    private parts;

    constructor(private vaultGenerator: VaultGeneratorService,
        @Inject("PlatformService") private platform: PlatformContract) {}

    request(path: string = '', layout: LayoutFileSystemEnum = LayoutFileSystemEnum.ROOT_PATH) {
        let currentProcess = new FileSystemService(this.vaultGenerator, this.platform);

        if (layout === LayoutFileSystemEnum.ROOT_PATH) {
            currentProcess.requestPath = this.getDirectory();
        } else if (layout === LayoutFileSystemEnum.NO_ROOT_PATH) {
            currentProcess.requestPath = path;
        }

        currentProcess.path   = path;
        currentProcess.layout = layout;
        currentProcess.worker = new WebWorkerService();

        return currentProcess;
    }

    exists(path: string, handler: Function): void {
        let result: boolean = false;
        (async () => {
            await this.platform.ready();
            window.resolveLocalFileSystemURL(
                this.getDirectory() + path,
                (entry: FileEntry | DirectoryEntry) => {
                    if (entry.isDirectory) result = true;
                    if (entry.isFile) result = true;
                    return handler(result);
                },
                error => {
                    let errorStr = "exists() -> erro chamando resolveLocalFileSystemURL (" + JSON.stringify(error) + ')';
                    console.log(errorStr);
                    return handler(false);
                }
            );
        })();
    }

    readDir(options?: string | object): Promise<string[] | Buffer[]> {
        return new Promise(async (resolve, reject) => {
            await this.platform.ready();
            window.resolveLocalFileSystemURL(this.getRequestPath(), (dirEntry: DirectoryEntry) => {
                if (!dirEntry.isDirectory) return reject("Request path is not a directory: " + this.getRequestPath());

                const readDir = dirEntry.createReader();

                readDir.readEntries((entries: Entry[]) => {
                    const entriesPATH = [];

                    entries.forEach(entry => {
                        console.log('readDir', entry.toInternalURL());
                        entriesPATH.push(entry.toInternalURL());
                    });

                    return resolve(entriesPATH);
                }, reject);
            });
        });
    }

    readFile() {
        this.interactivePromise = new Promise(async (resolve, reject) => {
            this.getFileEntry().then((entry: FileEntry) => {
                const reader = new FileReader();
                entry.file(file => {

                    if (file.size === 0) return reject(FileError.NOT_FOUND_ERR);

                    reader.onabort = (event) => {
                        console.log(reader.abort);
                    };

                    reader.onloadend = () => {
                        let resultData = reader.result ? reader.result : "";
                        return resolve(new Uint8Array(resultData));
                    };

                    reader.onerror = () => {
                        return reject(reader.error);
                    };

                    reader.readAsArrayBuffer(file);
                }, error => {
                    return reject(error);
                });
            }).catch(reject);
        });

        return this;
    }

    writeFile(data: string | Buffer | Uint8Array, successHandler: Function, errorHandler: Function, append: boolean) {

        this.getFileEntry()
            .then((result: FileEntry) => {
                result.createWriter(
                    writer => {
                        try {

                            const blobJSON = new Blob([data], {type: "application/json"});

                            if (blobJSON.size === 0) return errorHandler(FileError.INVALID_MODIFICATION_ERR);

                            if (append) {
                                try {
                                    writer.seek(writer.length);
                                } catch (error) {
                                    console.log('seek bitch');
                                    return errorHandler(error);
                                }
                            }

                            writer.onwriteend = event => {
                                return successHandler(event);
                            };

                            writer.onerror = function(error) {
                                console.log(JSON.stringify(error));
                                return errorHandler('Fail in write file' + error.toString());
                            };

                            writer.write(blobJSON);
                        } catch (error) {
                            errorHandler(error);
                        }
                    },
                    (error: FileError) => {
                        return errorHandler(error);
                    }
                );
            }).catch((error: FileError) => {
                return errorHandler(error);
            });

        return this;
    }

    rename(oldPath: string, newPath: string): void {
        //
    }

    createDir(): Promise<any> {
        return new Promise((resolve, reject) => {
            window.resolveLocalFileSystemURL(this.getDirectory(), (root: DirectoryEntry) => {
                const dirs = this.path.split("/").reverse();

                const makePath = (dir) => {
                    root.getDirectory(dir, {
                        create : true,
                        exclusive : false
                    }, successCB, failCB);
                };

                const successCB = (entry) => {
                    root = entry;
                    if (dirs.length > 0){
                        makePath(dirs.pop());
                    }else{
                        return resolve();
                    }
                };

                const failCB = (fileError: FileError) => {
                    let errorStr = "createDir -> failed on dir creation (" + this.path + ') - ' + fileError.code;
                    return reject(errorStr);
                };

                makePath(dirs.pop());
            });
        });
    }

    delete(): Promise<any> {
        return new Promise(async (resolve, reject) => {
            await this.platform.ready();
            window.resolveLocalFileSystemURL(
                this.getRequestPath() + this.path,
                (entry: FileEntry | DirectoryEntry) => {
                    const remover = {
                        file: (entry: FileEntry, handlerResolve: any, rejectResolve: any) => {
                            entry.remove(handlerResolve, rejectResolve);
                        },
                        directory: (entry: DirectoryEntry, handlerResolve: any, rejectResolve: any) => {
                            entry.removeRecursively(handlerResolve, rejectResolve);
                        }
                    };

                    if (entry.isFile) remover.file(<FileEntry>entry, resolve, reject);
                    else if (entry.isDirectory) remover.directory(<DirectoryEntry>entry, resolve, reject);
                    else reject("The request path is not a directory and is also not a file");
                },
                reject
            );
        });
    }

    getDirectory(): string {
        let directory = "cdvfile://localhost/";
        try {
            if (cordova.platformId.toLowerCase() == "ios") directory = cordova.file.dataDirectory;
            else if (cordova.platformId.toLowerCase() == "android") directory = cordova.file.externalDataDirectory;
        } catch (error) {
            console.log("getDirectoryError", error);
            if (this.platform.currentPlatform == PlatformEnum.IOS) directory = "cdvfile://localhost/library-nosync/";
            else if (this.platform.currentPlatform == PlatformEnum.ANDROID) directory = "cdvfile://localhost/files-external/";
        }

        return directory;
    }

    getRequestPath(): string {
        return this.requestPath;
    }

    getDiskUsage(): Promise<DiskUsageContract> {
        return new Promise(async (resolve, reject) => {
            await this.platform.ready();
            DiskSpacePlugin.info(
                { location: 1 },
                response => {
                    let data: DiskUsageContract = {
                        app: response.app,
                        free: response.free
                    };

                    resolve(data);
                },
                reject
            );
        });
    }

    openFileExternally(filePath: string, mimeType: string): Promise<any> {
        return new Promise<any>(async (resolve, reject) => {
            await this.platform.ready();
            const open = cordova.plugins["disusered"]["open"];
            open(filePath);
            // cordova.plugins["fileOpener2"].open(filePath, mimeType, {
            //     error: error => {
            //         let errorStr = "Erro chamando fileOpener2: " + JSON.stringify(error);
            //         reject(error);
            //     },
            //     success: () => {
            //         resolve();
            //     }
            // });
        });
    }

    toJSON(successHandler: Function, errorHandler: Function): void {
        this.interactivePromise
            .then(async (response: Uint8Array) => {
                await this.platform.ready();
                const blobJSON = new Blob([response], { type: "application/json" });

                const Reader = new FileReader();

                Reader.onloadend = () => {
                    const result = Reader.result;
                    if (result && result !== "") {
                        try {
                            return successHandler(JSON.parse(result));
                        } catch (error) {
                            console.log('toJSON - error: ' + error.toString() + '; returning data anyway');
                            return successHandler(result);
                        }
                    }
                    else return errorHandler(null);
                };

                Reader.onerror = () => {
                    return errorHandler(Reader.error);
                };

                Reader.readAsText(blobJSON);
            })
            .catch(errorHandler);
    }

    get(): Promise<string | Buffer> {
        return this.interactivePromise;
    }

    toString(successHandler: Function, errorHandler: Function): void {
        this.interactivePromise.then(async response => {
            await this.platform.ready();
            const blobJSON = new Blob([response], { type: "text/plain" });

            const Reader = new FileReader();

            Reader.onloadend = () => {
                if (Reader.result && Reader.result !== "") return successHandler(Reader.result);
                else return errorHandler(null);
            };

            Reader.onerror = () => {
                return errorHandler(Reader.error);
            };

            Reader.readAsText(blobJSON);
        }).catch(errorHandler);
    }

    openLinkExternally(urlPath: string) {
        if (typeof cordova !== "undefined") {
            if (cordova.platformId == "android") navigator.app.loadUrl(urlPath, {openExternal: true});
            else window.open(urlPath, "_system"); // iOS
        } else {
            let shell = self.require("electron").shell; // Electron
            shell.openExternal(urlPath);
        }
        return;
    }

    private getFileEntry() {
        return new Promise(async (resolve, reject) => {
            await this.platform.ready();
            if (this.getRequestPath() === null || this.getRequestPath() === undefined) return;

            window.resolveLocalFileSystemURL(
                this.getRequestPath(),
                (entry: DirectoryEntry) => {
                    if (entry.isDirectory) {
                        entry.getFile(
                            this.path,
                            { create: true, exclusive: false },
                            fileEntry => {
                                return resolve(fileEntry);
                            },
                            error => {
                                return reject(error);
                            }
                        );
                    } else if (entry.isDirectory && !this.path) return reject("NO PATH: getFileEntry method");
                    else return resolve(entry);
                },
                (error: FileError) => {
                    return reject(error);
                }
            );
        });
    }
}
