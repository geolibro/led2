import { PlatformContract } from '../contracts/platform/platform.contract';
import { PlatformEnum } from '../contracts/platform/platform.enum';
import { Platform } from '../contracts/platform/platform';
import { Injectable } from '@angular/core';

@Injectable()
export class PlatformService implements PlatformContract {
    currentPlatform: PlatformEnum;
    constructor() {
        this.ready().then(() => {
            if (this.is(PlatformEnum.IOS)) this.currentPlatform = PlatformEnum.IOS;
            else if (this.is(PlatformEnum.ANDROID)) this.currentPlatform = PlatformEnum.ANDROID;
            else this.currentPlatform = PlatformEnum.MOBILE;
        });
    }

    /**
     * https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
     *
     * @returns {Promise<Event>}
     * @memberof PlatformService
     */
    ready(): Promise<Event> {
        return new Promise((resolve, reject) => {
            document.addEventListener('deviceready', resolve, false);
        });
    }

    /**
     * https://cordova.apache.org/docs/en/latest/cordova/events/events.html#pause
     *
     * @returns {Promise<Event>}
     * @memberof PlatformService
     */
    pause(): Promise<Event> {
        return new Promise((resolve, reject) => {
            document.addEventListener('pause', resolve, false);
        });
    }

    /**
     * https://cordova.apache.org/docs/en/latest/cordova/events/events.html#resume
     *
     * @returns {Promise<Event>}
     * @memberof PlatformService
     */
    resume(): Promise<Event> {
        return new Promise((resolve, reject) => {
            document.addEventListener('resume', resolve, false);
        });
    }

    /**
     * https://cordova.apache.org/docs/en/latest/cordova/events/events.html#backbutton
     *
     * @returns {Promise<Event>}
     * @memberof PlatformService
     */
    backbutton(): Promise<Event> {
        return new Promise((resolve, reject) => {
            document.addEventListener('backbutton', resolve, false);
        });
    }

    is(platform: PlatformEnum): boolean {
        if (platform === PlatformEnum.DESKTOP) return false;
        if (platform === PlatformEnum.MOBILE) return true;
        if (cordova.platformId.toLowerCase().indexOf(Platform.TYPE[platform]) !== -1) return true;
        return false;
    }

    online(): Promise<Event> {
        return new Promise((resolve, reject) => {
            document.addEventListener('online', resolve, false);
        });
    }

    offline(): Promise<Event> {
        return new Promise((resolve, reject) => {
            document.addEventListener('offline', resolve, false);
        });
    }
}
