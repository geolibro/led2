import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { DeviceService } from './device.service';
import { DialogService } from './dialog.service';
import { DownloadService } from './download.service';
import { FileSystemService } from './file-system.service';
import { NetworkService } from './network.service';
import { ZipService } from './zip.service';
import { PlatformService } from './platform.service';
import {EpubDocumentService} from "./epub-document.service";
@NgModule({
    imports: [CommonModule, HttpModule],
    providers: [
        { provide: "DeviceService", useClass: DeviceService },
        { provide: "DialogService", useClass: DialogService },
        { provide: "DownloadService", useClass: DownloadService },
        { provide: "FileSystemService", useClass: FileSystemService },
        { provide: "NetworkService", useClass: NetworkService },
        { provide: "ZipService", useClass: ZipService },
        { provide: "EpubDocumentService", useClass: EpubDocumentService },
        { provide: "PlatformService", useClass: PlatformService }
    ]
})

export class ServicesMobileModule {}
