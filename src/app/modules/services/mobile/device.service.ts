import { DeviceContract } from '../contracts/device/device.contract';
import { Injectable } from '@angular/core';

@Injectable()
export class DeviceService implements DeviceContract {
    prepareDeviceInfoRequest() {
        return {
            'device_name': device.manufacturer,
            'device_info': [{
                'model': device.model,
                'os_name': device.platform,
                'os_version': device.version,
                'screen_height': window.screen.height * window.devicePixelRatio,
                'screen_width': window.screen.width * window.devicePixelRatio
            }]
        }
    }
}
