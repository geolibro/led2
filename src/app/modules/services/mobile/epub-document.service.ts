import { BookEvent } from '../../events/book/book.event';
import { EPubTextformatThemeTypeEnum } from '../../reader/components/e-pub-textformat/e-pub-textformat-themetypes.enum';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { PlatformEnum } from '../contracts/platform/platform.enum';
import { LedFileSystemContract } from "../contracts/file-system/led-file-system.contract";
import { PageReaderService } from "../reader/page-reader.service";
import { EPubItemContract } from "../reader/contracts/e-pub-item.contract";
import { Inject, Injectable } from "@angular/core";
import { EpubContract } from "../contracts/epub/epub.contract";

declare var MobileAccessibility;
declare var window;

// REFATORAR PRA MOBILE
@Injectable()
export class EpubDocumentService implements EpubContract {
    private isLoad: boolean = false;
    private fileBlob: Blob;
    private epubWrap: any;
    private epubContainer: any;
    private HammerJS: HammerManager;
    private countSwipes: number = 0;
    private leftPosition: number = 0;
    private pageNumber: number = 0;
    private PageReader: PageReaderService;
    private totalColumns: number = 0;
    private _style: any;
    private directory: string;

    private readonly column_gap = 40;
    private readonly totalExcess = this.column_gap * 2;
    private readonly core_width = screen.width;
    private readonly core_height = screen.height;

    private currentWindowWidth = 0;
    private iframeHTML: HTMLElement;
    private frameDimension: {} = {};

    constructor(private pageReader: PageReaderService,
                private bookEvent: BookEvent,
                @Inject('FileSystemService') private fileSystem: LedFileSystemContract) {
    }

    bootstrapEpubPage(bytesArray: Array<any>, directory: string, iframeContainer: any) {
        this.bookEvent.EchoResizeEpubWindow.subscribe((frameDimensions) => {
            this.frameDimension = {};
            this.updateContainerDimensions(frameDimensions);
        });
        this.directory = directory;
        this.fileBlob = new Blob([bytesArray], {type: 'text/xhtml'});
        this.isLoad = true;
        this.leftPosition = 0;
        this.countSwipes = 0;
        this.pageNumber = this.pageReader.getPageNumber();
        this._style = {};
        this.epubWrap = iframeContainer;
        this.epubContainer = this.epubWrap['contentDocument'];
        return this;

    }

    addSwipeEventListener(): Promise<number> {

        return new Promise((resolve, reject) => {

            /* --- MOBILE --- */
            if (this.isLoad) {
                this.HammerJS = new Hammer(this.epubContainer.getElementsByTagName('html')[0], {
                    domEvents: true,
                    touchAction: 'pan-y'
                });

                this.HammerJS.get('swipe').set({enable: true});
                this.swipeLeft(resolve);
                this.swipeRight(resolve);

            }
        });
    }

    addSummaryEventListener(anchor?: string, platform?: PlatformContract) {
        return new Promise((resolve, reject) => {

            if (platform.is(PlatformEnum.ANDROID)) {
                const epubWrap: any = $('#epub-wrap').get(0)['contentDocument'];

                $('a', epubWrap).each(function () {
                    $(this).click(resolve);
                });

            } else {

                const DOMIndexed = this.indexDOM(this.epubContainer);

                if (DOMIndexed.hasOwnProperty('A') && DOMIndexed.A.length) {
                    DOMIndexed.A.forEach(link => {
                        if (link.getAttribute('href') !== "") {
                            link.addEventListener("click", resolve, false);
                        }
                    });
                }
            }

            if (anchor) {
                setTimeout(() => {
                    this.epubContainer.location.hash = anchor.replace('#', '');
                }, 500);
            }

        });
    }

    setStyle(style: any) {
        this._style = style;
        this.forceReplacementStyle();

    }

    generateDocument(pageReader: PageReaderService, blob?: boolean): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.isLoad) {
                this.PageReader = pageReader;

                this.pageNumber = pageReader.getPageNumber();
                const readerCollection = pageReader.reader.getCollection();
                const styles: EPubItemContract[] = readerCollection.styles;

                const cssUri = pageReader.reader.getBasePath(styles[0].href);
                const reader = new FileReader();
                reader.onloadend = () => {
                    try {
                        const documentHtml = new DOMParser().parseFromString(
                            reader.result,
                            'application/xhtml+xml'
                        );

                        this.fixImages(documentHtml);
                        this.isVoiceOverRunning(documentHtml, cssUri, (div) => {
                            setTimeout(() => {
                                const arrayBytes = new Buffer(div.outerHTML);
                                return resolve(
                                    {
                                        'doc': pageReader.getPage(arrayBytes, {type: 'text/html'}, blob)
                                    }
                                )
                            }, 125);
                        });
                    } catch (e) {
                        console.log(e);
                    }
                };

                reader.readAsText(this.fileBlob);

            } else {
                return reject('Epub not loaded.');
            }
        });
    }

    updateEpubContainer(iframe?) {

        this.epubContainer = this.epubWrap['contentDocument'];
        this.iframeHTML = this.epubContainer ? this.epubContainer.getElementsByTagName('html')[0] : iframe;

        const iframeStyles = this.defaultFrameDimensions(this.frameDimension['offsetWidth']);
        this.epubContainer = this.setHTMLStyle(iframeStyles);
        this.iframeHTML['style']['left'] = this.leftPosition + 'px';
        this.currentWindowWidth = (this.iframeHTML.clientWidth - this.totalExcess);

        this.setTotalColumns(this.iframeHTML);

        const actionBarHeight = document.getElementById('epubActionbarReader').clientHeight;
        this.iframeHTML.style.height = (this.frameDimension['scrollHeight'] - actionBarHeight) + 'px';
    }

    /**
     * Made in Manga
     * @param e
     * @param o
     */
    indexDOM(e, o?: any) {
        e = e || document.body;
        o = o || {};
        o['$' + e.id] = e;
        (o[e.tagName] = o[e.tagName] || []).push(e);
        (e.className + '').split(/\s+/).forEach(p => (o[p] = o[p] || []).push(e));
        for (let i = 0, l = e.children.length; i < l; ++i) {
            this.indexDOM(e.children[i], o);
        }

        return o;
    }

    createDOM(tag?, props?, children?, parent?) {
        tag = props = children = parent = null;
        for (let i = 0; i < arguments.length; ++i) {
            if (typeof arguments[i] == 'string') {
                tag = arguments[i];
            }
            else if (typeof arguments[i] == 'object') {
                if (Array.isArray(arguments[i])) {
                    children = arguments[i];
                }
                else if (arguments[i] instanceof Element) {
                    parent = arguments[i];
                }
                else if (arguments[i]) {
                    props = arguments[i];
                }
            }
        }
        if (!props) {
            props = {};
        }
        if (!tag) {
            tag = 'div';
        }
        if (!children) {
            children = [];
        }
        const el = document.createElement(tag);
        for (const k in props) {
            if (k == 'style') {
                Object.assign(el.style, props[k]);
            }
            else if (k == 'attr') {
                for (const ak in props[k]) {
                    el.setAttribute(ak, props[k][ak]);
                }
            } else {
                el[k] = props[k];
            }
        }
        children.forEach(c => el.appendChild(Array.isArray(c) ? this.createDOM.apply(this, c) : c));
        if (parent) {
            parent.appendChild(el);
        }
        return el;
    }

    private fixImages(documentHTML) {
        const imgs = documentHTML.body.getElementsByTagName('img');

        for (let i = 0; i < imgs.length; i++) {

            const selectedImage = imgs[i];
            const sourcePATH = selectedImage.getAttribute('src').replace('../', '');
            let path = this.directory + this.PageReader.reader.getBasePath(sourcePATH);


            if (this.directory.indexOf('file://') === -1) {
                path = 'file://' + path;
            }
            this.fileSystem.request(this.PageReader.reader.getBasePath(sourcePATH))
                .readFile()
                .get()
                .then((imageBuffer) => {
                    const blob = new Blob([imageBuffer], {type: 'image/jpg'});
                    const pathUrl = URL.createObjectURL(blob);


                    let image = new Image();
                    image.onload = function (evt) {

                        selectedImage.style['max-width'] = this['width'] + 'px';
                        selectedImage.style['width'] = '100%';


                        selectedImage.setAttribute('src', pathUrl);
                        selectedImage.setAttribute('draggable', 'false');
                        if (selectedImage.parentElement) {
                            selectedImage.parentElement.setAttribute('draggable', 'false');
                        }
                    };

                    image.src = pathUrl;
                });
        }
    }

    private clearContainerClassList(containerID: string) {

        if (document.getElementById(containerID).classList.length) {
            const classList = document.getElementById(containerID).classList;
            for (const c in classList) {
                if (!isNaN(parseInt(c))) {
                    classList.remove(classList[c]);
                }
            }
        }
    }

    private applyThemeToHigherContainers() {
        const style = this._style['theme'] ? this._style['theme']['value'] : 0;
        switch (style) {
            case EPubTextformatThemeTypeEnum.ALT0_CONTRASTE:
                document.getElementById('app-container').classList.add('contrast');
                document.getElementById('epub-reader-frame').classList.add('contrast');
                break;
            case EPubTextformatThemeTypeEnum.SEPIA:
                document.getElementById('app-container').classList.add('sepia');
                document.getElementById('epub-reader-frame').classList.add('sepia');
                break;
        }
    }

    private forceReplacementStyle() {
        this.clearContainerClassList('app-container');
        this.clearContainerClassList('epub-reader-frame');
        this.applyThemeToHigherContainers();
        const styleReplaceElement: Element = this.iframeHTML.getElementsByTagName('replace-style').item(0);
        styleReplaceElement.innerHTML = '';

        if (!styleReplaceElement) return;

        let stringStyle = "", styles = "";
        for (const tag in this._style) {
            for (const prop in this._style[tag]) {
                styles += prop + ':' + this._style[tag][prop] + "; ";
            }
            stringStyle += tag + '{' + styles + '} ';
            styles = "";
        }
        const styleElement = document.createElement('style');
        styleElement.type = 'text/css';
        styleElement.appendChild(document.createTextNode(stringStyle));
        styleReplaceElement.appendChild(styleElement);

    }

    private isVoiceOverRunning(documentHTML: any, cssUri: string, handle: any) {

        this.fileSystem.request(cssUri)
            .readFile()
            .toString((s: string) => {
                const cssFile = new Blob([s], {type: 'text/css'});
                const cssLink = URL.createObjectURL(cssFile);
                const div = (display, stretchWidth) => {
                    return this.createDOM("div", documentHTML.body, [
                        ['link', {attr: {href: cssLink, rel: 'stylesheet'}}],
                        ['replace-style', {}],
                        [
                            [[{
                                id: 'stretch',
                                draggable: false,
                                style: {
                                    display: 'inline-bock',
                                    overflow: 'hidden',
                                    float: 'left',
                                    width: stretchWidth,
                                    '-webkit-user-select': 'none',
                                    '-moz-user-select': 'none',
                                    '-ms-user-select': 'none',
                                    'user-select': 'none',
                                    'user-drag': 'none',
                                    '-webkit-user-drag': 'none'
                                }
                            }, {
                                id: 'epub'
                            }, Array.from(documentHTML.body.childNodes)]
                            ]]
                    ]);
                };


                let display = 'none';
                let stretchWidth = '100%';
                let isVoiceOverExists = false;

                if (typeof MobileAccessibility !== 'undefined') {
                    MobileAccessibility.isVoiceOverRunning(isRunning => {
                        if (isRunning) {
                            display = 'visible';
                            stretchWidth = 'calc(100% - 200px)';
                            MobileAccessibility.speak('PÁGINA CARREGADA. TOQUE NAS LATERAIS PARA PASSAR DE PÁGINA.');
                        }

                        return handle(div(display, stretchWidth));
                    });
                }

                return handle(div(display, stretchWidth));
            });
    }

    private swipeLeft(resolve) {
        this.updateEpubContainer();
        const total = (this.iframeHTML.clientWidth + this.column_gap);
        this.HammerJS.on('swipeleft', (event) => {
            ++this.countSwipes;

            this.leftPosition += -total;

            if (this.countSwipes === this.totalColumns) {
                this.leftPosition = 0;
                this.countSwipes = 0;
                this.iframeHTML.style.left = this.leftPosition + 'px';
                resolve(++this.pageNumber);
                this.HammerJS.destroy();
            } else {
                this.iframeHTML.style.left = this.leftPosition + 'px';
            }
        });
    }

    private swipeRight(resolve) {
        this.updateEpubContainer();
        const total = (this.iframeHTML.clientWidth + this.column_gap);
        this.HammerJS.on('swiperight', (event) => {
            --this.countSwipes;

            this.leftPosition += total;

            if (this.countSwipes < 0) {
                this.countSwipes = this.totalColumns - 1;
                this.leftPosition = -((total) * (this.totalColumns - 1));
                this.iframeHTML.style.left = this.leftPosition + 'px';
                resolve(--this.pageNumber);
                this.HammerJS.destroy();
            } else {
                this.iframeHTML.style.left = this.leftPosition + 'px';
            }
        });
    }

    private setTotalColumns(EPub) {
        const columns = EPub['scrollWidth'] / screen.availWidth;
        this.totalColumns = columns >= 1 ? Math.floor(columns) : Math.ceil(columns);
        this.fitImages(EPub);
    }

    private fitImages(EPub): void {
        const imgs = EPub.getElementsByTagName('img');

        for (let img of imgs) {
            if (img) {
                img.style.width = 'auto';
                img.style.maxWidth = '50vh';
            }
        }
    }

    private async mobileSizeOf(args) {
        return new Promise((resolve, reject) => {

            window.resolveLocalFileSystemURL(args, (fileEntry: any) => {

                fileEntry.file(file => {
                    const reader = new FileReader();

                    reader.onloadend = function (evt) {

                        let image = new Image();
                        image.onload = function (evt) {

                            const width = this['width'];
                            const height = this['height'];
                            image = null;
                            return resolve({width, height});
                        };

                        image.src = evt.target['result'];
                    };

                    reader.readAsDataURL(file);

                }, reject);

            }, error => {
                //
            });

        });
    }

    private setHTMLStyle(iframeStyles: {}, iframe?: Element) {
        const htmlBody = this.iframeHTML.getElementsByTagName('body')[0];

        htmlBody['style']['position'] = 'relative';
        htmlBody['style']['padding'] = '0px';

        for (const style in iframeStyles['default']) {
            this.iframeHTML['style'][style] = iframeStyles['default'][style];
        }
        return iframe || this.epubContainer;
    }

    private updateContainerDimensions(dimensions?: any) {
        if (!dimensions) {
            this.frameDimension['scrollWidth'] = this.iframeHTML.scrollWidth;
            this.frameDimension['offsetWidth'] = this.iframeHTML.offsetWidth;
            this.frameDimension['scrollHeight'] = this.iframeHTML.scrollHeight;
            this.frameDimension['offsetHeight'] = this.iframeHTML.offsetHeight;
        } else {
            this.frameDimension = dimensions;
        }
        this.updateEpubContainer(this.iframeHTML);
    }

    private defaultFrameDimensions(width?: any, height?: any) {
        width = width || this.core_width;
        height = height || this.core_height;
        const iframeStyles = {};

        iframeStyles['default'] = {
            'position': 'relative',
            'margin': 0,
            'left': '0px',
            'top': '0px',
            'column-gap': this.column_gap + 'px',
            'column-fill': 'auto',
            '-webkit-column-width': this.iframeHTML.clientWidth + 'px',
            '-webkit-column-count': 'auto',
            'height': height - this.totalExcess + 'px',
            'minHeight': height - this.totalExcess + 'px',
            'maxHeight': height - this.totalExcess + 'px',
            'max-width': window.outerWidth + 'px',
        };
        return iframeStyles;
    }

}
