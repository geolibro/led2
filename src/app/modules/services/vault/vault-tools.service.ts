import { Injectable, Inject, EventEmitter } from '@angular/core';
import { BookEvent } from "../../events/book/book.event";
import { PlatformEnum } from '../contracts/platform/platform.enum';
import { LayoutFileSystemEnum } from '../contracts/file-system/layout-file-system.enum';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { AppEvent } from '../../events/app.event';
import * as IScroll from 'iscroll';
import { BookDownloadProgressContract } from "../../../contracts/book-download-progress-contract";


@Injectable()
export class VaultToolsService {
    noImageCover: string;
    /**
     *
     * @type {{string}} Base64 string image
     */
    private _image: string;

    /**
     * @type {{string}} BOOK_SALT
     */
    private _salt: string = "_DEV_DACCORD_PLURI";

    public iScroll: any;
    public progress: BookDownloadProgressContract = {};

    constructor(@Inject('FileSystemService') private fs: LedFileSystemContract,
        @Inject('PlatformService') private platform: PlatformContract,
        private appEvent: AppEvent,
        private bookEvent: BookEvent) { }

    get salt(): string {
        return this._salt;
    }

    initModals() {
        $('.modal').modal({
            dismissible: false,
            opacity: 0.5,
            inDuration: 300,
            outDuration: 200,
            startingTop: '4%',
            endingTop: '10%',
            ready: (modal, trigger) => {
                this.bookEvent.EchoAriaHiddenModal.emit(true);
                $('.modal-title-link').focus();
            },
            complete: (modal, trigger) => {
                this.bookEvent.EchoAriaHiddenModal.emit(false);
            }
        });
    }

    initScroll(eventBookLoading: EventEmitter<boolean>) {
        if (this.platform.is(PlatformEnum.IOS)) {
            eventBookLoading.subscribe(isLoading => {
                if (!isLoading) {
                    try {
                        this.iScroll = new IScroll("#wrapper", {
                            tap: true,
                            click: true,
                            mouseWheel: true
                        });
                    } catch (error) {
                        console.log(error);
                    }
                }
            });
        }
    }

    refreshScroll() {
        if (this.platform.is(PlatformEnum.IOS)) {
            try {
                if (this.iScroll) this.iScroll.refresh();
            } catch (error) {
                console.log(error);
            }
        }
    }

    initSideNav() {
        $('.button-collapse').sideNav({
            edge: 'left',
            closeOnClick: true,
            draggable: false,
            onOpen: () => {
                $('body').css('width', '100%');
                this.appEvent.EchoAriaHidden.emit(false);
            },
            onClose: () => {
                this.appEvent.EchoAriaHidden.emit(true);
                // this.router.navigateByUrl('/login  ');
            }
        });
    }

    sanitizeWord(name: string) {
        name = (name || "").toLowerCase().replace(/\s+/, " ");
        name = name.replace(/[áàãâä]/g, "a");
        name = name.replace(/[óòõôö]/g, "o");
        name = name.replace(/[úùûü]/g, "u");
        name = name.replace(/[éèêë]/g, "e");
        name = name.replace(/[íìîï]/g, "i");
        name = name.replace(/[ý]/g, "y");
        name = name.replace(/[ç]/g, "c");
        name = name.replace(/[ñ]/g, "n");
        name = name.replace(/[ß]/g, "ss");
        return name;
    }

    getImageBase64(input: string, directory: string = '') {
        return new Promise(async (resolve, reject) => {
            try {
                const currentPath = window.location.pathname.substring(0, window.location.pathname.lastIndexOf('/desktop.html'));
                let requestPath = `${currentPath}/assets/base64/${input}.txt`;

                if (!this.platform.is(PlatformEnum.DESKTOP)) {
                    if (this.platform.is(PlatformEnum.ANDROID)) {
                        requestPath = `cdvfile://localhost/assets/www/assets/base64/${input}.txt`;
                    } else {
                        requestPath = `cdvfile://localhost/bundle/www/assets/base64/${input}.txt`;
                    }
                }

                this.fs.request(requestPath, LayoutFileSystemEnum.NO_ROOT_PATH)
                    .readFile()
                    .toString(resolve, reject);

            } catch (error) {
                console.log(error);
                reject(error);
            }
        });
    }

    setBookProgressDownload(bookProgress: BookDownloadProgressContract) {
        this.progress = bookProgress;
    }

    async loadNoImageCoverInMemory() {
        if (!this.noImageCover) this.noImageCover = <string> await this.getImageBase64('empty-book-image');
        return this.noImageCover;
    }
}
