import { Injectable } from "@angular/core";
import * as CryptoJS from "crypto-js";

@Injectable()
export class VaultDecrypterService {
    private keyBuffer;
    private keyPos;
    private engine: CryptoJS = CryptoJS;

    prepare(xorKey: string, requestId?: string) {
        if (requestId == null) {
            requestId = "general";
        }
        this.keyBuffer[requestId] = xorKey ? new Uint8Array(new Buffer(xorKey)) : "";
        this.keyPos[requestId] = 0;
    }

    bitwiseX(bytesArray: Uint8Array, requestId?: string) {
        if (requestId == null) {
            requestId = "general";
        }
        for (let i: number = 0; i < bytesArray.byteLength; i++) {
            bytesArray[i] ^= this.keyBuffer[requestId][this.keyPos[requestId]++ % this.keyBuffer[requestId].byteLength];
        }
        return bytesArray;
    }

    get(standard: string): CryptoJS {
        return this.engine[standard.toUpperCase()];
    }

    getEncode() {
        return CryptoJS.enc.Utf8;
    }

    constructor() {
        this.keyBuffer = {};
        this.keyPos = {};
    }
}
