import { List } from 'immutable';
import { Injectable, Inject } from "@angular/core";
import { LegacyBookContract } from '../../contracts/legacy-book.contract';
import { GuardConstants } from "../services/guard/consts/guard-constants";
import { SessionGuardContract } from "../services/guard/session-guard-contract";
import { Ws102Contract } from "../../requests/contracts/ws/ws102.contract";
import { Ws013Contract } from "../../requests/contracts/ws/ws013.contract";
import { HttpService } from "../services/http/http.service";
import { HttpScaoResponse } from "../services/http/util/http-scao-response";
import { Ws010Contract } from "../../requests/contracts/ws/ws010.contract";
import { AuthGuardService } from "../services/guard/auth-guard.service";
import { ContentManagerService } from "../services/content-manager/content-manager.service";
import { BookEvent } from "../events/book/book.event";
import { UserLoggedFieldsContract } from "../../contracts/user-logged-fields.contract";
import { ManifestObject } from "../services/content-manager/manifest-object";
import { DeviceContract } from "../services/contracts/device/device.contract";
import { LedFileSystemContract } from "../services/contracts/file-system/led-file-system.contract";
import { PlatformContract } from "../services/contracts/platform/platform.contract";
import { BookEnumComponent } from "../shelf/components/shelf/book.enum.component";
import { BookShelfStatusEnum } from "../shelf/components/shelf/book-shelf-status.enum.component";
import { BookShelfStatusContract } from "../../contracts/book-shelf-contract";

@Injectable()
export class BookDao {
    private bookList: List<LegacyBookContract> = List();
    private disciplineList: string[] = [];
    private bookUpgradeable: any = {};
    private bookShelfStatus: BookShelfStatusContract = <BookShelfStatusContract>{};
    private updatedBookList: Array<any> = [];
    private _keyList: Array<{ book_led_id: string; book_key: string }> = [];
    private _ContentManager: ContentManagerService;
    private _AuthGuard: AuthGuardService;

    constructor(
        @Inject("FileSystemService") private fileSystem: LedFileSystemContract,
        private http: HttpService,
        @Inject("DeviceService") private device: DeviceContract,
        private bookEvent: BookEvent,
        @Inject("PlatformService") private platform: PlatformContract
    ) {}

    get ContentManager() {
        return this._ContentManager;
    }

    set ContentManager(contentManager: ContentManagerService) {
        this._ContentManager = contentManager;
    }

    get AuthGuard() {
        return this._AuthGuard;
    }

    set AuthGuard(authStorage: AuthGuardService) {
        this._AuthGuard = authStorage;
    }

    store(data?: LegacyBookContract | LegacyBookContract[], path?: string) {
        return new Promise((resolve, reject) => {
            const guard = this._AuthGuard.getStorage().getGuard();
            if (!guard) return;
            let dataInsert = data || guard.books;
            const ledUserId = guard.user.led_user_id;
            let bookPath = !path ? `users/${ledUserId}/${GuardConstants.DEFAULT_FILE_BOOK}` : path;
            const request = this.fileSystem.request(bookPath);
            let arrayBuffer;

            if (Array.isArray(dataInsert)) arrayBuffer = new Buffer(JSON.stringify(dataInsert));
            else {
                this.bookList.push(<LegacyBookContract>dataInsert);
                arrayBuffer = new Buffer(JSON.stringify(this.bookList));
            }
            request.writeFile(arrayBuffer, resolve, reject);
        });
    }

    mapBookList(bookLedId: string, value: string, index: string) {
        return new Promise((resolve, reject) => {
            this.bookList.map((book: LegacyBookContract) => {
                if (bookLedId === book.book_led_id) book[index] = value;
                return book;
            }).toList();
            resolve();
        });
    }

    loadKeys() {
        return new Promise((resolve, reject) => {
            const currentSession = this.getCurrentSession();
            if (!currentSession) return resolve();

            this.fileSystem
                .request(`users/${currentSession.led_user_id}/keys.libro`)
                .readFile()
                .toJSON(
                    async json => {
                        this._keyList = json;
                        for (let item of json) {
                            await this.mapBookList(item.book_led_id, item.book_key, "book_key");
                        }
                        resolve();
                    },
                    error => {
                        console.log(error);
                        resolve();
                    }
                );
        });
    }

    findById(bookLedId: string): Promise<LegacyBookContract> {
        return new Promise((resolve, reject) => {
            const selectedBook = this.bookList.filter(book => {
                return book.book_led_id === bookLedId;
            }).toArray();

            return resolve(selectedBook[0] || <LegacyBookContract>{});
        });
    }

    getBookShelfStatusList() {
        return this.bookShelfStatus;
    }

    getBookUpgradeableList() {
        return this.bookUpgradeable || [];
    }

    clear() {
        this.bookList = this.bookList.clear();
        this._keyList = [];
        this.disciplineList = [];
        this.bookShelfStatus = {};
        this.updatedBookList = [];
    }

    popDisciplineList() {
        if (this.disciplineList.length > 0) return;
        if (!this.bookList.size) return;

        let disciplineList = [];

        this.bookList.map(book => {
            disciplineList.push(book.book_discipline);
        });

        this.disciplineList = disciplineList.filter(function (el, pos, self) {
            return self.indexOf(el) === pos;
        });
    }

    fetchData(): Promise<List<LegacyBookContract>> {
        return new Promise(async (resolve, reject) => {
            try {
                let list: any = this.bookList;
                let shouldIFetchData = (this.bookList instanceof List) ? (list.size == 0) : (list.length == 0);
                list = null;

                if (shouldIFetchData) this.bookList = await this.fetchAll();

                await this.loadKeys();

                this.popDisciplineList();

                return resolve(this.bookList || List([]));
            } catch (err) {
                console.log('[BookDao][fetchData][ERROR] - ' + JSON.stringify(err));
                return reject(err);
            }
        });
    }

    bookHasUpdate(book_led_id: string, bookList: any): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            try {
                let book = await this.findById(book_led_id);
                if (book) {
                    let manifest: ManifestObject = <ManifestObject>await this._ContentManager.getFilesManifest(book_led_id);
                    resolve(book.last_version != parseInt(manifest.version_number));
                }
                resolve(false);
            } catch (err) {
                return reject(err);
            }
        });
    }

    installBookUpdate(bookLedId: string): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            const book = await this.findById(bookLedId);
            const oldBookManifest = await this._ContentManager.getFilesManifest(bookLedId);
            const newBookManifest = await this._getNewBookManifest(bookLedId, book);
            await this._ContentManager.saveFilesManifest(bookLedId, newBookManifest);

            let isBookUpdated = await this._ContentManager.prepareBookForUpdate(bookLedId, oldBookManifest, newBookManifest);
            if (isBookUpdated) {
                resolve(true);
            } else {
                resolve(false);
            }
        });
    }

    getBooksDisciplineList() {
        return this.disciplineList;
    }

    loadBookStatus(contentManager?: ContentManagerService) {
        return new Promise(async (resolve, reject) => {
            const cManager = this._ContentManager || contentManager;

            try {
                this.bookShelfStatus            = this.getCleanBookShelfStatusList();
                const booksPhysicalStatusData   = await cManager.getBookStatusData();

                for (let bookLedIdOfStatusData in booksPhysicalStatusData) {
                    this.bookShelfStatus[bookLedIdOfStatusData] = booksPhysicalStatusData[bookLedIdOfStatusData].book_status;
                    try {
                        this.bookUpgradeable[bookLedIdOfStatusData] = await this.bookHasUpdate(bookLedIdOfStatusData, this.updatedBookList || []);
                        await this.changeBookStatus();
                    } catch (err) {
                        console.log(err);
                    }
                }
                return resolve();
            } catch (error) {
                let strError = `[BookDao][loadBookStatus][ERROR] - ${error}`;
                console.log(strError);
                return reject(strError);
            }
        });
    }

    async getKeyById(bookLedId: string, license?: string) {
        if (!bookLedId) return;

        const book = await this.findById(bookLedId);
        let bookKey = book["book_key"];

        if (license && !bookKey) {
            const params = await this.getWs102Contract(book, license);
            const response: HttpScaoResponse = await this.http.makeRequest(this.http.getURL("scl", "ws102"), params);
            bookKey = response.all().book_key;
        }

        return bookKey;
    }

    async install(book: LegacyBookContract | string) {
        const bookLedId = typeof book == "string" ? book : book.book_led_id,
            response = await this.notifyInstallation(bookLedId);
        return response;
    }

    async uninstall(book: LegacyBookContract) {
        const bookLedId: string = book.book_led_id;
        const params = await this.getWs010Contract(bookLedId);
        await this.notifyUninstallation(params);
        this._ContentManager.uninstallBook(bookLedId);
        this.bookEvent.EchoBookDeleted.emit(bookLedId);
        return true;
    }

    async downloadBook(book: any) {
        try {
            const manifest = await this.install(book);
            this._ContentManager.onReceiveBookManifest(book.book_led_id, manifest, book.download_urls);
        } catch (error) {
            throw new Error(`[BookDao][DownloadBook][ERROR] - ${error}`);
        }
    }

    /**
     * Webservice: (SCAO) WS013
     * http://dacsrv/wiki/index.php/SCAO#WS013_-_Informar_a_instala.C3.A7.C3.A3o_de_uma_obra_no_dispositivo
     */
    private async notifyInstallation(bookLedId: string): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const book = await this.findById(bookLedId);

                if (!book.hasOwnProperty("book_key") || !book.book_key) {
                    try {
                        const params = await this.getWs013Contract(bookLedId);
                        const response = await this.http.makeRequest(this.http.getURL("scao", "ws013"), params);

                        await this.storeBookKey(bookLedId, response);
                        await this._AuthGuard.getStorage().updateUserLiteraryWorks(this.bookList.toArray());
                        await this.store();
                        await this.fetchAll();
                        await this._ContentManager.saveFilesManifest(bookLedId, response.all().manifest);

                        return resolve(response.all().manifest);
                    } catch (error) {
                        let strError = `[BookDao][notifyInstalation][BookKeyBlock][ERROR] - ${error}`;
                        return reject(strError);
                    }
                }

                const filesManifest = await this._ContentManager.getFilesManifest(book.book_led_id);
                return resolve(filesManifest);
            } catch (error) {
                let strError = `[BookDao][notifyInstallation][ERROR] - ${error}`;
                return reject(strError);
            }
        });
    }

    /**
     * Webservice: (SCAO) WS010
     * http://dacsrv/wiki/index.php/SCAO#WS010_-_Desinstalar_obra_do_dispositivo
     */
    private async notifyUninstallation(params: Ws010Contract): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const response: HttpScaoResponse = await this.http.makeRequest(this.http.getURL("scao", "ws010"), params);
                await this.deleteBookKey(params.book_led_id);
                return resolve(response.all());
            } catch (error) {
                reject(error.message);
            }
        });
    }

    private async storeBookKey(bookLedId: string, response: HttpScaoResponse) {
        return new Promise(async (resolve, reject) => {
            const bookKey = await this.getKeyById(bookLedId, response.all().license);

            await this.mapBookList(bookLedId, bookKey, "book_key");

            const currentSession = this.getCurrentSession();
            this._keyList.push({ book_led_id: bookLedId, book_key: bookKey });
            const data = new Buffer(JSON.stringify(this._keyList));
            this.fileSystem.request(`users/${currentSession.led_user_id}/keys.libro`).writeFile(data, resolve, reject);
        });
    }

    /**
     * @param bookLedId
     * @returns {{Ws013Contract}}
     */
    private async getWs013Contract(bookLedId: string): Promise<Ws013Contract> {
        const currentSession = this._AuthGuard.getStorage().getGuard().user;
        const deviceId = await this._AuthGuard.getDevice().getUniqueIdentifier();
        return {
            operation: "J036",
            login: currentSession.login,
            password: currentSession.password,
            device_id: deviceId,
            book_led_id: bookLedId
        };
    }

    private async getWs102Contract(book: LegacyBookContract, license: string): Promise<Ws102Contract> {
        const deviceId = await this._AuthGuard.getDevice().getUniqueIdentifier();
        const currentSession = this._AuthGuard.getStorage().getGuard().user;
        let ws102: Ws102Contract = {
            book_led_id: book.book_led_id,
            book_license_type: book.book_license_type,
            device_id: deviceId,
            login: currentSession.login,
            password: currentSession.password,
            license: license,
            operation: "J037",
            device_info: null,
            device_name: null
        };

        /** Gambi Fix WS102 */
        const deviceParamsWs102 = this.device.prepareDeviceInfoRequest(),
            deviceInfo = deviceParamsWs102.device_info.shift();

        ws102.device_info = deviceInfo;
        ws102.device_name = deviceParamsWs102.device_name;

        return ws102;
    }

    private async getWs010Contract(bookLedId: string) {
        const deviceId = await this._AuthGuard.getDevice().getUniqueIdentifier();
        return {
            book_led_id: bookLedId,
            device_id: deviceId,
            login: this._AuthGuard.user().login,
            operation: "J030",
            password: this._AuthGuard.user().password
        };
    }

    private getCleanBookShelfStatusList(): BookShelfStatusContract {
        let bookShelfStatusList = {};
        for (const book of this.bookList.toArray()) {
            bookShelfStatusList[book.book_led_id] = BookShelfStatusEnum.NOT_INSTALLED_OR_NEW;
        }
        return bookShelfStatusList;
    }

    private getFileSession(PATH: string): Promise<any> {
        return new Promise(async (resolve, reject) => {
            await this.platform.ready();
            this.fileSystem
                .request(PATH)
                .readFile()
                .toJSON(
                    (response: SessionGuardContract[]) => {
                        resolve(response);
                    },
                    err => {
                        console.log(err);
                    }
                );
        });
    }

    private fetchAll(): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const guard = this._AuthGuard ? this._AuthGuard.getStorage().getGuard() : null;

                if (guard) {
                    this.bookList = List(guard.books);
                    resolve(this.bookList);
                } else this.getBookFromCurrentSession(resolve);
            } catch (err) {
                return reject(err);
            }
        });
    }

    private getBookFromCurrentSession(resolve) {
        const currentSessionPATH = GuardConstants.DEFAULT_FILE_CURRENT_SESSION;
        this.fileSystem.exists(currentSessionPATH, async exists => {
            if (exists) {
                const currentSession: SessionGuardContract = await this.getFileSession(currentSessionPATH);
                this.bookList = List(currentSession.books);
                resolve(this.bookList);
            } else {
                return resolve([]);
            }
        });
    }

    private getCurrentSession(): UserLoggedFieldsContract {
        return this._AuthGuard.user();
    }

    private _getNewBookManifest(bookLedId: string, book: any) {
        return new Promise(async (resolve, reject) => {
            if (book.hasOwnProperty("book_key") || book.book_key) {
                const params = await this.getWs013Contract(bookLedId);
                const response = await this.http.makeRequest(this.http.getURL("scao", "ws013"), params);

                return resolve(response.all().manifest);
            }
        });
    }

    private changeBookStatus(): Promise<BookShelfStatusContract> {
        return new Promise( (resolve, reject) => {
            try {
                this.bookList.map(book => {
                    switch (book.book_status) {
                        case BookEnumComponent.INVALID:
                        case BookEnumComponent.EXPIRED:
                        case BookEnumComponent.UNAVAILABLE:
                        case BookEnumComponent.SUSPENDED:
                            this.bookShelfStatus[book.book_led_id] = BookShelfStatusEnum.EXPIRED;
                            break;
                    }
                });

                return resolve(this.bookShelfStatus);
            } catch (error) {
                return reject(error);
            }
        });
    }

    private deleteBookKey(bookLedId: string) {
        return new Promise(async (resolve, reject) => {
            this.clearBookKey(bookLedId);
            const currentSession = this.getCurrentSession();
            this._keyList = this._keyList.filter(keyList => keyList.book_led_id !== bookLedId);
            const data = new Buffer(JSON.stringify(this._keyList));
            this.fileSystem.request(`users/${currentSession.led_user_id}/keys.libro`).writeFile(data, resolve, reject);
        });
    }

    private clearBookKey(bookLedId: string) {
        this.bookList = this.bookList.map((book: LegacyBookContract) => {
            if (bookLedId === book.book_led_id && book.hasOwnProperty("book_key")) delete book.book_key;
            return book;
        }).toList();
    }
}
