import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {CurtainMobileComponent} from "./components/curtain-mobile/curtain-mobile.component";
import {CurtainDesktopComponent} from "./components/curtain-desktop/curtain-desktop.component";
import {FormsModule} from "@angular/forms";
import {CurtainComponent} from "./components/curtain/curtain.component";
import {CurtainDirective} from "./components/curtain/curtain.directive";

@NgModule({
    imports: [
        CommonModule, FormsModule
    ],
    declarations: [
        CurtainComponent,
        CurtainDirective,
        CurtainMobileComponent,
        CurtainDesktopComponent
    ],
    exports: [
        CurtainMobileComponent,
        CurtainDesktopComponent,
        CurtainComponent
    ],
    entryComponents: [
        CurtainDesktopComponent,
        CurtainMobileComponent
    ]
})
export class CurtainModule {}
