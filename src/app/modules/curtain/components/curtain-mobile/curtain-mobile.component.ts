import {Component, ElementRef, HostListener, Input, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {BookEvent} from "../../../events/book/book.event";

@Component({
    selector: 'app-curtain-mobile-component',
    templateUrl: "./curtain-mobile.component.html",
    styleUrls: ["./curtain-mobile.component.css"]

})

export class CurtainMobileComponent implements OnInit, OnDestroy {
    @ViewChild('curtain') curtain: ElementRef;

    private observables = [];
    public isEnable: boolean;
    headerOffsetHeight: any;
    footerOffsetY: any;
    windowWidth: any = (window.innerWidth - 1);
    windowHeight: any = window.innerHeight;

    @Input() x: number;
    @Input() y: number;
    @Input() width: number;
    @Input() height: number;
    px: number;
    py: number;
    minArea: number;
    draggingCorner: boolean;
    draggingWindow: boolean;
    resizer: Function;

    constructor(private bookEvent: BookEvent) {
        this.isEnable = false;
        this.x = 0;
        this.y = 0;
        this.px = 0;
        this.py = 0;
        this.width = 0;
        this.height = 0;
        this.draggingCorner = false;
        this.draggingWindow = false;
        this.minArea = 20000
    }

    ngOnInit() {
        console.log('App Curtain Mobile Component');

        this.onDeviceOrientation();

        this.observables.push(this.bookEvent.EchoCourtain.subscribe((isEnable) => {
            if (isEnable) {
                this.isEnable = true;
                this.checkConstraints();
                this.bootstrapCurtain();
            } else {
                this.isEnable = false;
            }
        }));

        window.addEventListener('resize', () => {
            this.windowResize();
        });

    }

    ngOnDestroy() {
        this.close();
        for (let evt of this.observables) {
            evt.unsubscribe();
        }
    }

    checkConstraints() {

        if (document.getElementById('legacyActionBar').style.visibility === 'hidden') {
            this.headerOffsetHeight = 0;
            this.footerOffsetY = 0;
            this.windowHeight = window.innerHeight;
        } else if (document.getElementById('legacyActionBar').style.visibility === 'visible') {
            this.headerOffsetHeight = document.getElementById('legacyActionBar').offsetHeight;
            this.footerOffsetY = document.getElementsByTagName('footer')[0].offsetHeight;
            this.windowHeight = window.innerHeight - (this.headerOffsetHeight + this.footerOffsetY);
        }
    }

    bootstrapCurtain() {
        this.y = this.headerOffsetHeight;
        this.x = 0;
        this.width = this.windowWidth;
        this.height = this.windowHeight;
    }

    onDeviceOrientation() {
        window.addEventListener('orientationchange', (event) => {
            setTimeout(() => {
                this.checkConstraints();
                this.bootstrapCurtain();
            }, 300);
        });
    }

    windowResize() {
        this.windowWidth = window.innerWidth;
        this.windowHeight = window.innerHeight;
        this.checkConstraints();
        this.bootstrapCurtain();
    }

    close() {
        this.isEnable = false;
    }

    area() {
        return this.width * this.height;
    }

    onWindowPress(event: any) {
        this.draggingWindow = true;
        this.px = event.touches[0].clientX;
        this.py = event.touches[0].clientY;
    }

    onWindowDrag(event: any) {
        if (!this.draggingWindow) {
            return;
        }

        let offsetX = event.touches[0].clientX - this.px;
        let offsetY = event.touches[0].clientY - this.py;

        this.px = event.touches[0].clientX;
        this.py = event.touches[0].clientY;

        if (event.target.offsetTop < this.headerOffsetHeight) {
            this.y = this.headerOffsetHeight;
            return;
        }

        if (event.target.offsetLeft < 0) {
            this.x  = 0;
            return;
        }

        if ((event.target.offsetLeft + event.target.offsetWidth) > this.windowWidth) {
            this.x  = this.windowWidth-event.target.offsetWidth;
            return;
        }

        const curtainOffsetTop = event.target.offsetTop;
        const curtainDivHeight = event.target.offsetHeight;
        const heightScreen = window.innerHeight;
        const footer = this.footerOffsetY;

        if (curtainOffsetTop+curtainDivHeight > heightScreen - footer) {
            this.y = (heightScreen-footer-curtainDivHeight);
            return;
        }

        this.x += offsetX;
        this.y += offsetY;
    }

    topResize(offsetX: number, offsetY: number) {

        let y = this.y;
        let height = this.height;

        y += offsetY;
        height -= offsetY;

        if (y < this.headerOffsetHeight) {
            this.y = this.headerOffsetHeight;
        } else {
            this.y = y;
            this.height = height;
        }

    }

    leftResize(offsetX: number, offsetY: number) {

        let x = this.x;
        let width = this.width;

        x += offsetX;
        width -= offsetX;


        if (x < 0) {
            return;
        } else {
            this.x = x;
            this.width = width;
        }


    }

    rightResize(offsetX: number, offsetY: number) {
        if (offsetX > 0) {

            if ( (this.curtain.nativeElement.offsetLeft + this.width) >= this.windowWidth ) {
                return
            } else {
                this.width += offsetX;
            }

        } else {
            this.width += offsetX;
        }
    }

    bottomResize(offsetX: number, offsetY: number) {

        let height = this.height;

        if (offsetY > 0) {
            if ( (height + (this.curtain.nativeElement.offsetTop - this.headerOffsetHeight)) > (this.windowHeight - 1)) {
                return;
            } else {
                height += offsetY;
            }
        } else {
            height += offsetY;
        }

        this.height = height;

    }

    topRightResize(offsetX: number, offsetY: number) {
        this.topResize(offsetX, offsetY);
        this.rightResize(offsetX, offsetY);
    }

    topLeftResize(offsetX: number, offsetY: number) {
        this.topResize(offsetX, offsetY);
        this.leftResize(offsetX, offsetY);
    }

    bottomLeftResize(offsetX: number, offsetY: number) {
        this.bottomResize(offsetX, offsetY);
        this.leftResize(offsetX, offsetY);
    }

    bottomRightResize(offsetX: number, offsetY: number) {
        this.bottomResize(offsetX, offsetY);
        this.rightResize(offsetX, offsetY);
    }

    onCornerClick(event: any, resizer?: Function) {
        this.draggingCorner = true;
        this.px = event.touches[0].clientX;
        this.py = event.touches[0].clientY;
        this.resizer = resizer;
        event.preventDefault();
        event.stopPropagation();
    }

    @HostListener('document:touchmove', ['$event'])
    onCornerMove(event: any) {
        if (!this.draggingCorner) {
            return;
        }

        let offsetX = event.touches[0].clientX - this.px;
        let offsetY = event.touches[0].clientY - this.py;

        let lastX = this.x;
        let lastY = this.y;

        let pWidth = this.width;
        let pHeight = this.height;

        this.resizer(offsetX, offsetY);

        if (this.area() < this.minArea) {
            this.x = lastX;
            this.y = lastY;
            this.width = pWidth;
            this.height = pHeight;
        }

        this.px = event.touches[0].clientX;
        this.py = event.touches[0].clientY;

    }

    @HostListener('document:touchend', ['$event'])
    onCornerRelease(event: any) {
        this.draggingWindow = false;
        this.draggingCorner = false;
    }
}
