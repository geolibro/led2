import {Component, ComponentFactoryResolver, Inject, OnInit, ViewChild} from '@angular/core';
import {CurtainMobileComponent} from "../curtain-mobile/curtain-mobile.component";
import {PlatformContract} from "../../../services/contracts/platform/platform.contract";
import {PlatformEnum} from "../../../services/contracts/platform/platform.enum";
import {CurtainDesktopComponent} from "../curtain-desktop/curtain-desktop.component";
import {CurtainDirective} from "./curtain.directive";

@Component({
    selector: 'app-curtain-component',
    templateUrl: "./curtain.component.html",
    styleUrls: ["./curtain.component.css"]

})
export class CurtainComponent implements OnInit {
    @ViewChild(CurtainDirective) curtainDirective: CurtainDirective;
    constructor(private factoryResolver: ComponentFactoryResolver, @Inject('PlatformService') private platform: PlatformContract) {}

    ngOnInit() {
        this.loadComponent();
    }

    private loadComponent() {
        let platformComponent;
        if (this.platform.is(PlatformEnum.DESKTOP)) platformComponent = CurtainDesktopComponent;
        else if (this.platform.is(PlatformEnum.MOBILE)) platformComponent = CurtainMobileComponent;
        else throw new Error('No component found');

        const resolverComponent = this.factoryResolver.resolveComponentFactory(platformComponent);
        this.curtainDirective.viewContainerRef.clear();
        this.curtainDirective.viewContainerRef.createComponent(resolverComponent);
    }
}
