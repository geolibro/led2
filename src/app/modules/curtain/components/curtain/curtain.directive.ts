import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
    selector: '[curtain-host]',
})
export class CurtainDirective {
    constructor(public viewContainerRef: ViewContainerRef) {}
}
