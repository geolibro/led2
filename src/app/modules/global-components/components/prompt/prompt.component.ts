import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
    selector: 'app-prompt',
    templateUrl: './prompt.component.html',
    styleUrls: ['./prompt.component.css']
})
export class PromptComponent implements OnInit, OnDestroy {

    modal: any;
    title: string;
    clickValue: boolean;
    input: any;
    promptOk: HTMLElement;
    promptCancel: HTMLElement;
    private option: {};


    ngOnInit() {
        this.input = document.getElementById('promptInput');
        this.promptOk = document.getElementById('promptOk');
        this.promptCancel = document.getElementById('promptCancel');
        $(document).ready(() => {
            this.modal = $('#PromptModal').modal({
                dismissible: true,
                opacity: .0,
                inDuration: 300,
                outDuration: 200,
                startingTop: '4%',
                endingTop: '10%'
            })

        });

    }

    onPrompt(): Promise<any> {
        this.input.value = '';
        this.modal.modal('open');
        this.input.focus();
        document.getElementsByClassName('modal-overlay')[0]['style'].display = 'none';
        return new Promise((resolve, reject) => {
            this.input.addEventListener('keypress', (e) => {
                if (e.keyCode === 13) {
                    // Trigger the button element with a click
                    this.promptOk.click();
                }
            });
            this.promptOk.addEventListener('click', () => {
                this.modal.modal('close');
                resolve({
                    ok: true,
                    input: this.input.value
                })
            });
            this.promptCancel.addEventListener('click', () => {
                this.modal.modal('close');
                resolve({
                    ok: false,
                    input: this.input.value
                })

            });

        });
    }

    ngOnDestroy() {
        this.input = null;
    }

}
