import { Component, OnInit } from '@angular/core';
import { AppEvent } from '../../../events/app.event';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  hiddenNavbar: boolean = false;

  constructor(protected app: AppEvent) {}

  ngOnInit() {
    this.app.EchoVisibleComponent.subscribe(value => {
          this.hiddenNavbar = !value.navbar;
      });
  }

}
