import {Component, DoCheck, Input, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-component-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent {

  // fixme: posicionar o dropdown-content sobre o botão clicado
  // todo: desativar todos os outros eventos  quando o dropdown-content estiver ativo

  @Input() id: string = '';

  protected options: Materialize.DropDownOptions = {
    inDuration: 600,
    outDuration: 625,
    constrainWidth: false,
    hover: false,
    gutter: 0,
    belowOrigin: false,
    alignment: 'left',
    stopPropagation: false
  };

  constructor() {}

  setDropdownId(newId: string) {
    this.id = newId;
  }

}
