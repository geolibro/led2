import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-component-pre-loader',
    templateUrl: './pre-loader.component.html',
    styleUrls: ['./pre-loader.component.css']
})
export class PreLoaderComponent implements OnInit {
    private loader: any;
    ngOnInit() {
        this.loader = $('#preLoader');

        this.loader.fakeLoader({
            timeToHide: 2000,
            bgColor: '#f2f2f2',
            spinner: 'spinner2',
            zIndex: 9999
        });
    }

    show() {
        this.loader.show();
    }

    hide() {
        this.loader.hide();
    }
}
