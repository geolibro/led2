import {Component, OnDestroy, OnInit} from '@angular/core';
import {BookEvent} from '../../../events/book/book.event';

@Component({
  selector: 'app-audio-player',
  templateUrl: './audio-player.component.html',
  styleUrls: ['./audio-player.component.css']
})
export class AudioPlayerComponent implements OnInit, OnDestroy {
  song: any;
  duration: any;
  pauseButton: any;
  playButton: any;
  seek: any;
  close: any;
  audioPlayer: any;
  observables: any = {};
  constructor(private bookEvent: BookEvent) {
    this.song = new Audio();
    this.observables.echoAudioPlayer = {};
    this.observables.echoKillAudio = {};
  }

  ngOnInit() {
    this.audioPlayer = $('#audioPlayer');
    this.pauseButton = document.getElementById('pause');
    this.playButton = document.getElementById('play');
    this.seek = document.getElementById('seek');
    this.close = document.getElementById('closePlayer');
    this.seek.max = this.song.duration;

    this.observables.echoAudioPlayer = this.bookEvent.EchoAudioPlayer.subscribe((audioLoaded) => {
      if (this.song) this.song.pause();
      this.audioPlayer.fadeIn(2000);
      this.song = new Audio(audioLoaded);
      this._updateSongTime();
      this._loadSeek();
      this.playButton.click();
    });

    this.observables.echoKillAudio = this.bookEvent.EchoKillAudio.subscribe((killAudio) => {
      this.song.pause();
      this.song.currentTime = 0;
      if (!killAudio) this.audioPlayer.fadeOut(2000);
    });
    this._loadPlayButton();
    this._loadPauseButton();
    this._loadClose();
  }

  private _loadPlayButton() {
    this.playButton.addEventListener('click', (e) => {
      if (this.song) {
        this.playButton.style.display = 'none';
        this.pauseButton.style.display = 'block';
        this.song.play();
        this.bookEvent.EchoKillAudioWithoutControl.emit(true);
        this.bookEvent.EchoAudioPlayerRunning.emit(true);
      }
    });
  }

  private _loadPauseButton() {
    this.pauseButton.addEventListener('click', (e) => {
      this.pauseButton.style.display = 'none';
      this.playButton.style.display = 'block';
      this.bookEvent.EchoKillAudioWithoutControl.emit(false);
      this.bookEvent.EchoKillAudio.emit(true);
    });
  }

  private _loadSeek() {
    ['input'].forEach((event) => {
      this.seek.addEventListener(event, (e) => {
        this.song.currentTime = parseInt(e.target.value, 10);
      });
    });
  }

  private _loadClose() {
    this.close.addEventListener('click', (e) => {
      this.pauseButton.click();
      this.audioPlayer.fadeOut(2000);
    });
  }

  private _updateSongTime() {
    this.song.addEventListener('timeupdate', (e) => {
      this.seek.value = parseInt(this.song.currentTime, 10);
    });
    this.song.addEventListener('loadedmetadata', (e) => {
      this.seek.max = this.song.duration;
    })
  }

  ngOnDestroy() {
    this.observables.echoAudioPlayer.unsubscribe();
    this.observables.echoKillAudio.unsubscribe();
  }

}
