import { Component, OnInit, Input, Inject } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { Router } from "@angular/router";
import { UserLoginFieldsContract } from "../../../../contracts/user-login-fields.contract";
import { AppEvent } from "../../../events/app.event";
import { environment } from "../../../../../environments/environment";
import { AuthGuardService } from "../../../services/guard/auth-guard.service";
import { PlatformContract } from "../../../services/contracts/platform/platform.contract";
import { LedFileSystemContract } from "../../../services/contracts/file-system/led-file-system.contract";
import { PlatformEnum } from "../../../services/contracts/platform/platform.enum";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    isBtnLoginDisabled: boolean;

    @Input() forgot_password_link = environment._url.forgot_password_link;

    @Input()
    LoginPost: UserLoginFieldsContract = {
        operation: 'J017',
        login: '',
        password: '',
        remember: true,
        device_id: null,
        device_name: null,
        device_info: [{
            os_name: null,
            os_version: null,
            model: null,
            screen_width: null,
            screen_height: null
        }]
    };

    constructor(
        private appEvent: AppEvent,
        @Inject('PlatformService') private platform: PlatformContract,
        private router: Router,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        private guard: AuthGuardService
    ) {
        this.isBtnLoginDisabled = false;
        this.platform.ready().then(() => {
            $('.container').removeAttr('class');
        });
    }

    ngOnInit() {
        this.loginForm = new FormGroup({
            'login': new FormControl(this.LoginPost.login, [Validators.required]),
            'password': new FormControl(this.LoginPost.password, [Validators.required])
        });
        this.appEvent.EchoVisibleComponent.emit({ toolbar: false, navbar: false });
    }

    async postLogin() {
        try {
            this.isBtnLoginDisabled = true;
            this.LoginPost.login = this.LoginPost.login.trim();
            await this.guard.auth(this.LoginPost);

            this.isBtnLoginDisabled = false;
            this.appEvent.EchoVisibleComponent.emit({
                toolbar: true,
                navbar: true
            });

            this.router.navigate(['shelf']);

        } catch (error) {
            this.isBtnLoginDisabled = false;
            let errorMessage = error instanceof HttpErrorResponse ? error.message : error;
            let linkTagError = '<a href="javascript:void(0);" style="color: white" id="toast-login">' + errorMessage + "</a>";
            Materialize.toast(linkTagError, 5000, 'toast-login');
            document.getElementById('toast-container').style.top = '0';
            document.getElementById('toast-container').style.bottom = '100%';
            if (this.platform.is(PlatformEnum.ANDROID)) $('.toast-login').attr('aria-live', 'assertive');
            if (this.platform.is(PlatformEnum.IOS)) $('#toast-container #toast-login').focus();
        }
    }

    private get login() {
        return this.loginForm.get('login');
    }

    private get password() {
        return this.loginForm.get('password');
    }

    forgotPassword(link) {
        this.fileSystem.openLinkExternally(link);
    }

}
