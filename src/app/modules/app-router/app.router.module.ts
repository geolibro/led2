import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../services/guard/auth-guard.service';
import { SelectivePreloadingStrategy } from '../../selective-preloading-strategy';
import { AuthResolverService } from '../services/guard/auth-resolver.service';
import { ShelfResolverService } from '../services/shelf/shelf-resolver.service';

const appRoutes: Routes = [
    { path: 'index.html', redirectTo: '/login', pathMatch: 'full' },
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    {
        path: 'login',
        loadChildren: 'app/modules/authenticator/authenticator.module#AuthenticatorModule',
        data: { preload: true },
        resolve: {
            AuthResolverService
        }
    },
    {
        path: 'shelf',
        loadChildren: 'app/modules/shelf/shelf.module#ShelfModule',
        canActivate: [AuthGuardService],
        data: { preload: true },
        resolve: {
            ShelfResolverService
        }
    },
    {
        path: 'reader',
        loadChildren: 'app/modules/reader/reader.module#ReaderModule',
        canActivate: [AuthGuardService],
        data: { preload: true }
    }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, { preloadingStrategy: SelectivePreloadingStrategy })],
    exports: [RouterModule],
    providers: [SelectivePreloadingStrategy]
})
export class AppRouterModule {}
