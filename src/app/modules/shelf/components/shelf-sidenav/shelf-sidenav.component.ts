import { Component, OnInit, Inject } from '@angular/core';
import { Router } from "@angular/router";
import { ContentManagerService } from "../../../services/content-manager/content-manager.service";
import { environment } from "../../../../../environments/environment";
import { AppEvent } from "../../../events/app.event";
import { AuthGuardService } from "../../../services/guard/auth-guard.service";
import { PlatformContract } from '../../../services/contracts/platform/platform.contract';
import { LedFileSystemContract } from '../../../services/contracts/file-system/led-file-system.contract';

declare var window;
declare var $;
@Component({
    selector: "app-shelf-sidenav-component",
    templateUrl: "./shelf-sidenav.component.html",
    styleUrls: ["./shelf-sidenav.component.css"]
})
export class ShelfSidenavComponent implements OnInit {
    hiddenNavbar = false;
    aria = true;
    client: string;

    constructor(
        private router: Router,
        @Inject('PlatformService') private plataform: PlatformContract,
        private contentManagerService: ContentManagerService,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        private appEvent: AppEvent,
        private auth: AuthGuardService
    ) {}

    ngOnInit() {
        this.client = environment.client;
        this.appEvent.EchoVisibleComponent.subscribe(value => {
            const SildeOut: any = document.getElementById("slide-out");
            if (SildeOut) SildeOut['style']['display'] = value.navbar === false ? "none" : "block";

            const exit: any = document.getElementById("exit");
            if (exit) exit.blur();

            this.aria = true;
        });
        this.appEvent.EchoAriaHidden.subscribe(value => {
            this.aria = value;
        });
    }

    exit() {
        this.auth.logout().then((sucess) => {
            this.router.navigateByUrl('/login');
        }).catch((error) => {
            console.log('Erro no componente ShelfSidenavComponent método exit' + error.code)
        });
    }
}
