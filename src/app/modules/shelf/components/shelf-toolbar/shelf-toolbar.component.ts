import { Component, ViewChild, OnInit } from '@angular/core';
import { AppEvent } from '../../../events/app.event';
import { ShelfReorderModalComponent } from '../shelf-reorder-modal/shelf-reorder-modal.component';
import { ShelfFilterModalComponent } from '../shelf-filter-modal/shelf-filter-modal.component';
@Component({
    selector: 'app-shelf-toolbar-component',
    templateUrl: './shelf-toolbar.component.html',
    styleUrls: ['./shelf-toolbar.component.css']
})
export class ShelfToolbarComponent implements OnInit {

    hiddenToolbar: boolean = false;

    @ViewChild(ShelfReorderModalComponent) reorderModal: ShelfReorderModalComponent;
    @ViewChild(ShelfFilterModalComponent) filterModal: ShelfFilterModalComponent;

    constructor(private appEvent: AppEvent) {}

    openOrderFilter() {
        this.reorderModal.title = 'Ordenamento';
        this.reorderModal.show();
    }

    openDefaultFilter() {
        this.filterModal.title = 'Filtro';
        this.filterModal.show();
    }

    ngOnInit(): void {
        this.appEvent.EchoVisibleComponent.subscribe(value => {
            this.hiddenToolbar = !value.toolbar;
        });
    }
}
