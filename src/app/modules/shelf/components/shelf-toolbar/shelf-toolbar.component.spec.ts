import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfToolbarComponent } from './shelf-toolbar.component';

describe('ShelfToolbarComponent', () => {
  let component: ShelfToolbarComponent;
  let fixture: ComponentFixture<ShelfToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
