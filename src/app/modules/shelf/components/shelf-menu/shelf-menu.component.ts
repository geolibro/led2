import { Component, OnInit, Inject } from '@angular/core';
import { PlatformContract } from '../../../services/contracts/platform/platform.contract';

@Component({
    selector: 'app-shelf-component-menu',
    templateUrl: './shelf-menu.component.html',
    styleUrls: ['./shelf-menu.component.css']
})
export class ShelfMenuComponent {

    menuList: Array<{ display: string, url: string, icon: string }>;

    constructor(@Inject('PlatformService') private platform: PlatformContract) {
        this.platform.ready().then(() => {
            this.menuList = [
                            // { display: 'EPUB READER', url: 'reader/epub', icon: 'list' },
                            // { display: 'Ajuda', url: 'help', icon: 'live_help' },
                        ];
        });
    }
}
