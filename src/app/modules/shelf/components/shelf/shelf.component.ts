import { Component, OnInit, Input, Inject, OnDestroy, SimpleChanges, ViewChild, AfterContentInit } from "@angular/core";
import { VaultToolsService } from "../../../services/vault/vault-tools.service";
import { AppEvent } from "../../../events/app.event";
import { BookEvent } from "../../../events/book/book.event";
import { LegacyBookContract } from "../../../../contracts/legacy-book.contract";
import { PlatformContract } from "../../../services/contracts/platform/platform.contract";
import { ShelfService } from "../../../services/shelf/shelf.service";
import { ActivatedRoute } from "@angular/router";
import { ShelfResolverContract } from "../../../services/shelf/shelf-resolver.contract";
import { BookListComponent } from '../book-list/book-list.component';
import { List } from 'immutable';
import { PlatformEnum } from "../../../services/contracts/platform/platform.enum";
import { BookInfoComponent } from "../book-info/book-info.component";
import { BookDownloadProgressContract } from "../../../../contracts/book-download-progress-contract";
import { BookShelfStatusContract } from "../../../../contracts/book-shelf-contract";
import { BookShelfStatusEnum } from "./book-shelf-status.enum.component";
import { DialogService } from "../../../services/mobile/dialog.service";
import { DownloadStopReason } from "../../../services/content-manager/download-manager.service";

@Component({
    selector: "app-shelf-component",
    templateUrl: "./shelf.component.html",
    styleUrls: ["./shelf.component.css"]
})
export class ShelfComponent implements OnInit, OnDestroy {
    /**
     * BookList provided from BookDao
     * Pre fetching data in shelf resolver
     */
    @Input() bookList: List<LegacyBookContract>;

    /**
     * Upgradeable bookList from BookDao
     * Pre fetching data in shelf resolver
     */
    @Input() bookUpgradeable: Array<any> = [];

    @Input() bookDownloadProgress: BookDownloadProgressContract = {};
    @Input() bookShelfStatus: BookShelfStatusContract = {};

    @Input() emptyBookImage: string = "";
    @Input() isLoadingActive: boolean = true;
    @Input() wrapperDivId: string = "wrapper";
    @Input() scrollableDivId: string = "scroller";

    @ViewChild(BookListComponent) BookListComponent: BookListComponent;
    @ViewChild(BookInfoComponent) BookInfoComponent: BookInfoComponent;

    private observables = [];

    constructor(
        @Inject("PlatformService") private platform: PlatformContract,
        private bookEvent: BookEvent,
        private appEvent: AppEvent,
        private tools: VaultToolsService,
        private shelfService: ShelfService,
        private route: ActivatedRoute,
        @Inject("DialogService") private dialogService: DialogService
    ) {
        if (this.platform.currentPlatform !== PlatformEnum.IOS) {
            this.scrollableDivId = "";
            this.wrapperDivId = "";
        }

        this.route.data.subscribe((data: { ShelfResolverService: ShelfResolverContract }) => {
            this.bookList           = data.ShelfResolverService.bookList;
            this.bookShelfStatus    = data.ShelfResolverService.bookShelfStatusList;
            this.bookUpgradeable    = data.ShelfResolverService.bookUpgradeableList;
            this.emptyBookImage     = data.ShelfResolverService.emptyBookImage;
        });
    }

    ngOnInit() {
        this.bootstrapComponent();
    }

    errorHandler(event) {
        event.target.src = this.emptyBookImage;
    }

    ngOnDestroy() {
        this.unsubscribeObservables();
        this.isLoadingActive = true;
        this.tools.setBookProgressDownload(this.bookDownloadProgress);
    }

    openBookInfo(book: LegacyBookContract) {
        this.BookInfoComponent.id = "BookInfoComponent";
        this.BookInfoComponent.title = "Informações da Obra";
        this.BookInfoComponent.setData(book);
        this.BookInfoComponent.show();
    }

    private bootstrapComponent() {
        this.initObservables();

        this.showBars();

        this.persistLastShelfInteraction();

        this.tools.initSideNav();

        this.tools.initModals();

        this.tools.initScroll(this.BookListComponent.isLoadingActive);

        this.bookDownloadProgress = this.tools.progress;
    }

    private initObservables() {
        this.observables.push(
            this.bookEvent.EchoBookDeleted.subscribe(bookLedId =>
            {
                delete this.bookShelfStatus[bookLedId];
                let inMemory        = this.bookShelfStatus;
                let updatedObject   = { [bookLedId]: BookShelfStatusEnum.NOT_INSTALLED_OR_NEW };
                Object.assign(updatedObject, inMemory);
                this.bookShelfStatus = updatedObject;
            })
        );

        this.observables.push(this.appEvent.EchoProgress.subscribe((response: any) => {
            delete this.bookShelfStatus[response.book_id];
            let inMemory        = this.bookShelfStatus;
            let updatedObject   = { [response.book_id]: response.book_status };
            Object.assign(updatedObject, inMemory);
            this.bookShelfStatus = updatedObject;
            this.updateBookDownloadProgress(response);
        }));

        /** Shelf Service Observables: Search / Filter / Order */
        this.observables.push(this.shelfService.searching.subscribe(bookList => this.bookList = bookList));

        this.observables.push(this.shelfService.filtering.subscribe(bookList => this.bookList = bookList));

        this.observables.push(this.shelfService.ordering.subscribe(bookList => this.bookList = bookList));
        /** End Shelf Service Observables */

        this.observables.push(this.BookListComponent.bookInfo.subscribe(book => this.openBookInfo(book)));

        this.observables.push(this.BookListComponent.isLoadingActive.subscribe(isActive => this.isLoadingActive = isActive));

        this.observables.push(this.appEvent.EchoDownloadStalled.subscribe(stallReason => {
            let msg = "";
            switch (stallReason) {
                case DownloadStopReason.NO_CONNECTION:
                    msg = "O download não pode prosseguir porque o sistema está sem conexão. " +
                          "Ele retomará automaticamente assim que a conexão for restaurada.";
                break;
                case DownloadStopReason.NO_DISK_SPACE: {
                    msg = "O download não pode prosseguir porque o sistema está sem espaco. Ele retomará automaticamente quando houver novamente espaco disponível";
                }
            }
            this.dialogService.onAlert(msg, "Problema no Download")
        }));
    }

    private unsubscribeObservables() {
        if (this.observables.length) {
            for (let i = 0, count = this.observables.length; i < count; i++) this.observables[i].unsubscribe();
        }
    }

    private showBars() {
        this.appEvent.EchoVisibleComponent.emit({
            toolbar: true,
            navbar: true
        });
    }

    private updateBookDownloadProgress(bookProgress: any) {

        let downloadProgressList = this.bookDownloadProgress;
        this.bookDownloadProgress = {};
        downloadProgressList[bookProgress.book_id] = bookProgress.book_progress * 100.0;
        this.bookDownloadProgress = downloadProgressList;

        if (bookProgress.book_progress == 100) {
            downloadProgressList = this.bookDownloadProgress;
            delete downloadProgressList[bookProgress.book_id];
            this.bookDownloadProgress = {};
            this.bookDownloadProgress = downloadProgressList;
        }
    }

    private persistLastShelfInteraction() {
        if (this.shelfService.term !== '') this.bookList = this.shelfService.getCachedSearch();
        else if (this.shelfService.filter.length > 0) this.bookList = this.shelfService.getCachedFilter();
        else if (this.shelfService.order) this.bookList = this.shelfService.getCachedOrder();
    }
}
