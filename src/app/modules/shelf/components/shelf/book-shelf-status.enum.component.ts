export enum BookShelfStatusEnum {
    UNDEFINED_STATUS = "",
    INICIALIZANDO = "inicializando",
    NOT_INSTALLED_OR_NEW = "novo",
    BAIXANDO = "baixando",
    BAIXADO = "baixado",
    INSTALANDO = "instalando",
    ATUALIZANDO = "atualizando",
    ERROR = "error",
    EXPIRED = "expired",
    OPEN_BOOK = "openBook",
    DOWNLOAD_BOOK = "downloadBook",
    LOADING = 'loading'
}
