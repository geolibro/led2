import { Component, Inject, Input } from '@angular/core';
import { LegacyBookContract } from '../../../../contracts/legacy-book.contract';
import { BookService } from '../../../services/book.service';
import { BookDao } from '../../../daos/book.dao';
import { ModalContract } from '../../../../contracts/modal.contract';
import { DialogContract } from '../../../services/contracts/dialog/dialog.contract';
import { BookShelfStatusContract } from "../../../../contracts/book-shelf-contract";
import { BookShelfStatusEnum } from "../shelf/book-shelf-status.enum.component";

@Component({
    selector: 'app-book-info',
    templateUrl: './book-info.component.html',
    styleUrls: ['./book-info.component.css']
})
export class BookInfoComponent implements ModalContract {
    id: string = 'BookInfoComponent';
    title: string;
    book: LegacyBookContract = <LegacyBookContract>{};
    @Input() bookShelfStatus: BookShelfStatusContract;

    constructor(@Inject('DialogService') private dialog: DialogContract,
        private bookService: BookService,
        private bookDao: BookDao) {
    }

    show() {
        $('#' + this.id).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }

    getBookCover(book: LegacyBookContract) {
        if (!book.hasOwnProperty('download_urls')) {
            return;
        }
        return this.bookService.getBookCoverURL(book);
    }

    setData(book: LegacyBookContract) {
        this.book = book;
    }

    uninstallBook() {
        this.dialog.onConfirm('Você realmente deseja desinstalar esta obra?',
            'Desinstalação/Remoção de obra').then(async (result: boolean) => {
                if (result) {
                    await this.bookDao.uninstall(this.book);
                }
            });
    }

    isBookDownloaded(book: LegacyBookContract) {
        let isDownloaded = false;
        if (this.bookShelfStatus[book.book_led_id]) {
            if (this.bookShelfStatus[book.book_led_id] == BookShelfStatusEnum.BAIXADO) isDownloaded = true;
        }
        return isDownloaded;
    }
}
