import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ShelfUserInfoComponent } from './shelf-user-info.component';


describe('UserInfoComponent', () => {
  let component: ShelfUserInfoComponent;
  let fixture: ComponentFixture<ShelfUserInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfUserInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfUserInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
