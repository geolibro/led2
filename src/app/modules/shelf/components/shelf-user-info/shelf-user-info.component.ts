import { Component, OnInit, Input } from '@angular/core';
import { AppEvent } from '../../../events/app.event';
import { AuthGuardService } from '../../../services/guard/auth-guard.service';
import { UserLoggedFieldsContract } from '../../../../contracts/user-logged-fields.contract';

@Component({
    selector: 'app-shelf-user-info-component',
    templateUrl: './shelf-user-info.component.html',
    styleUrls: ['./shelf-user-info.component.css']
})
export class ShelfUserInfoComponent implements OnInit {
    @Input() userLoggedFields: UserLoggedFieldsContract = <UserLoggedFieldsContract>{};
    aria = true;
    constructor(private appEvent: AppEvent, private auth: AuthGuardService) { }

    ngOnInit() {
        this.userLoggedFields = this.auth.user() ? this.auth.user() : <UserLoggedFieldsContract>{};

        this.appEvent.EchoAriaHidden.subscribe((result) => {
            this.aria = result;
        });
    }
}
