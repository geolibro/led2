import { Component, Input, OnInit } from '@angular/core';
import { BookEvent } from '../../../events/book/book.event';
import { UserSettingsService } from '../../../services/user-settings.service';
import { AppEvent } from '../../../events/app.event';
import { ModalContract } from '../../../../contracts/modal.contract';
import { ContentManagerService } from '../../../services/content-manager/content-manager.service';

@Component({
    selector: 'app-shelf-management-space',
    templateUrl: './shelf-management-space.component.html',
    styleUrls: ['./shelf-management-space.component.css']
})

export class ShelfManagementSpaceComponent implements OnInit, ModalContract {
    id: string;
    title: string = 'Limite de uso de espaço';

    @Input() public isLimitlessChecked: boolean = false;
    @Input() public isLimitChecked: boolean = false;

    @Input() public minSpace: string;
    @Input() public space: string;

    private lastSpace: string;

    private inputSpace: HTMLInputElement;
    private buttonChangeLimitWrap: HTMLElement;
    private inputSpaceWrap: HTMLElement;

    constructor(protected bookEvent: BookEvent,
        protected userSettings: UserSettingsService,
        protected appEvent: AppEvent,
        protected contentManager: ContentManagerService) {
            this.minSpace = '500';
        }

    private setupElements(): void {

        this.inputSpace = <HTMLInputElement>document.getElementById('inputSpace');
        this.buttonChangeLimitWrap = (document.getElementById('buttonChangeLimitWrap') as HTMLElement);
        this.inputSpaceWrap = (document.getElementById('inputSpaceWrap') as HTMLElement);

        this.inputSpace.addEventListener('input', () => {
            this.space = this.inputSpace.value;
            this.loadViewFromCurrentSettings();
        });

        $('input[type=\'radio\']').each((index, element) => {
            $(element).change(() => {
                if ($('#yesLimit').is(':checked')) {
                    this.isLimitChecked = true;
                    this.isLimitlessChecked = false;
                } else {
                    this.isLimitChecked = false;
                    this.isLimitlessChecked = true;
                }
                this.loadViewFromCurrentSettings();
            });
        });
    }

    private loadCurrentSettings() {

        let bytes = this.userSettings.getUserDefinedSpaceLimit();
        if (bytes == Number.MAX_SAFE_INTEGER) {
            this.isLimitChecked = false;
            this.isLimitlessChecked = true;
            this.space = null;
        } else {
            this.isLimitChecked = true;
            this.isLimitlessChecked = false;
            this.space = Math.round((bytes / (1024 * 1024))).toString();
        }

        this.loadViewFromCurrentSettings();
    }

    private loadViewFromCurrentSettings(): void {

        if (this.isLimitChecked) {

            this.inputSpaceWrap.style.display = 'block';

            if (this.space == null) {
                this.space = this.minSpace;
            }

            if (parseInt(this.space) >= parseInt(this.minSpace)) {
                this.buttonChangeLimitWrap.style.display = 'block';
            } else {
                this.buttonChangeLimitWrap.style.display = 'none';
            }

        } else if (this.isLimitlessChecked) {

            this.buttonChangeLimitWrap.style.display = 'block';
            this.inputSpaceWrap.style.display = 'none';
            this.space = null;
        }

    }

    okClick(): void {
        this.lastSpace = this.space;

        if (this.isLimitChecked) {
            const limitInBytes = parseInt(this.space) * 1024 * 1024;
            this.userSettings.setUserDefinedSpaceLimit(limitInBytes);
        } else if (this.isLimitlessChecked) {
            this.userSettings.setUserDefinedSpaceLimit(Number.MAX_SAFE_INTEGER);
        }

        this.contentManager.requestSpace();

        this.hide();
    }

    ngOnInit() {
        this.setupElements();
    }

    show() {
        this.loadCurrentSettings();

        $(`#${this.id}`).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }

}
