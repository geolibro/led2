import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfSearchbarComponent } from './shelf-searchbar.component';

describe('ShelfSearchbarComponent', () => {
  let component: ShelfSearchbarComponent;
  let fixture: ComponentFixture<ShelfSearchbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfSearchbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfSearchbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
