import { ShelfService } from "./../../../services/shelf/shelf.service";
import { Component, Input, OnInit } from "@angular/core";

@Component({
    selector: "app-shelf-searchbar-component",
    templateUrl: "./shelf-searchbar.component.html",
    styleUrls: ["./shelf-searchbar.component.css"]
})
export class ShelfSearchbarComponent implements OnInit {
    @Input() value: string = '';
    constructor(private shelfService: ShelfService) {}

    ngOnInit() {
        this.value = this.shelfService.term;
    }

    search(event: any) {
        this.shelfService.term = event;
    }
}
