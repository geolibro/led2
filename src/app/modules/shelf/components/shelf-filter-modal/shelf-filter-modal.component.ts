import { FormGroup, FormBuilder } from '@angular/forms';
import {
    Component, Input, OnInit,
} from '@angular/core';
import { BookEvent } from "../../../events/book/book.event";
import { AuthGuardService } from "../../../services/guard/auth-guard.service";
import { ModalContract } from "../../../../contracts/modal.contract";
import { ShelfService } from '../../../services/shelf/shelf.service';

@Component({
    selector: "app-shelf-component-filter-modal",
    templateUrl: "./shelf-filter-modal.component.html",
    styleUrls: ["./shelf-filter-modal.component.css"]
})
export class ShelfFilterModalComponent implements ModalContract, OnInit {
    id : string = 'ModalFilter';
    title : string;
    @Input() disciplineList : Array<any> = [];
    formFilter : FormGroup;

    constructor(private bookEvent : BookEvent,
                private auth : AuthGuardService,
                private shelfService : ShelfService,
                private formBuilder : FormBuilder) {
        this.bootstrapComponent();
    }

    ngOnInit() {
    }

    onSubmit(value : any) {
        let requestFiltering = [];

        Object.assign({}, value, {
            disciplines: value.disciplines.map((selected, i) => {
                if (selected) requestFiltering.push(this.disciplineList[i].name);
            })
        });

        this.shelfService.filter = requestFiltering;
    }

    show() {
        $(`#${this.id}`).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }

    get disciplines() {
        return this.formFilter.get('disciplines');
    }

    private bootstrapComponent() {
        this.disciplineList = this.shelfService.getBookFilters();
        this.createForm();
    }

    private createForm() {
        this.buildCheckBoxes();
        this.formFilter = this.formBuilder.group({
            disciplines: this.buildCheckBoxes()
        });
    }

    private buildCheckBoxes() {
        const filterListControl = this.disciplineList.map(disciplineObject => {
            return this.formBuilder.control(disciplineObject.selected);
        });

        return this.formBuilder.array(filterListControl);
    }
}
