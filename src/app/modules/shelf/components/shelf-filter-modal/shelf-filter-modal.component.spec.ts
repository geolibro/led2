import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ShelfFilterModalComponent } from './shelf-filter-modal.component';


describe('ShelfFilterModalComponent', () => {
  let component: ShelfFilterModalComponent;
  let fixture: ComponentFixture<ShelfFilterModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfFilterModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfFilterModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
