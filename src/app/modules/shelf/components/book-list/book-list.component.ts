import { Component, ElementRef, Input, OnInit, ViewChildren, Inject, Renderer2, OnChanges, SimpleChanges, QueryList, Output, EventEmitter, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { LegacyBookContract } from '../../../../contracts/legacy-book.contract';
import { BookDao } from "../../../daos/book.dao";
import { Router } from "@angular/router";
import { BookService } from "../../../services/book.service";
import { NetworkContract } from "../../../services/contracts/network/network.contract";
import { DialogContract } from "../../../services/contracts/dialog/dialog.contract";
import { BookShelfStatusEnum } from "../shelf/book-shelf-status.enum.component";
import { ImageCover } from "../shelf/image-cover.enum.component";
import { BookEnumComponent } from "../shelf/book.enum.component";
import { VaultToolsService } from "../../../services/vault/vault-tools.service";
import { List } from 'immutable';
import { BookShelfStatusContract } from "../../../../contracts/book-shelf-contract";
import { BookDownloadProgressContract } from "../../../../contracts/book-download-progress-contract";
import { ExistsBookShelfContract } from "../../../../contracts/exists-book-shelf-contract";

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: "app-shelf-book-list",
    templateUrl: "./book-list.component.html",
    styleUrls: ["./book-list.component.css"]
})
export class BookListComponent implements OnChanges {
    /**
     * BookList provided from BookDao
     * Pre fetching data in shelf resolver
     */
    @Input() bookList: List<LegacyBookContract>;

    /**
     * Upgradeable bookList from BookDao
     * Pre fetching data in shelf resolver
     */
    @Input() bookUpgradeable: Array<any>;

    @Input() bookDownloadProgress: BookDownloadProgressContract;

    @Input() bookShelfStatus: BookShelfStatusContract;

    @Input() emptyBookImage: string;

    @Output() isLoadingActive: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() bookInfo: EventEmitter<LegacyBookContract> = new EventEmitter<LegacyBookContract>();

    @ViewChild("determinate") bookProgress: ElementRef;

    @ViewChildren("bookCard", { read: ElementRef }) bookCard: QueryList<ElementRef>;
    @ViewChildren("bookCover", { read: ElementRef }) bookCover: QueryList<ElementRef>;
    @ViewChildren("bookLink", { read: ElementRef }) bookLink: QueryList<ElementRef>;

    constructor(
        private bookDao: BookDao,
        @Inject("NetworkService") private network: NetworkContract,
        @Inject("DialogService") private dialog: DialogContract,
        private router: Router,
        private bookService: BookService,
        private renderer: Renderer2,
        private tools: VaultToolsService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        if (changes.hasOwnProperty('bookList')) {

            if (!changes.bookList.currentValue) return;

            /** This operation needs to be refactored */
            this.attachAllBookCardsBehaviors();

            /** Just a random mseconds time for not show loading screen */
            setTimeout(() => this.isLoadingActive.emit(false), 100);

            /** iOS hack for smooth scroll */
            if (this.bookList.size > 0) this.tools.refreshScroll();
        }

        if (changes.hasOwnProperty('bookShelfStatus')) {

            if (!changes.bookShelfStatus.currentValue) return;
            if (changes.bookShelfStatus.firstChange) return;

            this.attachAllBookCardsBehaviors();
        }
    }

    /**
     * @param book LegacyBookContract
     */
    goToBook(book: LegacyBookContract) {
        try {
            switch (this.bookShelfStatus[book.book_led_id]) {
                case BookShelfStatusEnum.BAIXADO:
                case BookShelfStatusEnum.BAIXANDO:
                case BookShelfStatusEnum.OPEN_BOOK:
                    return this.checkForUpdates(book.book_led_id);
                case BookShelfStatusEnum.NOT_INSTALLED_OR_NEW:
                    return this.checkConnectionAndDownload(book);
                default:
                    return;
            }
        } catch (error) {
            console.log(`[InternalError] - ${error}`);
            /** Needs more realistic messages for user with possibilities to fix the error */
            Materialize.toast('Falha ao iniciar download, favor entre em contato com o suporte.', 5000);
        }
    }

    showLoader(book: LegacyBookContract) {
        let isInitializing  = this.existsBookStatusObject({ bookLedId: book.book_led_id, bookStatus: BookShelfStatusEnum.INICIALIZANDO });
        let isUpdating      = this.existsBookStatusObject({ bookLedId: book.book_led_id, bookStatus: BookShelfStatusEnum.ATUALIZANDO });
        return isInitializing || isUpdating;
    }

    errorHandler(event) {
        event.target.src = this.emptyBookImage;
    }

    isAuthorizedBook(bookStatus: number): boolean {
        let isAuthorized = true;
        switch (bookStatus) {
            case BookEnumComponent.INVALID:
            case BookEnumComponent.EXPIRED:
            case BookEnumComponent.UNAVAILABLE:
            case BookEnumComponent.SUSPENDED:
                isAuthorized = false;
                break;
        }
        return isAuthorized;
    }

    getBookUrl(book: LegacyBookContract) {
        return this.bookService.getBookCoverURL(book);
    }

    private existsBookStatusObject(object: ExistsBookShelfContract) {
        let exists = false;

        if (this.bookShelfStatus[object.bookLedId]) {
            if (this.bookShelfStatus[object.bookLedId] == object.bookStatus) exists = true;
        }

        return exists;
    }

    private attachAllBookCardsBehaviors() {
        setTimeout(() => this.bookList.map(book => this.setBookCardBehavior(book)), 300);
    }

    private setBookCardBehavior(book: LegacyBookContract) {
        const bookCover = this.getBookCover(book);

        switch (this.bookShelfStatus[book.book_led_id]) {
            case BookShelfStatusEnum.ATUALIZANDO:
            case BookShelfStatusEnum.INICIALIZANDO:
                this.renderer.addClass(bookCover.nativeElement, ImageCover.DISABLED_IMG);
            break;
            case BookShelfStatusEnum.BAIXADO:
            case BookShelfStatusEnum.BAIXANDO:
            case BookShelfStatusEnum.INSTALANDO:
            case BookShelfStatusEnum.UNDEFINED_STATUS:
            case BookShelfStatusEnum.NOT_INSTALLED_OR_NEW:
                this.renderer.removeClass(bookCover.nativeElement, ImageCover.DISABLED_IMG);
                this.renderer.addClass(bookCover.nativeElement, ImageCover.OPEN_BOOK);
            break;
            case BookShelfStatusEnum.ERROR:
            case BookShelfStatusEnum.EXPIRED:
                this.renderer.removeClass(bookCover.nativeElement, ImageCover.OPEN_BOOK);
                this.renderer.addClass(bookCover.nativeElement, ImageCover.DISABLED_IMG);
            break;
        }
    }

    private async checkForUpdates(bookLedId: string) {
        if (this.bookUpgradeable[bookLedId]) {
            const isUpdated = await this.bookDao.installBookUpdate(bookLedId);
            if (isUpdated) {
                this.bookUpgradeable[bookLedId] = false;
            }
        } else {
            this.router.navigateByUrl("/reader/" + bookLedId);
        }
    }

    private checkConnectionAndDownload(book) {
        if (!this.network.isConnected() || this.network.isLimitedConnection()) {
            this.dialog
                .onConfirm("Parece que você não está conectado à internet ou está em uma internet limitada, deseja prosseguir o download?", "Confirmar download", ["Continuar", "Cancelar"])
                .then(result => {
                    if (result) {
                        this.bookDao.downloadBook(book);
                    }
                });
        } else {
            this.bookDao.downloadBook(book);
        }
    }

    private getBookCover(book: LegacyBookContract) {
        return this.bookCover.filter((cover, index) => {
            return (cover.nativeElement.getAttribute('id').indexOf(book.book_led_id) !== -1);
        })[0];
    }
}
