import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ShelfReorderModalComponent } from './shelf-reorder-modal.component';



describe('ShelfReorderModalComponent', () => {
  let component: ShelfReorderModalComponent;
  let fixture: ComponentFixture<ShelfReorderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfReorderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfReorderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
