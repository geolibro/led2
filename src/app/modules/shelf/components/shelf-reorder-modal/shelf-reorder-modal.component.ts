import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { ModalContract } from '../../../../contracts/modal.contract';
import { SortEnum } from "../../../services/shelf/prototype/sort.enum";
import { ShelfService } from "../../../services/shelf/shelf.service";

@Component({
  selector: 'app-shelf-component-reorder-modal',
  templateUrl: './shelf-reorder-modal.component.html',
  styleUrls: ['./shelf-reorder-modal.component.css']
})
export class ShelfReorderModalComponent implements ModalContract, OnInit {
    id: string = 'ModalOrder';
    title: string;
    private lastSort: number;
    @Input() selectedOrder: number = -1;
    @Input() orderList: Array<{ display: string, value: any, active: boolean }> = [
                                                            { display: 'Título', value: SortEnum.SORT_TITLE, active: false },
                                                            { display: 'Autor', value:  SortEnum.SORT_AUTHOR, active: false },
                                                            { display: 'Disciplina', value: SortEnum.SORT_DISCIPLINE, active: false }
                                                        ];

    constructor(private shelfService: ShelfService) {}

    ngOnInit() {
        this.selectedOrder = this.shelfService.order;
        this.orderList.map(orderItem => {
            if (this.selectedOrder === orderItem.value) orderItem.active = true;
        });
    }

    chooseOrder(order: number) {
        this.orderList.map(orderItem => {
            if (order == orderItem.value) {
                if (orderItem['active']) {
                    this.shelfService.order = SortEnum.SORT_DEFAULT;
                    orderItem['active'] = false;
                    this.lastSort = null;
                } else {
                    this.lastSort = order;
                    this.orderList.map(orderedList => orderedList['active'] = false);
                    this.shelfService.order = order;
                    orderItem['active'] = true;
                }
            }
        });
    }

    show() {
        $(`#${this.id}`).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }
}
