import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfNavbarComponentComponent } from './shelf-navbar.component';

describe('ShelfNavbarComponentComponent', () => {
  let component: ShelfNavbarComponentComponent;
  let fixture: ComponentFixture<ShelfNavbarComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfNavbarComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfNavbarComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
