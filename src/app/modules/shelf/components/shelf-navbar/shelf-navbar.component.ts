import {Component, OnInit, ViewChild} from '@angular/core';
import {AppEvent} from '../../../events/app.event';
import {NavbarComponent} from '../../../global-components/components/navbar/navbar.component';
import {ShelfManagementSpaceComponent} from '../shelf-management-space/shelf-management-space.component';
import {BookEvent} from '../../../events/book/book.event';

@Component({
  selector: 'app-shelf-navbar-component',
  templateUrl: './shelf-navbar.component.html',
  styleUrls: ['./shelf-navbar.component.css']
})
export class ShelfNavbarComponent extends NavbarComponent implements OnInit {

  @ViewChild(ShelfManagementSpaceComponent) ShelfManagementSpace: ShelfManagementSpaceComponent;
  hiddenNavbar: boolean = false;
  aria = false;

  constructor(protected app: AppEvent, private bookEvent: BookEvent) {
    super(app);
  }

  ngOnInit() {

    this.ShelfManagementSpace.id = "ShelfManagementSpace";

    this.app.EchoShowManagementSpace.subscribe(show => {
      if (show) {
        this.ShelfManagementSpace.show();
      }
    });

    this.app.EchoVisibleComponent.subscribe(value => {
      this.hiddenNavbar = !value.navbar;
    });
    this.bookEvent.EchoAriaHiddenModal.subscribe((isHidden) => {
      this.aria = isHidden;
    });

  }

}
