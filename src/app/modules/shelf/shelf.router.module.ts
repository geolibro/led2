import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShelfComponent } from './components/shelf/shelf.component';

const shelfRoutes: Routes = [
    {
        path: '',
        component: ShelfComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(shelfRoutes)],
    exports: [RouterModule]
})
export class ShelfRouterModule { }
