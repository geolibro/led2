import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GlobalComponentsModule } from '../global-components/global-components.module';
import { ActionbarReaderComponent } from './components/actionbar-reader/actionbar-reader.component';
import { AnnotationComponent } from './components/annotation/annotation.component';
import { EPubActionbarReaderComponent } from './components/e-pub-actionbar-reader/e-pub-actionbar-reader.component';
import { EPubBookReaderComponent } from './components/e-pub-book-reader/e-pub-book-reader.component';
import { EPubBookSliderComponent } from './components/e-pub-book-slider/e-pub-book-slider.component';
import { EPubTextformatComponent } from './components/e-pub-textformat/e-pub-textformat.component';
import { EPubTextsearchReaderComponent } from './components/e-pub-textsearch-reader/e-pub-textsearch-reader.component';
import { EPubToolbarReaderComponent } from './components/e-pub-toolbar-reader/e-pub-toolbar-reader.component';
import { LegacyBookReaderComponent } from './components/legacy-book-reader/legacy-book-reader.component';
import { LegacyBookSliderComponent } from './components/legacy-book-slider/legacy-book-slider.component';
import { MainReaderComponent } from './components/main-reader/main-reader.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { PresentationComponent } from './components/presentation/presentation.component';
import { SummaryComponent } from './components/summary/summary.component';
import { TeacherLayerComponent } from './components/teacher-layer/teacher-layer.component';
import { ToolbarReaderComponent } from './components/toolbar-reader/toolbar-reader.component';
import { CurtainScribbleComponent } from './components/curtain-scribble/curtain-scribble.component';
import { ActionbarCanvasComponent } from './components/actionbar-canvas/actionbar-canvas.component';
import { ToolbarCanvasComponent } from './components/toolbar-canvas/toolbar-canvas.component';
import { LegacyHotspotContainerComponent } from './components/legacy-hotspot-container/legacy-hotspot-container.component';
import { PageMarkerComponent } from './components/page-marker/page-marker.component';
import { EPubActionbarDropdownComponent } from './components/e-pub-actionbar-dropdown/e-pub-actionbar-dropdown.component';
import { LegacySearchComponent } from './components/legacy-search/legacy-search.component';
import { ScribbleModalComponent } from './components/scribble-modal/scribble-modal.component';
import { ScribbleComponent } from './components/scribble/scribble.component';
import { AudioPlayerComponent } from '../global-components/components/audio-player/audio-player.component';
import { MediaComponent } from "./components/media/media.component";
import { ReaderRouterModule } from './reader.router.module';
import {CurtainModule} from "../curtain/curtain.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        GlobalComponentsModule,
        ReaderRouterModule,
        CurtainModule
    ],
    exports: [
        MainReaderComponent,
        LegacyBookReaderComponent,
        EPubBookReaderComponent
    ],
    declarations: [
        MainReaderComponent,
        LegacyBookReaderComponent,
        EPubBookReaderComponent,
        ToolbarReaderComponent,
        ActionbarReaderComponent,
        SummaryComponent,
        AnnotationComponent,
        PaginationComponent,
        TeacherLayerComponent,
        PresentationComponent,
        CurtainScribbleComponent,
        LegacyBookSliderComponent,
        EPubActionbarReaderComponent,
        EPubActionbarDropdownComponent,
        EPubToolbarReaderComponent,
        EPubTextformatComponent,
        EPubTextsearchReaderComponent,
        EPubBookSliderComponent,
        ToolbarCanvasComponent,
        ActionbarCanvasComponent,
        ActionbarReaderComponent,
        ScribbleModalComponent,
        ScribbleComponent,
        LegacyHotspotContainerComponent,
        PageMarkerComponent,
        LegacySearchComponent,
        AudioPlayerComponent,
        MediaComponent
    ]
})
export class ReaderModule {}
