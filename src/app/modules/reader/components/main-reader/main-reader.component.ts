import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppEvent } from '../../../events/app.event';
import { BookEvent } from '../../../events/book/book.event';
import { ContentManagerService } from '../../../services/content-manager/content-manager.service';
import { EchoBookReaderContract } from '../../../../contracts/echo-book-reader.contract';
import { PageReaderService } from '../../../services/reader/page-reader.service';
import { EpubReaderService } from '../../../services/reader/epub-reader.service';
import {HotspotLegacyService} from "../../../services/reader/hotspot-legacy.service";
import {LegacyReaderService} from "../../../services/reader/legacy-reader.service";
import { BookDao } from '../../../daos/book.dao';
import { LedFileSystemContract } from '../../../services/contracts/file-system/led-file-system.contract';
import { PlatformContract } from '../../../services/contracts/platform/platform.contract';
import { DeviceContract } from '../../../services/contracts/device/device.contract';

@Component({
  selector: 'app-main-reader',
  templateUrl: './main-reader.component.html',
  styleUrls: ['./main-reader.component.css']
})
export class MainReaderComponent implements OnInit {
  private IntentActivity: any;
  private bookPath: string = 'books/';
  private bookLedId: string;
  private selectedBook: any;

  constructor(
    @Inject('PlatformService') private platform: PlatformContract,
    private appEvent: AppEvent,
    private route: ActivatedRoute,
    private router: Router,
    private bookEvent: BookEvent,
    private contentManagerService: ContentManagerService,
    private PageReader: PageReaderService,
    private ePubBookReaderService: EpubReaderService,
    private legacyBookReaderService: LegacyReaderService,
    private hotspotLegacyManager: HotspotLegacyService,
    @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
    @Inject('DeviceService') private device: DeviceContract,
    private book: BookDao
  ) {

    this.route.params.subscribe(async params => {
      if (Object.keys(params).length > 0) {

        this.contentManagerService.resumeDownload(params['id']);
        await this.book.install(params['id']);

        this.contentManagerService
          .getBook(params['id'])
          .then(book => {

            let reader;
            let routeReader: string;

            if (book.book_format === 'ledEpub3_v1') {
                routeReader  = '/(readers:epub)';
                reader       = this.ePubBookReaderService;
            } else {
                reader       = this.legacyBookReaderService;
                routeReader  = '/(readers:legacy)';
            }

            this.PageReader.load(reader, params['id'], () => {
                const manifest = this.PageReader.reader.getInstalledManifest();
                return this.goToReader(params, routeReader, book, manifest);
            });

          }).catch((error: any) => {
            console.log(error);
            this.router.navigateByUrl('shelf');
          });
      }
    });
  }

  private goToReader(params: any, routeReader: string, book: any, installedBookManifest?: any) {

    this.router
    .navigateByUrl('reader/' + params['id'] + routeReader)
    .then(() => {

        const bookReader: EchoBookReaderContract = <EchoBookReaderContract>{};

        bookReader.book_title = book.book_title;
        bookReader.bookId = params['id'];
        bookReader.bookType = 'ledEpub3_v1';
        if (installedBookManifest) {
            installedBookManifest.book_publisher_id = book.book_publisher_id;
            bookReader.summary = installedBookManifest.chapters;
            bookReader.bookType = 'ledLegacy';
            this.bookEvent.EchoInstalledManifest.emit(installedBookManifest);
        }

        this.bookEvent.EchoBookReader.emit(bookReader);
    });
  }

  ngOnInit() {
    this.appEvent.EchoVisibleComponent.emit({
      toolbar: false,
      navbar: false
    });
  }

}
