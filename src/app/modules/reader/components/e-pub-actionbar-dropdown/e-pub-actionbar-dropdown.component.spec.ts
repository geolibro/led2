import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPubActionbarDropdownComponent } from './e-pub-actionbar-dropdown.component';

describe('EPubActionbarDropdownComponent', () => {
  let component: EPubActionbarDropdownComponent;
  let fixture: ComponentFixture<EPubActionbarDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPubActionbarDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPubActionbarDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
