import {Component, Input, OnInit} from '@angular/core';
import {DropdownComponent} from '../../../global-components/components/dropdown/dropdown.component';

@Component({
  selector: 'app-e-pub-actionbar-dropdown',
  templateUrl: './e-pub-actionbar-dropdown.component.html',
  styleUrls: ['./e-pub-actionbar-dropdown.component.css']
})
export class EPubActionbarDropdownComponent extends DropdownComponent implements OnInit {
  @Input() id: string = 'epubdp';
  @Input() idContent: string = 'epubdp-2';

  constructor() {
    super();
  }

  ngOnInit() {
    $(document).ready(() => {
      $('#' + this.id).dropdown(this.options);
    });
  }

}
