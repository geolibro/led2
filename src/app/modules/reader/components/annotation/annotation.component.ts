import {Component, ViewChild, OnInit} from '@angular/core';
import {MainReaderComponent} from '../main-reader/main-reader.component';
import {environment} from '../../../../../environments/environment';
import {JsonObject} from '../../../../providers/json-object.provider';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {ExternalAnnotationService} from '../../../services/external-annotation.service';
import {BookEvent} from '../../../events/book/book.event';
import { ModalContract } from '../../../../contracts/modal.contract';

@Component({
  selector: 'app-annotation',
  templateUrl: './annotation.component.html',
  styleUrls: ['./annotation.component.css']
})
export class AnnotationComponent implements ModalContract, OnInit {
    id: string;
    title: string;

  private external: ExternalAnnotationService;
  private currentPage: number = 0;

  constructor(private sanitizer: DomSanitizer,
              private bookEvent: BookEvent) {
    this.bookEvent.EchoExternalNotesPlugin.subscribe(external => {
      this.external = external;
    });
  }

  ngOnInit() {
    this.bookEvent.EchoCurrentPage.subscribe(({internal: currentPage}) => {
      this.currentPage = currentPage;
    });
    this.bookEvent.EchoCourtain.subscribe((active) => {
      if (active) {
        this.hide();
      }
    });
    this.bookEvent.EchoEnableScribble.subscribe((active) => {
      if (active) {
        this.hide();
      }
    });
  }

  callExternalApp() {
    this.hide();

    const topic = this.external.getTopicsForPage(this.currentPage);

    if (topic.length > 0) {
      this.external.openAnnotationOnExternalApp(topic[0]);
    } else {
      Materialize.toast('Não há tópicos nesta página.', 500);
    }
  }

    show() {
        $(`#${this.id}`).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }
}
