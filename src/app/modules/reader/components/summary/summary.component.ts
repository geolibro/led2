import { Component, OnInit, DoCheck, Input, OnDestroy } from "@angular/core";
import { BookEvent } from "../../../events/book/book.event";
import { HotspotLegacyService } from "../../../services/reader/hotspot-legacy.service";
import { UserSettingsService } from "../../../services/user-settings.service";
import { ModalContract } from '../../../../contracts/modal.contract';
@Component({
    selector: "app-summary",
    templateUrl: "./summary.component.html",
    styleUrls: ["./summary.component.css"]
})
export class SummaryComponent implements OnInit, OnDestroy, ModalContract {
    @Input() summaryShow: any = [];
    @Input() currentPage: number = 1;
    @Input() currentBookId: string;

    id: string;
    title: string = "Sumário e Listas";

    summary: any = [];
    summaryWay: Array<number> = [];
    bookMarks: Array<number> = [];
    oedList: Array<any>;
    oedGrouped: Array<any> = [];
    oedTypesListed: Array<any> = [];
    hotspotList: Array<{}> = [];

    private eventList: {} = {};

    constructor(protected bookEvent: BookEvent,
                protected hotspotManager: HotspotLegacyService,
                protected userSettings: UserSettingsService) {

        this.eventList['EchoUpdateBookmarkSummary'] = this.bookEvent.EchoUpdateBookmarkSummary.subscribe(val => {
            if (val) this.getBookmarks();
        });

        this.eventList['EchoCurrentPage'] = this.bookEvent.EchoCurrentPage.subscribe(({internal: currentPage}) => {
            this.currentPage = currentPage;
        });

        this.eventList['EchoInstalledManifest'] = this.bookEvent.EchoInstalledManifest.subscribe(installedManifest => {
            if (installedManifest.oeds) {
                this.oedList = installedManifest.oeds;
                this._groupOedsList(installedManifest.oeds);
            }
        });

        this.eventList['EchoBookReader'] = this.bookEvent.EchoBookReader.subscribe(data => {
            if (data.summary) {
                this.summary = data.summary;
                this.summaryShow = data.summary;
            }
            this.currentBookId = data.bookId;
        });

        this.eventList['EchoAnnotationsUpdated'] = this.bookEvent.EchoAnnotationsUpdated.subscribe(isUpdated => {
            if (isUpdated) {
                this.updateHotspotList();
            }
        });
    }

    updateHotspotList() {
        // todo: limpar hotspotlist quando apagar um hootspot
        this.hotspotList = this.hotspotManager.getAllUserHotspots();
    }

    changePageToOED(oed: any) {
        this.bookEvent.EchoLoadOEDFromSummary.emit({page: oed.page_id, oed_id: oed.id});
        this.changePage(oed.page_id);
        setTimeout(() => {
            this.bookEvent.EchoOpenHotspot.emit(oed);
        }, 300);
    }

    changePage(page: number) {
        this.bookEvent.EchoBookPage.emit({pageNumber: page, isRawNumber: true});
        this.hide();
    }

    changeSumario(action: string, idx: number = null) {
        if (action === "add") {
            this.summaryWay.push(idx);
        } else {
            this.summaryWay.pop();
        }

        if (this.summaryWay.length === 0) {
            this.summaryShow = this.summary;
        } else {
            this.summaryShow = this.summary;
            for (let i = 0; i < this.summaryWay.length; i++) {
                this.summaryShow = this.summaryShow.children[this.summaryWay[i]];
            }
        }
    }

    formatSummaryText(text: string) {
        let textFormated = text.replace(/\[(\/)?b\]/gi, "");
        if (text.indexOf("@@") !== -1) {
            textFormated = textFormated.substr(0, textFormated.indexOf("@@"));
        }
        return textFormated;
    }

    getBookmarks() {
        this.bookMarks = [];
        const bookMarks = this.userSettings.getAllBookmarkStatus(this.currentBookId);
        if (bookMarks) {
            bookMarks.map((bookMark, page) => {
                if (bookMark) this.bookMarks.push(page);
            });
        }
    }

    ngOnInit() {
        this.getBookmarks();
        $(document).ready(function () {
            $("ul.tabs").tabs();
            $(".tabs .indicator").css("background-color", "#039be5");
            $(".collapsible").collapsible();
        });
    }

    ngOnDestroy() {
        this.hotspotList = [];
        for (let event in this.eventList) {
            if (this.eventList.hasOwnProperty(event)) this.eventList[event].unsubscribe();
        }
    }

    private _groupOedsList(oedList) {
        const  oedFilteredList = oedList.filter(oed => {
            if (oed.hasOwnProperty('menu_class')) return oed.menu_class.indexOf('Nenhum') === -1
        });

        for (let i = 0, count = oedFilteredList.length; i < count; i++) {
            if (!this.oedGrouped[oedFilteredList[i].menu_class]) {
                this.oedTypesListed.push(oedFilteredList[i].menu_class);
                this.oedGrouped[oedFilteredList[i].menu_class] = [];
            }
            this.oedGrouped[oedFilteredList[i].menu_class].push(oedFilteredList[i]);
        }
    }

    show() {
        $(`#${this.id}`).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }
}
