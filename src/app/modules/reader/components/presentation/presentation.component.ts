import { Component } from '@angular/core';
import { BookEvent } from '../../../events/book/book.event';
import { ModalContract } from '../../../../contracts/modal.contract';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.css']
})
export class PresentationComponent implements ModalContract {

  id: string;
  title: string = 'Apresentação';

  hide() {
      //
  }

  show() {
      //
  }

}
