export enum EPubTextformatFontTypeConst {
    CURSIVE = 'Comic Sans MS, Comic Sans, cursive',
    SERIF = 'Serif',
    MONOSPACE = 'MONOSPACE'
}