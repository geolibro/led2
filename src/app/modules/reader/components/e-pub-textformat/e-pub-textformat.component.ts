import { Component, Input, OnInit, Inject } from '@angular/core';

import { BookEvent } from '../../../events/book/book.event';
import { EPubTextformatThemeTypeEnum } from './e-pub-textformat-themetypes.enum';
import { EPubTextformatFontTypeEnum } from './e-pub-textformat-fonttypes.enum';
import { ModalContract } from '../../../../contracts/modal.contract';
import { OptionItemContract } from '../../../../contracts/option-item.contract';
import { PlatformContract } from "../../../services/contracts/platform/platform.contract";


@Component({
    selector: 'app-e-pub-textformat',
    templateUrl: './e-pub-textformat.component.html',
    styleUrls: ['./e-pub-textformat.component.css']
})

export class EPubTextformatComponent implements ModalContract, OnInit {
    id: string;
    title: string = 'Características do Texto';

    @Input() public fontSize: number = 100;

    @Input() public themes: Array<OptionItemContract> = [
        {label: 'Padrão', value: EPubTextformatThemeTypeEnum.PADRAO},
        {label: 'Alto Contraste', value: EPubTextformatThemeTypeEnum.ALT0_CONTRASTE},
        {label: 'Sépia', value: EPubTextformatThemeTypeEnum.SEPIA}
    ];

    @Input() public fontFamilies: Array<OptionItemContract> = [
        {label: 'Padrão', value: EPubTextformatFontTypeEnum.PADRAO},
        {label: 'Monoespaçada', value: EPubTextformatFontTypeEnum.MONOSPACE},
        {label: 'Serifada', value: EPubTextformatFontTypeEnum.SERIF},
        {label: 'Cursiva', value: EPubTextformatFontTypeEnum.CURSIVE}
    ];

    @Input() public margins: string[] = ['Padrão', 'Pequenas', 'Grandes'];

    @Input() public lineHeightings: string[] = ['Padrão', 'Pequeno', 'Grandes'];

    @Input() public newStyle: {} = {
        containers: {
            nav: {},
            div: {},
            li: {},
            ol: {},
            header: {},
            footer: {},
            section: {},
            main: {},
            aside: {},
            article: {},
            body: {}
        },
        textContainers: {
            p: {},
            h1: {},
            h2: {},
            h3: {},
            h4: {},
            h5: {},
            h6: {},
            label: {},
            strong: {},
            cite: {},
            em: {},
            q: {},
            i: {},
            b: {},
            u: {},
            span: {},
            a: {}
        },
        img: {}
    };

    selectedTheme: OptionItemContract;
    selectedFontFamily: OptionItemContract;

    constructor(private bookEvent: BookEvent,
                @Inject('PlatformService') private platform: PlatformContract) {
    }

    ngOnInit() {
        $('ul span').css('color', 'black');
    }

    public emitEvent(): void {
        const newStyle = this.unboxThemeStyle();
        this.bookEvent.EchoFormatText.emit(newStyle);
    }

    public increment(): void {

        if (this.fontSize < 300) {
            if (document.getElementById('decrement').classList.contains('disabled')) {
                document.getElementById('decrement').classList.remove('disabled');
            }
        }

        ++this.fontSize;
        this.applyThemeStyle('font-size', this.fontSize + '%', 'textContainers');

        this.emitEvent();

        if (this.fontSize === 300) {
            document.getElementById('increment').classList.add('disabled');
        } else if (document.getElementById('increment').classList.contains('disabled')) {
            document.getElementById('increment').classList.remove('disabled');
        }
    }

    public reset(): void {

        this.fontSize = 100;

        this.applyThemeStyle('font-size', this.fontSize + '%', 'textContainers');

        this.emitEvent();

        if (document.getElementById('decrement').classList.contains('disabled')) {
            document.getElementById('decrement').classList.remove('disabled');
        }
        if (document.getElementById('increment').classList.contains('disabled')) {
            document.getElementById('increment').classList.remove('disabled');
        }
    }

    public decrement(): void {

        if (this.fontSize > 80) {
            if (document.getElementById('increment').classList.contains('disabled')) {
                document.getElementById('increment').classList.remove('disabled');
            }

            --this.fontSize;
            this.applyThemeStyle('font-size', this.fontSize + '%', 'textContainers');

            this.emitEvent();

            if (this.fontSize === 80) {
                document.getElementById('decrement').classList.add('disabled');
            } else if (document.getElementById('decrement').classList.contains('disabled')) {
                document.getElementById('increment').classList.remove('disabled');
            }
        }
    }

    selectFont() {
        switch (this.selectedFontFamily.value) {
            case EPubTextformatFontTypeEnum.PADRAO:
                this.applyThemeStyle('font-family', '', 'textContainers');
                break;

            case EPubTextformatFontTypeEnum.MONOSPACE:
                this.applyThemeStyle('font-family', 'MONOSPACE !important', 'textContainers');
                break;

            case EPubTextformatFontTypeEnum.SERIF:
                this.applyThemeStyle('font-family', 'Times New Roman, Georgia, serif !important', 'textContainers');
                break;
            case EPubTextformatFontTypeEnum.CURSIVE:
                this.applyThemeStyle('font-family', 'Comic Sans MS, Comic Sans, cursive !important', 'textContainers');
                break;
        }
    }

    selectTheme() {
        this.newStyle['theme'] = this.selectedTheme.value;
        switch (this.selectedTheme.value) {
            case EPubTextformatThemeTypeEnum.PADRAO:
                this.applyThemeStyle('background', 'white !important', 'containers');
                this.applyThemeStyle('filter', 'none !important', 'containers');
                this.applyThemeStyle('mix-blend-mode', '', 'containers');
                this.applyThemeStyle('color', 'inherit !important', 'textContainers');
                this.newStyle['img']['color'] = 'inherit !important';
                this.newStyle['img']['filter'] = 'none !important';
                break;

            case EPubTextformatThemeTypeEnum.ALT0_CONTRASTE:
                this.applyThemeStyle('background', '#000 !important', 'containers');
                this.applyThemeStyle('filter', '', 'containers');
                this.applyThemeStyle('mix-blend-mode', '', 'containers');
                this.applyThemeStyle('color', 'white !important', 'textContainers');
                this.newStyle['textContainers']['a']['color'] = 'yellow !important';
                this.newStyle['img']['filter'] = 'grayscale(100%) contrast(120%)';

                break;

            case EPubTextformatThemeTypeEnum.SEPIA:
                this.applyThemeStyle('color', '#000 !important', 'textContainers');
                this.applyThemeStyle('background', '#E3B98F !important', 'containers');
                this.newStyle['img']['mix-blend-mode'] = 'multiply';

                break;
        }
    }

    submit(selectedFontFamily: string, selectedTheme: string) {
        this.selectedFontFamily = JSON.parse(selectedFontFamily);
        this.selectedTheme = JSON.parse(selectedTheme);
        this.selectFont();
        this.selectTheme();
        this.emitEvent();
    }

    show() {
        $(`#${this.id}`).modal('open');
        $('select').material_select();
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }

    private unboxThemeStyle() {
        let newStyle = {};
        for (const tag in this.newStyle) {
            if (tag === 'containers' || tag === 'textContainers') {
                for (const tag2 in this.newStyle[tag]) {
                    for (const prop2 in this.newStyle[tag][tag2]) {
                        if (!newStyle[tag2]) newStyle[tag2] = {};
                        newStyle[tag2][prop2] = this.newStyle[tag][tag2][prop2];
                    }
                }
            } else {
                for (const prop in this.newStyle[tag]) {
                    if (this.newStyle[tag].hasOwnProperty(prop)) {
                        if (!newStyle[tag]) newStyle[tag] = {};
                        newStyle[tag][prop] = this.newStyle[tag][prop];
                    }
                }
            }
        }
        newStyle['theme'] = this.selectedTheme || 0;
        return newStyle;
    }

    private applyThemeStyle(property: string, value: string, containerType: string) {
        for (const tag in this.newStyle[containerType]) {
            this.newStyle[containerType][tag][property] = value;
        }
    }
}
