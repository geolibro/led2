export enum EPubTextformatFontTypeEnum {
  PADRAO,
  MONOSPACE,
  SERIF,
  CURSIVE
}
