import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolbarReaderComponent } from './toolbar-reader.component';

describe('ToolbarReaderComponent', () => {
  let component: ToolbarReaderComponent;
  let fixture: ComponentFixture<ToolbarReaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolbarReaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
