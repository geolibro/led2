import {PaginationComponent} from '../pagination/pagination.component';
import {Component, OnInit, ViewChild} from '@angular/core';
import {SummaryComponent} from '../summary/summary.component';
import {AnnotationComponent} from '../annotation/annotation.component';
import {TeacherLayerComponent} from '../teacher-layer/teacher-layer.component';
import {PresentationComponent} from '../presentation/presentation.component';
import {BookEvent} from '../../../events/book/book.event';
import {CurtainScribbleComponent} from "../curtain-scribble/curtain-scribble.component";

@Component({
  selector: 'app-e-pub-toolbar-reader',
  templateUrl: './e-pub-toolbar-reader.component.html',
  styleUrls: ['./e-pub-toolbar-reader.component.css']
})
export class EPubToolbarReaderComponent implements OnInit {
  tapped: boolean = false;
  @ViewChild(CurtainScribbleComponent) CurtainScribble: CurtainScribbleComponent;

  constructor(private bookEvent: BookEvent) {
  }

  ngOnInit() {
    this.CurtainScribble.id = "CurtainScribbleComponent";

    this.bookEvent.EchoHideBar.subscribe((data: boolean) => {
      this.tapped = data;
    })
  }

  openCortina() {
    this.CurtainScribble.show();
  }
}
