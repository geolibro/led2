import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPubToolbarReaderComponent } from './e-pub-toolbar-reader.component';

describe('EPubToolbarReaderComponent', () => {
  let component: EPubToolbarReaderComponent;
  let fixture: ComponentFixture<EPubToolbarReaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPubToolbarReaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPubToolbarReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
