import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import {Router} from "@angular/router";
import {BookEvent} from "../../../events/book/book.event";

@Component({
  selector: 'app-actionbar-canvas',
  templateUrl: './actionbar-canvas.component.html',
  styleUrls: ['./actionbar-canvas.component.css']
})
export class ActionbarCanvasComponent implements OnInit{
  private installedBookManifest: any;
  bookLedId: string;
  @ViewChild('closeCanvas') closeCanvasLink: ElementRef;

  constructor(private router: Router, private bookEvent: BookEvent, private renderer: Renderer2) {}

  ngOnInit() {
      this.renderer.listen(this.closeCanvasLink.nativeElement, 'click', this.closeCanvas.bind(this));
  }
  closeCanvas() {
    this.bookEvent.EchoEnableScribble.emit(false);
    document.getElementById('page-slider').style.position = 'relative'
  }
}
