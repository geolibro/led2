import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegacyBookReaderComponent } from './legacy-book-reader.component';

describe('LegacyBookReaderComponent', () => {
  let component: LegacyBookReaderComponent;
  let fixture: ComponentFixture<LegacyBookReaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegacyBookReaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegacyBookReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
