import { environment } from '../../../../../environments/environment';
import { BookService } from '../../../services/book.service';
import { Component, OnInit, Input, Inject } from '@angular/core';
import { ExternalAnnotationService } from '../../../services/external-annotation.service';
import { CallerProvider } from '../../../../providers/caller.provider';
import { BookEvent } from '../../../events/book/book.event';
import { AuthGuardService } from '../../../services/guard/auth-guard.service';
import { VaultToolsService } from '../../../services/vault/vault-tools.service';
import { PlatformContract } from '../../../services/contracts/platform/platform.contract';
import { LedFileSystemContract } from '../../../services/contracts/file-system/led-file-system.contract';
import { NetworkContract } from '../../../services/contracts/network/network.contract';
declare var Materialize;

@Component({
  selector: 'app-legacy-book-reader',
  templateUrl: './legacy-book-reader.component.html',
  styleUrls: ['./legacy-book-reader.component.css']
})
export class LegacyBookReaderComponent implements OnInit {
  @Input() externalNotesCount: number = 0;
  @Input() isAnnotationCountVisible: boolean = false;
  @Input() isScribble: boolean = false;
  @Input() client: string = environment.client;
  private ExternalNotesPlugin: ExternalAnnotationService;
  private currentPage: number = 1;
  installedBookManifest: any;
  book: any;
  pages: any = [];

  constructor(
    @Inject('PlatformService') private platform: PlatformContract,
    @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
    @Inject('NetworkService') private network: NetworkContract,
    private bookService: BookService,
    private bookEvent: BookEvent,
    private caller: CallerProvider,
    private auth: AuthGuardService,
    private tools: VaultToolsService
  ) {

    this.bookEvent.EchoInstalledManifest.subscribe(installedManifest => {
      this.installedBookManifest = installedManifest;
      this.onExternalAnnotation(installedManifest);
    });

  }

  ngOnInit() {

    const loader: any = $('.fakeloader');

    loader.fakeLoader({
        timeToHide: 1800,
        bgColor: '#f2f2f2',
        spinner: 'spinner2',
        zIndex: 3
      });

    this.tools.initModals();

    this.bookEvent.EchoEnableScribble.subscribe(isScribble => {
      this.isScribble = isScribble;
    });

    this.onExternalDataEvent();

    this.bookEvent.EchoCurrentPage.subscribe(({internal: page}) => {
      this.currentPage = page;
      this.updateAnnotations();
    });
  }

  openExternalAnnotationOnTopic() {
      // tslint:disable-next-line:curly
      if (environment.client !== 'alfacon') return;

      const topic = this.ExternalNotesPlugin.getTopicsForPage(this.currentPage);
      if (topic.length > 0) {
        this.ExternalNotesPlugin.openAnnotationOnExternalApp(topic[0]);
      } else {
        Materialize.toast('Não há tópicos nesta página.', 500);
      }
  }

  getPages(): Array<any> {
    return this.installedBookManifest.pages_manifest_urls;
  }

  getPageMap(): Array<any> {
    return this.installedBookManifest.page_id_number_map;
  }

  private updateAnnotations() {
    // tslint:disable-next-line:curly
    if (environment.client !== 'alfacon') return;

    this.isAnnotationCountVisible = this.ExternalNotesPlugin.pageHasAssociatedTopic(this.currentPage);
    this.externalNotesCount = this.ExternalNotesPlugin.getAnnotationsCountForPage(
      this.currentPage
    );
  }

  private onExternalDataEvent() {
    // tslint:disable-next-line:curly
    if (environment.client !== 'alfacon') return;

    this.bookEvent.EchoExternalData.subscribe((externalData: string) => {
      this.ExternalNotesPlugin.onGetExternalAppSyncData(externalData);
      this.isAnnotationCountVisible = this.ExternalNotesPlugin.pageHasAssociatedTopic(this.currentPage);
      this.externalNotesCount = this.ExternalNotesPlugin.getAnnotationsCountForPage(
        this.currentPage
      );
    });
  }

  private onExternalAnnotation(installedManifest) {
    // tslint:disable-next-line:curly
    if (environment.client !== 'alfacon') return;

    this.ExternalNotesPlugin = new ExternalAnnotationService(
      installedManifest,
      installedManifest.book_publisher_id,
      this.bookService,
      this.caller,
      this.network,
      this.fileSystem,
      this.auth
    );

    this.ExternalNotesPlugin.syncWebService(this.platform);

    this.isAnnotationCountVisible = this.ExternalNotesPlugin.pageHasAssociatedTopic(this.currentPage);

    this.bookEvent.EchoExternalNotesPlugin.emit(this.ExternalNotesPlugin);
  }
}
