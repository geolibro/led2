import { Component, OnInit, Input, OnDestroy, Inject } from "@angular/core";
import { Router } from "@angular/router";
import { BookEvent } from "../../../events/book/book.event";
import { PlatformEnum } from "../../../services/contracts/platform/platform.enum";
import { PlatformContract } from "../../../services/contracts/platform/platform.contract";
import { PageReaderService } from "../../../services/reader/page-reader.service";
import { BookDao } from "../../../daos/book.dao";

@Component({
    selector: 'app-actionbar-reader',
    templateUrl: './actionbar-reader.component.html',
    styleUrls: ['./actionbar-reader.component.css']
})
export class ActionbarReaderComponent implements OnInit, OnDestroy {
    @Input() bookTitle: string = '';
    aria = false;
    searchClicked = false;
    tapped: boolean = false;
    searchField: HTMLElement;
    bookTitleContainer: HTMLElement;
    wordNavigation: HTMLElement;
    startSearch: HTMLElement;
    completeSearch: HTMLElement;
    observers = [];

    constructor(private router: Router, private bookEvent: BookEvent,
                @Inject('PlatformService') private platform: PlatformContract,
                private pageReader: PageReaderService,
                private bookDao: BookDao) {

        const bookId = this.pageReader.reader.getInstalledManifest().book_id;

        this.bookDao.findById(bookId).then(book => {
            this.bookTitle = book.book_title;
        });

        this.observers.push(this.bookEvent.EchoChangeSearch.subscribe((isSearchActive) => {
            this.searchClicked = isSearchActive;
        }));

        this.observers.push(this.bookEvent.EchoHideBar.subscribe((tapped) => {
            if (tapped) {
                document.getElementById('legacyActionBar')['style'].visibility = 'hidden';
                document.getElementById('legacyActionBar')['style'].display = 'none';
            } else {
                document.getElementById('legacyActionBar')['style'].visibility = 'visible';
                document.getElementById('legacyActionBar')['style'].display = 'block';
            }
        }));
    }

    getDOMElements() {
        this.wordNavigation = document.getElementById('word-navigation');
        this.bookTitleContainer = document.getElementById('bookTitle');
        this.startSearch = document.getElementById('pog');
        this.completeSearch = document.getElementById('completeSearch');
        this.searchField = document.getElementById('searchField');
    }

    ngOnInit() {

        this.getDOMElements();

        this.searchField['style'].display = 'none';
        this.completeSearch['style'].display = 'none';

        this.bookEvent.EchoAriaHiddenModal.subscribe((isHidden) => {
            this.aria = isHidden;
        });
        this.bookEvent.EchoKillSearch.subscribe((isKilled) => {
            this.disableSearchFields();
        });

    }

    emptyInput() {
        if (!document.getElementById('inputSearchField')['value']) {
            this.completeSearch['style'].display = 'block';
            this.wordNavigation['style'].display = 'none';
        }
    }

    goBack() {
        if (!this.searchClicked) {
            this.bookEvent.EchoKillVideoHotspot.emit(true);
            this.router.navigateByUrl('/shelf');
        } else {
            this.disableSearchFields();
            this.bookEvent.EchoKillSearch.emit(true);
        }
    }

    disableSearchFields() {
        this.searchField['style'].display = 'none';
        this.bookTitleContainer['style'].display = 'block';
        this.startSearch['style'].display = 'block';
        this.completeSearch['style'].display = 'none';
        this.wordNavigation['style'].display = 'none';
        document.getElementById('inputSearchField')['value'] = "";
        this.searchClicked = false;
    }

    ngOnDestroy() {
        for (let evt of this.observers) {
            evt.unsubscribe();
        }

    }

}
