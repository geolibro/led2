import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegacyHotspotContainerComponent } from './legacy-hotspot-container.component';

describe('LegacyHotspotContainerComponent', () => {
  let component: LegacyHotspotContainerComponent;
  let fixture: ComponentFixture<LegacyHotspotContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegacyHotspotContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegacyHotspotContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
