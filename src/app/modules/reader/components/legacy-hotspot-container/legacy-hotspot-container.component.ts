import { Component, Input, OnDestroy, OnInit, ViewChild, Inject } from '@angular/core';
import { HotspotLegacyService } from '../../../services/reader/hotspot-legacy.service';
import { BookEvent } from '../../../events/book/book.event';
import { HotspotTypeEnum } from '../../../services/reader/hotspot-type.enum';
import { HotspotContract } from '../../../services/reader/contracts/hotspot.contract';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { AuthGuardService } from '../../../services/guard/auth-guard.service';
import { PromptComponent } from "../../../global-components/components/prompt/prompt.component";
import { DialogContract } from '../../../services/contracts/dialog/dialog.contract';
import { PlatformContract } from '../../../services/contracts/platform/platform.contract';
import { LedFileSystemContract } from '../../../services/contracts/file-system/led-file-system.contract';
import { PlatformEnum } from '../../../services/contracts/platform/platform.enum';

declare var window;
declare var OEDWebView;

@Component({
    selector: 'app-legacy-hotspot-container',
    templateUrl: './legacy-hotspot-container.component.html',
    styleUrls: ['./legacy-hotspot-container.component.css']
})
export class LegacyHotspotContainerComponent implements OnInit, OnDestroy {

    @Input() audioWithControlsSrc: SafeResourceUrl | string = '';
    @ViewChild(PromptComponent) promptComponent: PromptComponent;
    private userRect: HTMLElement;
    private hotspotArea: HTMLElement;
    private touchStartListener;
    private touchMoveListener;
    private touchEndListener;
    private captureEndListener;
    private isCapturing: boolean;
    private audioPlayer: HTMLAudioElement;
    private isDragging: boolean;
    private tapped: boolean;
    private hotspotContext = 'annotation';
    private teacherLayerEnabled = false;
    private HammerJS: HammerManager;
    private observers = {};

    constructor(private hotspotService: HotspotLegacyService,
        @Inject('DialogService') private dialog: DialogContract,
        private bookEvent: BookEvent,
        @Inject('PlatformService') private platform: PlatformContract,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        private domSanitizer: DomSanitizer,
        private auth: AuthGuardService) {
    }

    public toggleHotspotCapture(onCaptureEnd: Function) {
        this.captureEndListener = onCaptureEnd;

        if (this.isCapturing) {
            this.finishHotspotCapture(false);
        } else {
            this.initHotspotCapture();
        }
    }

    ngOnInit() {
        this.observers['killSearch'] = this.bookEvent.EchoKillSearch.subscribe((isKilled) => {
            if (isKilled) this.clearHotspots('word-hotspot');
        });
        this.observers['killAudio'] = this.bookEvent.EchoKillAudioWithoutControl.subscribe((killAudio) => {
            this.audioPlayer.pause();
        });
        this.observers['showWord'] = this.bookEvent.EchoShowWordHotspot.subscribe((elem) => {
            this.loadSearchHotspots(elem[0], elem[1]);
        });
        this.observers['openHotspot'] = this.bookEvent.EchoOpenHotspot.subscribe((hotspot) => {
            const currentPageBookHotspots = this.hotspotService.getOEDHotposts();
            if (currentPageBookHotspots) {
                const findAudioHotspot = currentPageBookHotspots.filter((hotspot) => {
                    return hotspot.type === HotspotTypeEnum.AUDIO_CONTROL || hotspot.type === HotspotTypeEnum.AUDIO_AUTOPLAY;
                });
                currentPageBookHotspots.map((pageHotspot) => {
                    if (findAudioHotspot.length) {
                        this.bookEvent.EchoKillAudio.emit(true);
                        this.bookEvent.EchoKillAudioWithoutControl.emit(true);
                    }
                    if (pageHotspot.id === hotspot.id) {
                        if (pageHotspot.type === HotspotTypeEnum.AUDIO_AUTOPLAY || pageHotspot.type === HotspotTypeEnum.AUDIO_CONTROL) {
                            this.bookEvent.EchoKillAudio.emit(true);
                            this.bookEvent.EchoKillAudioWithoutControl.emit(true);
                        }
                        this.openHotspot(pageHotspot);
                    }
                });
            }
        });

        this.hotspotArea = document.getElementById('canvasHotspot');
        this.audioPlayer = new Audio();
        this.userRect = document.getElementById('userRect');
        this.isCapturing = false;
        this.bookEvent.EchoCourtain.subscribe((active) => {
            if (active) {
                this._disableAnnotation();
            }
        });
        this.observers['enableScribble'] = this.bookEvent.EchoEnableScribble.subscribe((active) => {
            if (active) {
                this._disableAnnotation();
            }
        });
        this.observers['loadOED'] = this.bookEvent.EchoLoadOEDFromSummary.subscribe((OEDFromSummary) => {
            this.hotspotService.setCurrentPageId(OEDFromSummary.page);
            const currentPageBookHotspots = this.hotspotService.getOEDHotposts();
            if (currentPageBookHotspots) {
                currentPageBookHotspots.map((pageHotspot) => {
                    if (pageHotspot.id === OEDFromSummary.oed_id) {
                        this.openHotspot(pageHotspot);
                    }
                });
            }
        });
        this.observers['EchoToggleTeacherLayer'] = this.bookEvent.EchoToggleTeacherLayer.subscribe((isEnabled) => {
            this.teacherLayerEnabled = isEnabled;
        });

        this.initEventHandlers();
    }

    public loadSearchHotspots(word: string, page: number | string): void {

        this.clearHotspots('word-hotspot');
        this.hotspotContext = 'word';
        let searchHotspots = this.hotspotService.getHotspotsForWord(word);
        if (searchHotspots.length) {
            searchHotspots.map((hotspot, index) => {
                if (hotspot[page]) {
                    let hotspotToAdd = hotspot[page];
                    hotspotToAdd = hotspotToAdd.sort((a, b) => {
                        return a.id - b.id;
                    });
                    hotspotToAdd = hotspotToAdd.filter((word) => {
                        return this.teacherLayerEnabled ? word.type == 1 || word.type == 0 : word.type == 0;
                    });
                    for (let i = 0, count = hotspotToAdd.length; i < count; i++) {
                        const currentWordHotspot = hotspotToAdd[i];
                        const wordHotspot = {
                            id: currentWordHotspot.id, x: currentWordHotspot.x, y: currentWordHotspot.y,
                            width: currentWordHotspot.w, height: currentWordHotspot.h,
                            type: currentWordHotspot.type, description: 'WordHotspot'
                        };
                        this.addHotspotElement(this.hotspotService.decryptHotspot(wordHotspot));
                    }
                }

            });
        }
    }

    public loadPageHotspots() {

        this.clearHotspots();

        let userHotspots = this.hotspotService.getUserHotspots();
        let oedHotspots = this.hotspotService.getOEDHotposts();

        let pageHotspots = oedHotspots.concat(userHotspots);

        if (pageHotspots != null) {
            for (let hotspotId in pageHotspots) {
                if (pageHotspots[hotspotId]) this.addHotspotElement(pageHotspots[hotspotId]);
            }
        }
    }

    ngOnDestroy() {
        this.bookEvent.EchoKillAudio.emit(false);
        this.bookEvent.EchoKillAudioWithoutControl.emit(true);

        if (Object.keys(this.observers).length) {
            for (let evt of Object.keys(this.observers)) {
                this.observers[evt].unsubscribe();
            }
        }
    }

    private initEventHandlers() {

        let rect: {
            x: number,
            y: number,
            width: number,
            height: number
        };

        this.touchStartListener = (e) => {
            e.stopPropagation();

            if (this.platform.is(PlatformEnum.MOBILE)) {
                rect = { x: e.touches[0].pageX, y: e.touches[0].pageY, width: 0, height: 0 };
            } else if (this.platform.is(PlatformEnum.DESKTOP)) {
                rect = { x: e.pageX, y: e.pageY, width: 0, height: 0 };
            }

            this.isDragging = true;
            this.applyCoordinatesToUserHotspot(rect);
            e.preventDefault();
        };

        this.touchMoveListener = (e) => {
            if (this.isDragging) {

                if (this.platform.is(PlatformEnum.MOBILE)) {
                    if (rect) {
                        rect.width = (e.touches[0].pageX - rect.x);
                        rect.height = (e.touches[0].pageY - rect.y);
                    } else {
                        rect.width = (e.touches[0].pageX);
                        rect.height = (e.touches[0].pageY);
                    }
                } else if (this.platform.is(PlatformEnum.DESKTOP)) {
                    if (rect) {
                        rect.width = (e.pageX - rect.x);
                        rect.height = (e.pageY - rect.y);
                    } else {
                        rect.width = (e.pageX);
                        rect.height = (e.pageY);
                    }
                }

                this.applyCoordinatesToUserHotspot(rect);
                e.preventDefault();
            }
        };

        this.touchEndListener = (e) => {
            if (this.isDragging) {

                if (this.platform.is(PlatformEnum.MOBILE)) {
                    if (rect) {
                        rect.width = (e.touches.pageX - rect.x);
                        rect.height = (e.touches.pageY - rect.y);
                    } else {
                        rect.width = (e.touches.pageX);
                        rect.height = (e.touches.pageY);
                    }
                } else if (this.platform.is(PlatformEnum.DESKTOP)) {
                    if (rect) {
                        rect.width = (e.pageX - rect.x);
                        rect.height = (e.pageY - rect.y);
                    } else {
                        rect.width = (e.pageX);
                        rect.height = (e.pageY);
                    }
                }

                this.applyCoordinatesToUserHotspot(rect);
                e.preventDefault();
                this.finishHotspotCapture(true);
                this.isDragging = false;
            }
        };
    }

    private applyCoordinatesToUserHotspot(rect: { x: number, y: number, width: number, height: number }) {
        let left = rect.x, top = rect.y, width = rect.width, height = rect.height;

        if (width < 0) {
            left += width;
            width = Math.abs(width);
        }

        if (height < 0) {
            top += height;
            height = Math.abs(height);
        }

        this.userRect.style.top = top + 'px';
        this.userRect.style.left = left + 'px';
        this.userRect.style.width = width + 'px';
        this.userRect.style.height = height + 'px';
    }

    private enableHotspotCaptureListeners() {
        // Set up touch events for mobile, etc

        ['touchstart', 'mousedown'].forEach(event => this.hotspotArea.addEventListener(event, this.touchStartListener));

        ['touchmove', 'mousemove'].forEach(event => this.hotspotArea.addEventListener(event, this.touchMoveListener));

        ['touchend', 'mouseup'].forEach(event => this.hotspotArea.addEventListener(event, this.touchEndListener));

    }

    private disableHotspotCaptureListeners() {
        ['touchstart', 'mousedown'].forEach(event => this.hotspotArea.removeEventListener(event, this.touchStartListener));

        ['touchmove', 'mousemove'].forEach(event => this.hotspotArea.removeEventListener(event, this.touchMoveListener));

        ['touchend', 'mouseup'].forEach(event => this.hotspotArea.removeEventListener(event, this.touchEndListener));

    }

    private initHotspotCapture() {
        this.isCapturing = true;
        this.hotspotArea.style.backgroundColor = 'rgba(0,0,0,0.5)';
        this.enableHotspotCaptureListeners();
    }

    private finishHotspotCapture(shouldSave: boolean) {

        this.disableHotspotCaptureListeners();
        this.hotspotArea.style.backgroundColor = 'rgba(0,0,0,0)';
        this.isCapturing = false;
        if (typeof (this.captureEndListener) === 'function') this.captureEndListener();

        if (shouldSave) {
            this.promptComponent.title = 'Digite um texto ou hyperlink para esta marcação (opcional)';
            this.promptComponent.onPrompt().then(promptResult => {

                const resultText = promptResult;

                let hotspotType;

                if (resultText.ok) {
                    if (resultText.input.indexOf('http://') === 0 || resultText.input.indexOf('https://') === 0) {
                        hotspotType = HotspotTypeEnum.USER_WEB_LINK;
                    } else if (resultText.input.trim().length === 0) {
                        hotspotType = HotspotTypeEnum.TEXT_HIGHLIGHT;
                    } else {
                        hotspotType = HotspotTypeEnum.TEXT_HIGHLIGHT_WITH_NOTE;
                    }

                    if (promptResult.ok) {
                        let hotspot: HotspotContract = {
                            x: parseInt(this.userRect.style.left) / (this.hotspotArea.clientWidth),
                            y: parseInt(this.userRect.style.top) / (this.hotspotArea.clientHeight),
                            width: (this.userRect.clientWidth) / (this.hotspotArea.clientWidth),
                            height: (this.userRect.clientHeight) / (this.hotspotArea.clientHeight),
                            description: resultText.input, type: hotspotType
                        };

                        this.hotspotService.saveUserHotspot(hotspot).then(() => {
                            this.loadPageHotspots();
                            this.bookEvent.EchoAnnotationsUpdated.emit(true);
                        }).catch((error) => {
                            this.loadPageHotspots();
                            console.log('error saving hotspots: ' + error);
                            this.bookEvent.EchoAnnotationsUpdated.emit(false);
                        });
                    }
                } else {
                    this._disableAnnotation();
                }

                this.userRect.style.top = 'initial';
                this.userRect.style.left = 'initial';
                this.userRect.style.width = 'initial';
                this.userRect.style.height = 'initial';
            });
        }
    }

    private clearHotspots(divClass?: string) {
        let hotspotList = Array.from(divClass ? this.hotspotArea.getElementsByClassName(divClass) : this.hotspotArea.getElementsByTagName('div'));

        for (let item of hotspotList) {
            if (item.getAttribute('id') != 'userRect' && (!divClass ? !item.classList.contains('word-hotspot') : true)) {
                item.remove();
            }
        }

    }

    private addHotspotElement(hotspot: HotspotContract) {
        let hotspotDiv = document.createElement('div');

        hotspotDiv.setAttribute('hotspotId', hotspot.id.toString());

        hotspotDiv.style.top = (hotspot.y * 100) + '%';
        hotspotDiv.style.left = (hotspot.x * 100) + '%';
        hotspotDiv.style.width = (hotspot.width * 100) + '%';
        hotspotDiv.style.height = (hotspot.height * 100) + '%';

        hotspotDiv.style.position = 'absolute';

        if (hotspot.type == HotspotTypeEnum.TEXT_HIGHLIGHT ||
            hotspot.type == HotspotTypeEnum.TEXT_HIGHLIGHT_WITH_NOTE ||
            hotspot.type == HotspotTypeEnum.USER_WEB_LINK) {

            hotspotDiv.style.backgroundColor = 'yellow';
            hotspotDiv.style.opacity = '0.4';

            hotspotDiv.setAttribute('class', 'user-hotspot');

        }
        else if (hotspot.type == HotspotTypeEnum.WORD ||
            hotspot.type == HotspotTypeEnum.WORD_TEACHER) {
            hotspotDiv.style.backgroundColor = 'green';
            hotspotDiv.style.opacity = '0.4';
            hotspotDiv.setAttribute('class', 'word-hotspot');
            let existingWordHotspots = this.hotspotArea.getElementsByClassName('word-hotspot');
            let firstWordHotspot = null;
            let i = 0;
            if (existingWordHotspots.length) {
                firstWordHotspot = existingWordHotspots[i++];
            }
            if (firstWordHotspot && hotspot.id < parseInt(firstWordHotspot.getAttribute('hotspotId'))) {
                firstWordHotspot.classList.remove('active');
                hotspotDiv.classList.add('active');
                hotspotDiv.style.opacity = '0.6';
            } else if (!firstWordHotspot) {
                hotspotDiv.classList.add('active');
                hotspotDiv.style.opacity = '0.6';
            }
        } else {
            hotspotDiv.style.cursor = 'pointer';
            hotspotDiv.setAttribute('class', 'book-hotspot');

        }

        this.HammerJS = new Hammer(hotspotDiv, {
            domEvents: true
        });

        // TODO: setar propriedades, ex.: listener de cada hostpot
        this.HammerJS.on('tap', (hammerEvent) => {
            hammerEvent.preventDefault();
            $('.hotspot').hide();
            this.openHotspot(hotspot);
        });

        if (hotspotDiv.getAttribute('class') == 'user-hotspot') {
            this.HammerJS.on('press', (hammerEvent) => {
                this.dialog
                    .onConfirm('Deseja apagar esta marcação?',
                        'Confirmação')
                    .then(choice => {
                        if (choice) {
                            let hotspotId = parseInt(hotspotDiv.getAttribute('hotspotId'));
                            this.deleteHotspot(hotspotId);
                        }
                    }).catch(error => {
                        console.log(error);
                    });
            });
        }

        ['touchmove', 'mousemove'].forEach(event => {

            hotspotDiv.addEventListener(event, (e) => {
                e.stopPropagation();
                e.preventDefault();
            });

        });

        ['touchend', 'mouseup'].forEach(event => {

            hotspotDiv.addEventListener(event, (e) => {
                e.stopPropagation();
                e.preventDefault();
            });

        });

        ['touchcancel', 'mousecancel'].forEach(event => {

            hotspotDiv.addEventListener(event, (e) => {
                e.stopPropagation();
                e.preventDefault();
            });

        });

        this.hotspotArea.appendChild(hotspotDiv);
    }

    private errorHandler(message: string): void {
        let linkTagError = $('<a href="javascript:void(0);" style="color: white" id="toast-login">' + message + '</a>');
        Materialize.toast(linkTagError, 5000, 'toast-login');
        document.getElementById('toast-container').style.top = '0';
        document.getElementById('toast-container').style.bottom = '100%';
        if (this.platform.is(PlatformEnum.ANDROID)) $('.toast-login').attr('aria-live', 'assertive');
        if (this.platform.is(PlatformEnum.IOS)) $('#toast-container #toast-login').focus();
    }

    private openHotspot(hotspot: HotspotContract) {

        // finalizando execução de audio
        this.bookEvent.EchoKillAudio.emit(false);
        this.bookEvent.EchoKillAudioWithoutControl.emit(true);

        /* HotspotTypeEnum não mapeados
         *
         * "HTML_TEXT"
         * "HTML5_EMBEDDED"
         *
         */

        let itemUrl: string;
        switch (hotspot.type) {

            case HotspotTypeEnum.TEXT_HIGHLIGHT_WITH_NOTE:

                if (hotspot.description !== '' && hotspot.description !== null) this.dialog.onAlert(hotspot.description, 'Anotações');
                else this.errorHandler('Houve um problema ao carregar o objeto');

                break;

            case HotspotTypeEnum.USER_WEB_LINK:
            case HotspotTypeEnum.WEB_LINK:

                if (hotspot.description !== '' && hotspot.description !== null) {
                    this.fileSystem.openLinkExternally(hotspot.description);
                }
                else this.errorHandler('Houve um problema ao carregar o objeto');

                break;

            case HotspotTypeEnum.VIDEO_FULLSCREEN:

                itemUrl = hotspot.book_id + hotspot.resource.url;
                this.bookEvent.EchoOpenVideoHotspot.emit(itemUrl);
                // this.fileSystem.openFileExternally(itemUrl, 'video/mp4').then(() => {
                //     console.log('executando video fullscreen');
                // }).catch((error) => {
                //     console.log('erro abrindo hotspot do tipo ' + hotspot.type);
                //     this.errorHandler('Houve um problema ao carregar o objeto');
                // });


                break;
            case HotspotTypeEnum.IMAGE_ZOOM:
                itemUrl = this.fileSystem.getDirectory() + hotspot.book_id + hotspot.resource.url;

                this.fileSystem.openFileExternally(itemUrl, 'image/png').then(() => {
                    console.log('abrindo imagem com zoom');
                }).catch((error) => {
                    console.log('erro abrindo hotspot do tipo ' + hotspot.type);
                    this.errorHandler('Houve um problema ao carregar o objeto');
                });
                break;
            case HotspotTypeEnum.PDF:

                itemUrl = this.fileSystem.getDirectory() + hotspot.book_id + hotspot.resource.url;

                this.fileSystem.openFileExternally(itemUrl, 'application/pdf').then(() => {
                    console.log('executando pdf');
                }).catch(() => {
                    console.log('erro abrindo hotspot do tipo ' + hotspot.type);
                    this.errorHandler('Houve um problema ao carregar o objeto');
                });

                break;

            case HotspotTypeEnum.AUDIO_CONTROL:

                itemUrl = hotspot.book_id + hotspot.resource.url;

                this.fileSystem.request(itemUrl).readFile().get().then((fileBytes) => {
                    const file: Blob = new Blob([fileBytes], { type: 'audio/mp3' });

                    // fixme: é correto eu validar se o arquivo têm size 0 ?
                    if (file.size == 0) {
                        this.errorHandler('Houve um problema ao carregar o objeto');
                    } else {
                        let audioURL = window.URL.createObjectURL(file);
                        this.bookEvent.EchoAudioPlayer.emit(audioURL);
                    }
                }).catch((error) => {
                    console.log(error);
                    this.errorHandler('Houve um problema ao carregar o objeto');
                });

                break;

            case HotspotTypeEnum.AUDIO_AUTOPLAY:

                itemUrl = hotspot.book_id + hotspot.resource.url;
                this.bookEvent.EchoKillAudioWithoutControl.emit(true);
                this.bookEvent.EchoKillAudio.emit(false);
                this.fileSystem.request(itemUrl).readFile().get().then((fileBytes) => {
                    const file: Blob = new Blob([fileBytes], { type: 'audio/mp3' });


                    let audioURL = window.URL.createObjectURL(file);
                    this.audioPlayer.src = audioURL;
                    this.bookEvent.EchoKillAudio.emit(true);
                    this.audioPlayer.load();
                    this.audioPlayer.play().then(() => {
                        console.log('executando audio sem controle (' + file + ')');
                    }).catch((error) => {
                        let errorStr = 'Erro executando audio sem controle para execução! (' + JSON.stringify(error) + ')';
                        console.log(errorStr);
                        this.errorHandler('Houve um problema ao carregar o objeto');
                    });
                }).catch((error) => {
                    let errorStr = 'Erro carregando audio sem controle para execução! (' + JSON.stringify(error) + ')';
                    console.log(errorStr);
                    this.errorHandler('Houve um problema ao carregar o objeto');
                });

                break;

            case HotspotTypeEnum.HTML5_FULLSCREEN:

                if (this.platform.is(PlatformEnum.DESKTOP)) {

                    let oedURL = 'file://' + this.fileSystem.getDirectory() + hotspot.book_id + hotspot.resource.url + '/index.html';
                    let hotspotJSON = JSON.stringify(hotspot);
                    let userId = this.auth.user().led_user_id;
                    let oedParams = JSON.parse(hotspot.resource.parameters);
                    let oedWidth = oedParams.largura ? oedParams.largura : 1024;
                    let oedHeight = oedParams.altura ? oedParams.altura : 768;
                    let child = window.require('child_process').execFile;

                    let electronExe: string = window.require('electron').remote.app.getPath('exe');
                    let electronFolderPath = electronExe.substring(0, electronExe.lastIndexOf('/'));

                    let javaExePath = electronFolderPath + './win32resources/win32jre/bin/java.exe';
                    let parameters = ['-jar', electronFolderPath + './win32resources/sandbox.jar', oedURL, '--userId=' + userId,
                        '--width=' + oedWidth, '--height=' + oedHeight, '--hotspot=' + JSON.stringify(hotspot)];

                    console.log('electronExe: ' + electronExe);
                    console.log('electronFolderPath' + electronFolderPath);

                    console.log('launching: [' + javaExePath + ' ' + parameters + ']');
                    child(javaExePath, parameters, (err, data) => {
                        this.bookEvent.EchoKillAudio.emit(true);
                        this.bookEvent.EchoKillAudioWithoutControl.emit(true);

                        if (data !== '') {
                            console.log(data.toString());
                        } else if (err.code === 'ENOENT') {
                            console.log(err);
                            this.errorHandler('Houve um problema ao carregar o objeto');
                        }
                    });
                }
                else {
                    itemUrl = hotspot.book_id + hotspot.resource.url;

                    const options = hotspot.resource.parameters;
                    const id = this.auth.user().led_user_id;

                    this.fileSystem.exists(itemUrl, exists => {

                        if (exists) {
                            this.bookEvent.EchoKillAudio.emit(true);
                            this.bookEvent.EchoKillAudioWithoutControl.emit(true);

                            // if (this.platform.is(PlatformEnum.IOS)) {

                            // }

                            itemUrl = this.fileSystem.getDirectory() + itemUrl;
                            itemUrl = itemUrl.replace('file://', '');

                            OEDWebView.show(itemUrl, options, id, function () {
                                console.log("OED mostrado com sucesso!");
                            }, function (e) {
                                console.log("Falha ao tentar executar o modal!");
                                this.errorHandler('Houve um problema ao carregar o objeto');
                            });
                        }
                        else {
                            this.errorHandler('Houve um problema ao carregar o objeto');
                        }

                    });
                }

                break;

            case HotspotTypeEnum.INTERNAL_LINK:
                let pageId: number;
                if (hotspot) pageId = this.getPageIdFromHotspot(hotspot);
                else this.errorHandler('Houve um problema ao carregar o objeto');

                if (pageId) this.bookEvent.EchoBookPage.emit({ pageNumber: pageId, isRawNumber: true });
                else this.errorHandler('Houve um problema ao carregar o objeto');

                break;
            default:
                console.log('HotspotTypeEnum not found');

        }

        $('.hotspot').show();
    }

    private getPageIdFromHotspot(hotspot: HotspotContract): number {
        return parseInt(hotspot.description.match(/\d+/)[0]);
    }

    private deleteHotspot(hotspotId: number) {
        this.hotspotService.deleteUserHotspot(hotspotId).then(() => {
            this.bookEvent.EchoAnnotationsUpdated.emit(true);
            this.loadPageHotspots();
        }).catch((error) => {
            this.bookEvent.EchoAnnotationsUpdated.emit(false);
            console.log('Erro ao excluir hotspot: ' + JSON.stringify(error));
        });
    }

    private _disableAnnotation() {
        this.disableHotspotCaptureListeners();
        this.hotspotArea.style.backgroundColor = 'rgba(0,0,0,0)';
        this.isCapturing = false;
        if (typeof (this.captureEndListener) === 'function') this.captureEndListener();

    }

}
