import { Component, ElementRef, OnDestroy, OnInit, ViewChild, Inject } from '@angular/core';
import { BookEvent } from "../../../events/book/book.event";
import { MediaService } from "../../../services/media/media.service";
import { ModalContract } from "../../../../contracts/modal.contract";
import { LedFileSystemContract } from '../../../services/contracts/file-system/led-file-system.contract';

@Component({
    selector: 'app-media',
    templateUrl: './media.component.html',
    styleUrls: ['./media.component.css']
})
export class MediaComponent implements ModalContract, OnInit, OnDestroy {
    id: string = 'VideoHotspotModal';
    title: string;

    @ViewChild('mContainer') mediaContainer: ElementRef;
    @ViewChild('vContainer') videoContainer: ElementRef;
    @ViewChild('subContainer') subtitleContainer: ElementRef;
    private observables = [];
    private _path: string;

    private options = {};

    constructor(private bookEvent: BookEvent,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        private mediaService: MediaService) { }

    ngOnInit() {
        this.subtitleContainer.nativeElement.addEventListener('load', function () {
            this.mode = 'showing';
        });
        this.observables.push(this.bookEvent.EchoOpenVideoHotspot.subscribe((videoPath) => {
            this.videoContainer.nativeElement.src = '';
            this.subtitleContainer.nativeElement.src = '';
            this._path = videoPath.substring(0, videoPath.lastIndexOf('.'));
            this.fileSystem.exists(this._path + '.srt', (exists) => {
                if (exists) {
                    this.fileSystem.request(this._path + '.srt').readFile().toString((subtitleContent) => {
                        const contentConverted = this.mediaService.srt2vtt(subtitleContent.toString('utf-8'));
                        const blobContentConverted = new Blob([contentConverted], { type: 'text/vtt' });
                        const blobUrl = URL.createObjectURL(blobContentConverted);
                        this.videoContainer.nativeElement.src = this.fileSystem.getDirectory() + this._path + '.mp4';
                        this.videoContainer.nativeElement.addEventListener('loadedmetadata', () => {
                            this.subtitleContainer.nativeElement.src = blobUrl;
                        });
                    }, (error) => {
                        console.log(error);
                    });
                } else {
                    this.videoContainer.nativeElement.src = this.fileSystem.getDirectory() + this._path + '.mp4';
                    document.getElementsByClassName('modal-overlay')[0]['style'].zIndex = 800;
                }
            });
            this.show();
        }));

        this.observables.push(this.bookEvent.EchoKillVideoHotspot.subscribe((kill) => {
            this.killVideo();
        }));
    }

    killVideo() {
        this.hide();
        this.subtitleContainer.nativeElement.src = '';
        this.videoContainer.nativeElement.src = '';
        this.videoContainer.nativeElement.pause();

    }

    closeVideo() {
        this.hide();
        this.videoContainer.nativeElement.pause();
    }

    ngOnDestroy() {
        if (this.observables.length) {
            for (let evt of this.observables) {
                evt.unsubscribe();
            }
        }

        this.subtitleContainer.nativeElement.src = '';
        this.videoContainer.nativeElement.src = '';
        this.videoContainer.nativeElement.pause();
    }

    show() {
        $(`#${this.id}`).modal("open", this.options);
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }
}
