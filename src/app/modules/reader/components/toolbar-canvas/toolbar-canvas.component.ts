import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { BookEvent } from '../../../events/book/book.event';
import { ScribbleModalComponent } from '../scribble-modal/scribble-modal.component';

@Component({
    selector: 'app-toolbar-canvas',
    templateUrl: './toolbar-canvas.component.html',
    styleUrls: ['./toolbar-canvas.component.css']
})
export class ToolbarCanvasComponent implements OnInit {
    @ViewChild(ScribbleModalComponent) ScribbleModal: ScribbleModalComponent;
    @ViewChild('eraser') eraserElement: ElementRef;

    constructor(private bookEvent: BookEvent, private renderer: Renderer2) {
    }

    ngOnInit() {
        this.ScribbleModal.id = "CanvasModal";
        this.attachEventListeners();
    }

    openCanvasModal() {
        this.ScribbleModal.show();
        this.bookEvent.EchoClickEraser.emit(false);
        this.eraserElement.nativeElement.classList.remove('active')
    }
    eraser(event) {
        if (event.target.classList.contains('active')) {
            this.bookEvent.EchoClickEraser.emit(false);
            this.eraserElement.nativeElement.classList.remove('active')
        } else {
            this.bookEvent.EchoClickEraser.emit(true);
            this.eraserElement.nativeElement.classList.add('active')

        }
    }
    clear() {
        this.bookEvent.EchoCanvasSettings.emit('clear');
    }
    private attachEventListeners() {
        this.renderer.listen(this.eraserElement.nativeElement, 'click', this.eraser.bind(this));
    }

}
