export enum SettingsEnum {
    SMALL = "small",
    LARGE = "large",
    NORMAL = "normal",
    HUGE = "huge",
    CLEAR = "clear",
    RED = "#ff0000",
    BLUE = "#0000ff",
    GREEN = "#008000"
}
