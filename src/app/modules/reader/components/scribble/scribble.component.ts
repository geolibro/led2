import { Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild, Input, AfterViewInit, HostListener } from '@angular/core';
import { BookEvent } from '../../../events/book/book.event';
import { ScribblePoint } from "./scribble-point.interface";
import { LineWidthEnum } from "./line-width.enum";
import { SettingsEnum } from "./settings.enum";

@Component({
    selector: 'app-scribble',
    templateUrl: './scribble.component.html',
    styleUrls: ['./scribble.component.css']
})
export class ScribbleComponent implements OnInit, AfterViewInit, OnDestroy {

    top: number;

    @ViewChild('canvasScribble') public canvasScribble: ElementRef;
    @ViewChild('eraser') public eraser: ElementRef;
    @Input() public posWidth;
    @Input() public posHeight;

    private ctx: CanvasRenderingContext2D;
    private curColor = '#ff0000';
    private curSize = 'normal';
    private paint = false;
    private isErasing = false;
    private observables = {canvasSettings: {} = [], eraserSituation: {} = []};
    private drawHistory = [];
    private countMoves = 0;

    private readonly RUBBER_WIDTH = 30;
    private readonly RUBBER_HEIGHT = 30;
    private readonly PENCIL_CLASS = 'brushDesktop';
    private readonly RUBBER_CLASS = 'eraserDesktop';

    constructor(private renderer: Renderer2, private bookEvent: BookEvent) {
    }

    ngOnInit() {
        this.getContext();
        this.attachListeners();

        this.observables.canvasSettings.push(this.bookEvent.EchoCanvasSettings.subscribe((setting) => {
            if (setting === SettingsEnum.SMALL || setting === SettingsEnum.NORMAL || setting === SettingsEnum.LARGE || setting === SettingsEnum.HUGE) {
                this.changeBrushWeight(setting);
            } else if (setting === SettingsEnum.RED || setting === SettingsEnum.BLUE || setting === SettingsEnum.GREEN) {
                this.changeBrushColor(setting);
            } else if (setting === SettingsEnum.CLEAR) {
                this.clearAllDoodles();
            }
        }));
        this.observables.eraserSituation.push(this.bookEvent.EchoClickEraser.subscribe((situation) => {
            if (situation) {
                this.turnEraserOn();
            } else {
                this.turnEraserOff();
            }
            this.changeIcon(situation);
        }));
    }

    public ngAfterViewInit() {
        this._calculateCanvasWidthAndHeight();
        this.bookEvent.EchoEnableHammerEvents.emit(false);
    }

    public ngOnDestroy() {
        this.bookEvent.EchoEnableHammerEvents.emit(true);
        if (this.observables.eraserSituation.length) this.observables.eraserSituation[0].unsubscribe();
        if (this.observables.canvasSettings.length) this.observables.canvasSettings[0].unsubscribe();
    }

    @HostListener('touchstart', ['$event'])
    @HostListener('mousedown', ['$event'])
    onTouchStart(event: any) {
        event.stopPropagation();
        event.preventDefault();

        const touch = this.returnTouchInstance(event);
        const coordinates = this.getGesturesPosition(touch);
        this.paint = !this.isErasing;
        this.drawHistory[this.countMoves] = [];
        this.ctx.beginPath();
        if (this.paint) this.ctx.moveTo(coordinates.posX, coordinates.posY);
    }

    @HostListener('touchend', ['$event'])
    @HostListener('mouseup', ['$event'])
    onTouchEnd(event: any) {

        this.ctx.closePath();
        this.paint = false;
        ++this.countMoves;
    }

    @HostListener('touchmove', ['$event'])
    @HostListener('mousemove', ['$event'])
    onTouchMove(event: any) {
        event.stopPropagation();
        event.preventDefault();
        if (event.type == 'mousemove') {
            if (event.buttons === 0) return;
        }
        const touch = this.returnTouchInstance(event);
        const coordinates = this.getGesturesPosition(touch);
        const scribblePoint: ScribblePoint = {
            x: coordinates.posX,
            y: coordinates.posY,
            dragging: true,
            erasing: false,
            lineWidth: this.curSize,
            color: this.curColor
        };
        this.eraseOrRedraw(scribblePoint);
        this.ctx.beginPath();
        this.ctx.moveTo(coordinates.posX, coordinates.posY);
    }

    private getGesturesPosition(touch: any) {
        const canvasBoundingRect = this.ctx.canvas.getBoundingClientRect();
        return {
            'posX': (touch.clientX - canvasBoundingRect.left),
            'posY': (touch.clientY - canvasBoundingRect.top)
        }
    }

    private draw(scribblePoint: ScribblePoint) {
        this.createContextObject(scribblePoint);
        this.ctx.lineTo(scribblePoint.x, scribblePoint.y);
        this.ctx.closePath();
        this.ctx.stroke();
    }

    private createContextObject(scribblePoint: ScribblePoint) {
        Object.assign(this.ctx, {
            lineJoin: 'round',
            strokeStyle: scribblePoint.color,
            lineWidth: ({
                small: LineWidthEnum.SMALL,
                normal: LineWidthEnum.NORMAL,
                large: LineWidthEnum.LARGE,
                huge: LineWidthEnum.HUGE
            })[scribblePoint.lineWidth] || 0
        });
    }

    private redraw() {
        for (let i = 0, count = this.drawHistory.length; i < count; i++) {
            if (this.drawHistory[i]) {
                for (let j = 0, count = this.drawHistory[i].length; j < count; j++) {
                    const scribblePoint = this.drawHistory[i][j],
                        previousScribblePoint = this.drawHistory[i][j - 1];
                    this.ctx.beginPath();
                    this.createContextObject(scribblePoint);

                    if (!scribblePoint.erasing) {
                        if (j)
                            this.ctx.moveTo(previousScribblePoint.x * this.ctx.canvas.offsetWidth, previousScribblePoint.y * this.ctx.canvas.offsetHeight);
                        else
                            this.ctx.moveTo((scribblePoint.x * this.ctx.canvas.offsetWidth) - 1, scribblePoint.y * this.ctx.canvas.offsetHeight);

                        this.ctx.lineTo(scribblePoint.x * this.ctx.canvas.offsetWidth, scribblePoint.y * this.ctx.canvas.offsetHeight);
                        this.ctx.closePath();
                        this.ctx.stroke();
                    } else {
                        this.ctx.clearRect(scribblePoint.x * this.ctx.canvas.offsetWidth, scribblePoint.y * this.ctx.canvas.offsetHeight, this.RUBBER_WIDTH, this.RUBBER_HEIGHT);
                    }
                }
            }
        }
    }

    private returnTouchInstance(event: any) {
        if (event.touches) {
            return event.touches[0];
        } else {
            return event;
        }
    }

    private getContext() {
        this.ctx = this.canvasScribble.nativeElement.getContext('2d');
        this._calculateCanvasWidthAndHeight();
    }

    private _calculateCanvasWidthAndHeight(aspectRatio = 1): void {

        const parentElement = this.canvasScribble.nativeElement.parentElement.parentElement;
        const clientWidth = parentElement ? parseInt(parentElement.clientWidth) : 1;
        const clientHeight = parentElement ? parseInt(parentElement.clientHeight) : 1;
        aspectRatio = this.calculateAspectRatio(clientWidth, clientHeight);

        if (aspectRatio > 1) {
            this.ctx.canvas.width = clientWidth * aspectRatio;
            this.ctx.canvas.height = clientHeight * aspectRatio;
        } else {
            this.ctx.canvas.width = clientWidth;
            this.ctx.canvas.height = clientHeight;
        }
        this.posHeight = this.ctx.canvas.height;
        this.posWidth = this.ctx.canvas.width;
    }

    private changeOrientation(event) {
        setTimeout(() => {
            this._calculateCanvasWidthAndHeight();
            this.redraw();
        }, 300);
    }
    private resize(event) {
        setTimeout(() => {
            this._calculateCanvasWidthAndHeight();
            this.redraw();
        }, 500);

    }

    private removePositions(scribblePoint: ScribblePoint) {
        this.drawHistory[this.countMoves].push(scribblePoint);
    }

    private erasing(scribblePoint: ScribblePoint) {
        this.ctx.clearRect(scribblePoint.x, scribblePoint.y, this.RUBBER_WIDTH, this.RUBBER_HEIGHT);
        scribblePoint.x = scribblePoint.x / this.ctx.canvas.width;
        scribblePoint.y = scribblePoint.y / this.ctx.canvas.height;
        scribblePoint.erasing = true;

        this.removePositions(scribblePoint);

    }

    private attachListeners() {
        this.renderer.listen('window', 'orientationchange', this.changeOrientation.bind(this));
        this.renderer.listen('window', 'resize', this.resize.bind(this));
    }

    private turnEraserOn() {
        this.isErasing = true;
    }

    private turnEraserOff() {
        this.isErasing = false;
    }

    private clearAllDoodles() {
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.drawHistory = [];
        this.countMoves = 0;
    }

    private changeBrushWeight(setting) {
        this.ctx.canvas.classList.remove(this.RUBBER_CLASS);
        this.ctx.canvas.classList.add(this.PENCIL_CLASS);
        this.curSize = setting;
    }

    private changeBrushColor(setting) {
        this.ctx.canvas.classList.remove(this.RUBBER_CLASS);
        this.ctx.canvas.classList.add(this.PENCIL_CLASS);
        this.curColor = setting;
    }

    private changeIcon(situation) {
        if (situation) {
            this.ctx.canvas.classList.remove(this.PENCIL_CLASS);
            this.ctx.canvas.classList.add(this.RUBBER_CLASS);
        } else {
            this.ctx.canvas.classList.remove(this.RUBBER_CLASS);
            this.ctx.canvas.classList.add(this.PENCIL_CLASS);

        }

    }

    private calculateAspectRatio(clientWidth, clientHeight) {
        let aspectRatio = 1;
        if (this.ctx.canvas.width < clientWidth)
            aspectRatio = this.ctx.canvas.width / clientWidth;
        if (Math.abs(aspectRatio - 1) < 1e-14 && this.ctx.canvas.height < clientHeight)
            aspectRatio = this.ctx.canvas.height / clientHeight;
        return aspectRatio;
    }

    private eraseOrRedraw(scribblePoint: ScribblePoint) {
        if (this.paint) {
            this.draw(scribblePoint);
            scribblePoint.x = scribblePoint.x / this.ctx.canvas.width;
            scribblePoint.y = scribblePoint.y / this.ctx.canvas.height;
            this.drawHistory[this.countMoves].push(scribblePoint);
        } else {
            if (this.isErasing) this.erasing(scribblePoint);
        }
    }


}
