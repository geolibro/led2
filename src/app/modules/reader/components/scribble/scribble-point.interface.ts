export interface ScribblePoint {
    x: number,
    y: number,
    lineWidth: any,
    color: string,
    dragging: boolean,
    erasing: boolean
}
