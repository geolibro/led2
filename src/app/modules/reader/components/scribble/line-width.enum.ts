export enum LineWidthEnum {
    SMALL = 2,
    NORMAL = 5,
    LARGE = 10,
    HUGE = 20
}
