import { Input, OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';

import { BookEvent } from '../../../events/book/book.event';
import { VaultToolsService } from '../../../services/vault/vault-tools.service';

@Component({
  selector: 'app-e-pub-book-reader',
  templateUrl: './e-pub-book-reader.component.html',
  styleUrls: ['./e-pub-book-reader.component.css']
})
export class EPubBookReaderComponent implements OnInit {

  constructor(private bookEvent: BookEvent, private tools: VaultToolsService) {}

  ngOnInit() {

    this.tools.initModals();

    this.bookEvent.EchoFormatText.subscribe(style => {
      this.bookEvent.EchoFormatTextBookSlider.emit(style);
    });
  }
}
