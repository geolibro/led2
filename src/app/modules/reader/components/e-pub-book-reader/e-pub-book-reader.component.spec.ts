import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPubBookReaderComponent } from './e-pub-book-reader.component';

describe('EPubBookReaderComponent', () => {
  let component: EPubBookReaderComponent;
  let fixture: ComponentFixture<EPubBookReaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPubBookReaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPubBookReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
