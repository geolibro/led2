import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPubBookSliderComponent } from './e-pub-book-slider.component';

describe('EPubBookSliderComponent', () => {
  let component: EPubBookSliderComponent;
  let fixture: ComponentFixture<EPubBookSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPubBookSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPubBookSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
