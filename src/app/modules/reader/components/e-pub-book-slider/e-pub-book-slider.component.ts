import { DomSanitizer } from '@angular/platform-browser';
import { PageReaderService } from '../../../services/reader/page-reader.service';
import { Component, Input, OnDestroy, OnInit, ViewChild, ElementRef, Inject, Renderer2 } from '@angular/core';
import { BookEvent } from '../../../events/book/book.event';
import { PlatformContract } from '../../../services/contracts/platform/platform.contract';
import { LedFileSystemContract } from '../../../services/contracts/file-system/led-file-system.contract';
import { PlatformEnum } from '../../../services/contracts/platform/platform.enum';
import { EpubContract } from "../../../services/contracts/epub/epub.contract";

@Component({
    selector: 'app-e-pub-book-slider',
    templateUrl: './e-pub-book-slider.component.html',
    styleUrls: ['./e-pub-book-slider.component.css']
})
export class EPubBookSliderComponent implements OnInit, OnDestroy {

    static counter: number = 0;
    @Input() newStyle: {} = {};
    @Input() EpubHTML: any;
    @Input() page: number = 0;
    aria = false;
    isDesktop: boolean = false;
    @ViewChild('iframe') iframe: ElementRef;
    private changeDetected: boolean = false;
    private epubLoaded: boolean = false;
    private loader: any;
    private deviceWidth: string = window.screen.width.toString();
    private deviceHeight: number = window.screen.height;
    private anchor: string;
    private currentPageObject: any = {};

    constructor(private sanitizer: DomSanitizer,
                private bookEvent: BookEvent,
                @Inject('PlatformService') private platform: PlatformContract,
                private PageReader: PageReaderService,
                private Renderer: Renderer2,
                @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
                @Inject('EpubDocumentService') private EpubDocument: EpubContract) {

        this.EpubHTML = this.sanitizer.bypassSecurityTrustResourceUrl('iframe.html');

        this.onDeviceOrientation();

        $(document).ready(() => {
            this.loader = $('.fakeloader');

            this.loader.fakeLoader({
                timeToHide: 1600,
                bgColor: '#f2f2f2',
                spinner: 'spinner2',
                zIndex: 9999
            });
            $(window).on('resize', () => {
                this.loader.show();
                this.processPage(false);
            });
        });
    }


    ngOnInit() {

        this.getPage(this.page).then((bytesArray: Array<any>) => {
            this.currentPageObject['bytesArray'] = bytesArray;
            this.generateDocument();
        }).catch(error => {
            console.log(error);
        });
        this.addEPubEventListener();

        this.bookEvent.EchoFormatTextBookSlider.subscribe((style) => {
            if (!this.EpubDocument) return;
            this.newStyle = style;
            this.EpubDocument.setStyle(style);
        });
        this.bookEvent.EchoAriaHiddenModal.subscribe((isHidden) => {
            this.aria = isHidden;
        });
        this.isDesktop = this.platform.is(PlatformEnum.DESKTOP);
    }

    navigatePages(direction) {
        const page = this.EpubDocument.navigateDirection(direction);
        if (page) {
            this.page = page;
            this.processPage();
        }
    }

    resolveEPubLink(event: any) {
        event.preventDefault();

        const page = event.target.getAttribute('href');
        if (!page || page === "") {
            return;
        }

        if (/^https?:.*/i.test(page)) { // Link to external page, open in Browser
            this.fileSystem.openLinkExternally(page);
            return;
        }


        this.PageReader.getPageByHref(page).then(obj => {
            const anchor = obj.anchor || '';

            this.EpubDocument = this.EpubDocument.bootstrapEpubPage(obj.bytes, this.fileSystem.getDirectory(), this.iframe.nativeElement);
            /** BIOHAZARD */
            this.EpubDocument.generateDocument(this.PageReader).then(blobAddress => {
                this.EpubHTML = this.sanitizer.bypassSecurityTrustResourceUrl(blobAddress);
                this.anchor = obj.anchor;
                this.loader.show();
            });

        }).catch(error => {
            console.log(error);
        });

        return;
    }


    ngOnDestroy() {
        this.PageReader.destroy();
    }

    onDeviceOrientation() {
        window.addEventListener('orientationchange', (event) => {
            if (window.orientation === 90 || window.orientation === -90) {

                if (this.platform.is(PlatformEnum.ANDROID)) {
                    this.deviceWidth = window.screen.width.toString();
                    this.deviceHeight = window.screen.height;
                }

                if (this.platform.is(PlatformEnum.IOS)) {
                    this.deviceWidth = window.screen.height.toString();
                    this.deviceHeight = window.screen.width;
                }

            } else {

                this.deviceWidth = window.screen.width.toString();
                this.deviceHeight = window.screen.height;

            }
            this.iframe.nativeElement.contentWindow.location.reload(true);

        });
    }

    private addEPubEventListener() {
        this.Renderer.listen(this.iframe.nativeElement, 'load', (event: any) => {
            if (event.target.src.indexOf('iframe.html') !== -1) {
                return event.preventDefault();
            }

            this.epubLoaded = false;
            this.loader.fadeOut();

            this.emitResizeCall();

            this.EpubDocument.addSummaryEventListener(this.anchor, this.platform).then((eventClick: Event) => {
                this.resolveEPubLink(eventClick);
                this.anchor = '';
            });

            if (this.newStyle) {
                this.EpubDocument.setStyle(this.newStyle);
            }

            /*
              MOBILE
             */
            if (this.platform.is(PlatformEnum.MOBILE)) {
                this.EpubDocument.addSwipeEventListener().then((page: number) => {
                    this.page = page;
                    this.processPage();
                });
            }
        });
    }

    private getPage(pageNumber: number) {
        return new Promise((resolve, reject) => {
            this.PageReader.processPage(pageNumber).then(resolve).catch(error => {
                if (error === 'NOT_FOUND') {
                    this.getPage(pageNumber++).then().catch(reject);
                }
            });
        });
    }

    private emitResizeCall() {
        const html = this.iframe.nativeElement;
        this.bookEvent.EchoResizeEpubWindow.emit({
            scrollWidth: html.scrollWidth, offsetWidth: html.offsetWidth,
            scrollHeight: html.scrollHeight, offsetHeight: html.offsetHeight
        });
    }

    private generateDocument(reattachBytesArray = true) {
        debugger;
        const directory = this.fileSystem.getDirectory();
        if (reattachBytesArray && this.EpubDocument) {
            this.EpubDocument = this.EpubDocument.bootstrapEpubPage(this.currentPageObject['bytesArray'], directory, this.iframe.nativeElement);
        } else {
            if (!this.EpubDocument) {
                this.EpubDocument = this.EpubDocument.bootstrapEpubPage(this.currentPageObject['bytesArray'], directory, this.iframe.nativeElement);
            }
        }
        this.EpubDocument.generateDocument(this.PageReader).then(obj => {
            this.EpubHTML = this.sanitizer.bypassSecurityTrustResourceUrl(obj['doc']);
            this.loader.show();
            this.EpubDocument.updateEpubContainer();
            // this.loader.hide();
        });
    }

    private processPage(reattachBytesArray = true) {
        this.PageReader.processPage(this.page).then(bytesArray => {
            const oldBytesArray = this.currentPageObject['bytesArray'];
            this.currentPageObject['bytesArray'] = bytesArray;
            if (!reattachBytesArray) this.currentPageObject['bytesArray'] = oldBytesArray;
            this.generateDocument();
        }).catch(error => {
            if (error.hasOwnProperty('error') && error.error === 'NOT_FOUND') {
                this.page = 0;
                this.getPage(this.page).then((bytesArray: Array<any>) => {
                    if (reattachBytesArray) this.currentPageObject['bytesArray'] = bytesArray;
                    this.generateDocument();
                }).catch(error => {
                    console.log(error);
                });
            }
            console.log(error);
        });
    }
}
