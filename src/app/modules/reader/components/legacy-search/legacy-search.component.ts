import { BookEvent } from '../../../events/book/book.event';
import { Router } from '@angular/router';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { HotspotLegacyService } from '../../../services/reader/hotspot-legacy.service';
import { isNullOrUndefined } from "util";

@Component({
    selector: 'app-legacy-search',
    templateUrl: './legacy-search.component.html',
    styleUrls: ['./legacy-search.component.css']
})
export class LegacySearchComponent implements OnInit, OnDestroy {
    aria = false;
    searchClicked = false;
    tapped: boolean = false;
    searchField: HTMLElement;
    bookTitleContainer: HTMLElement;
    wordNavigation: HTMLElement;
    startSearch: HTMLElement;
    completeSearch: HTMLElement;
    currentPage: any;
    pageNumbers: Array<any> = [];
    word: string;
    pog: any;
    private _currentHotspotsLoaded: Array<any> = [];
    private _observables: any = {};

    @Input() searchAvailable: boolean = false;

    constructor(private bookEvent: BookEvent, private hotpostService: HotspotLegacyService) {
        this._observables.ariaHidden = {};
        this._observables.currentPage = {};
    }

    ngOnInit() {
        this._observables.ariaHidden = this.bookEvent.EchoAriaHiddenModal.subscribe((isHidden) => {
            this.aria = isHidden;
        });
        this._observables.currentPage = this.bookEvent.EchoCurrentPage.subscribe(currentPage => {

            if (this.currentPage == null) {
                // na falta de evento adequado, gambiarra p/ assegurar que inicalizacao dos hotspots de busca seja feito no momento certo
                // (após módulo leitor ter sido inicializado)
                let startTime = Date.now();
                this.hotpostService.loadWordHotpots().then((hasWordHotspots) => {
                    this.searchAvailable = hasWordHotspots;
                    console.log('loadWordHotspots -> time elapsed to load indexed search data: ' + (Date.now() - startTime) + ' (searchAvailable = ' + this.searchAvailable + ')') ;

                }).catch(error => {
                    let errorStr = 'legacy-search-component -> ngOnInit -> erro solicitando hotspots de busca! ' + error.toString();
                    console.log(errorStr);
                })
            }

            this.currentPage = currentPage;
        });

        this.wordNavigation = document.getElementById('word-navigation');
        this.searchField = document.getElementById('searchField');
        this.bookTitleContainer = document.getElementById('bookTitle');
        this.startSearch = document.getElementById('pog');
        this.completeSearch = document.getElementById('completeSearch');

        this.wordNavigation.style.display = 'none';
        document.getElementById('inputSearchField').addEventListener('keydown', (event) => {
            if (document.getElementById('inputSearchField')['value'] === ''
                || document.getElementById('inputSearchField')['value'].length <= 2) {
                this._clearHotspots('word-hotspot');
                this.completeSearch.style.display = 'block';
                this.startSearch.style.display = 'none';
                this.wordNavigation.style.display = 'none';
            }
        });
        this.bookEvent.EchoKillSearch.subscribe((isKilled) => {
            this._clearHotspots('word-hotspot');
        })


    }

    activeSearch() {
        this.searchClicked = !this.searchClicked;
        if (this.searchClicked) {
            this.searchField.style.display = 'block';
            this.bookTitleContainer.style.display = 'none';
            this.startSearch.style.display = 'none';
            this.completeSearch.style.display = 'block';
            this.bookEvent.EchoChangeSearch.emit(true);
        }
    }

    ngOnDestroy() {
        this._observables.ariaHidden.unsubscribe();
        this._observables.currentPage.unsubscribe();
        this.searchAvailable = false;
    }

    changePage(direction = 'next') {
        let nextPage = 0;
        const nextIndex = direction === 'next' ? this.pageNumbers.indexOf(this.currentPage.internal.toString()) + 1 : this.pageNumbers.indexOf(this.currentPage.internal.toString()) - 1,
            firstIndex = direction === 'next' ? 0 : this.pageNumbers.length - 1;
        if (!this.pageNumbers[nextIndex]) {
            nextPage = this.pageNumbers[firstIndex];
        } else {
            nextPage = this.pageNumbers[nextIndex];
        }
        this._goAhead(nextPage, direction);
    }

    search() {
        this.word = document.getElementById('inputSearchField')['value'];
        if (!this.word) {
            this.wordNavigation.style.display = 'none';
            Materialize.toast('Busca não pode estar vazia', 3000);
            return;
        }
        this.completeSearch.style.display = 'none';
        this.wordNavigation.style.display = 'block';

        this.loadSearchHotspot(this.word);
    }

    public loadSearchHotspot(word: string, page?: any): void {
        this.bookEvent.EchoChangeSearch.emit(true);
        const loadPages = this.hotpostService.getHotspotsForWord(word);
        if (loadPages.length === 0) {
            Materialize.toast('Termo não encontrado.', 3000);
            return;
        }

        if (loadPages.length) {
            loadPages.map((pageNumber) => {
                this._currentHotspotsLoaded.push(pageNumber);
                this.pageNumbers = this.pageNumbers.concat(Object.keys(pageNumber).sort((a, b) => {
                    return parseInt(a) - parseInt(b);
                }));
            });
        }
        if (page) {
            this.currentPage = { internal: page };
            this.bookEvent.EchoBookPage.emit({ 'pageNumber': page, 'isRawNumber': true });
            this.bookEvent.EchoShowWordHotspot.emit([this.word, page]);
        }
        if (!page && this.pageNumbers.indexOf(this.currentPage.internal.toString()) === -1) {

            const min = this.pageNumbers[0];
            if (isNullOrUndefined(min)) return;
            this.bookEvent.EchoBookPage.emit({ 'pageNumber': min, 'isRawNumber': true });
            this.bookEvent.EchoShowWordHotspot.emit([this.word, min]);
        } else {
            if (!page) {
                this.bookEvent.EchoShowWordHotspot.emit([this.word, this.currentPage.internal]);
            }
        }

    }

    private _goAhead(page?: any, direction?: string): any {
        let countHotspotsLoaded = [];
        if (this._currentHotspotsLoaded.length) {
            this._currentHotspotsLoaded.map((hotspot) => {
                let isLoaded = false;

                if (hotspot[this.currentPage.internal.toString()]) {
                    countHotspotsLoaded = countHotspotsLoaded.concat(hotspot[this.currentPage.internal.toString()]);
                }
            })
        }
        if (countHotspotsLoaded.length > 1) {
            let allWordHotspots = document.getElementsByClassName('word-hotspot');

            if (allWordHotspots) {
                const _allWordHotspots = [].slice.call(allWordHotspots);
                const optimalValues = this._getOptimalValues(allWordHotspots);
                const getCurrentActiveHotspotID = document.getElementsByClassName('active')[0].getAttribute('hotspotId');
                for (let i = 0, count = _allWordHotspots.length; i < count; i++) {
                    if ((direction === 'next' &&
                        (getCurrentActiveHotspotID == null || getCurrentActiveHotspotID == optimalValues['max'])) ||
                        (direction === 'prev' && getCurrentActiveHotspotID == optimalValues['min'])) {
                        this.loadSearchHotspot(this.word, page);
                        break;
                    }
                    if (_allWordHotspots[i].classList.contains('active')) {
                        this._resetActiveHotspots(_allWordHotspots[i]);
                        if (direction === 'next' && _allWordHotspots[i + 1]) {
                            this._setHotspotActive(_allWordHotspots[i + 1]);
                        }
                        if (direction === 'prev' && _allWordHotspots[i - 1]) {
                            this._setHotspotActive(_allWordHotspots[i - 1]);
                        }
                        break;
                    }
                }
                return _allWordHotspots;
            }
            return undefined;
        } else {
            this.loadSearchHotspot(this.word, page);
        }
    }

    private _setHotspotActive(hotspot: any): void {
        hotspot.classList.add('active');
        hotspot.style.opacity = '0.6';
    }

    private _resetActiveHotspots(hotspot: any): void {
        hotspot.classList.remove('active');
        hotspot.style.opacity = '0.4';
    }

    private _getOptimalValues(wordHotspots: NodeList): any {
        const arrHotspots = [].slice.call(wordHotspots);
        const maxId = arrHotspots.sort((a, b) => {
            return b.getAttribute('hotspotId') - a.getAttribute('hotspotId');
        })[0].getAttribute('hotspotId'), minId = arrHotspots.sort((a, b) => {
            return a.getAttribute('hotspotId') - b.getAttribute('hotspotId');
        })[0].getAttribute('hotspotId');
        return {
            'max': maxId,
            'min': minId
        }
    }

    private _clearHotspots(divClass = 'word-hotspot') {
        const hotspotList = document.getElementById('canvasHotspot').getElementsByClassName(divClass);

        while (hotspotList.length > 0) {
            const element = hotspotList.item(0);
            element.remove();
        }
    }

}
