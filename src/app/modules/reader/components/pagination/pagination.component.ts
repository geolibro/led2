import { Component, Input, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { BookEvent } from '../../../events/book/book.event';
import { ModalContract } from '../../../../contracts/modal.contract';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, ModalContract {
    id: string;
    title: string = 'Ir para a página';

    @Input() page: any;
    @ViewChild('paginationInputNumber') paginationInput: ElementRef;

    constructor(private bookEvent: BookEvent) {}

    isValid(event: any): number {
        return event.target.value;
    }

    ngOnInit() {

        this.bookEvent.EchoCurrentPage.subscribe(({readable: currentPage}) => {
            this.page = currentPage;
            this.setCoverLabel();
            this.paginationInput.nativeElement.value = this.page;
        });
        this.setCoverLabel();
        this.paginationInput.nativeElement.value = this.page;
    }

    changePage() {
        this.page = this.paginationInput.nativeElement.value;
        if (isNaN(parseInt(this.page)) && this.page.toLowerCase() != 'capa') {
            return;
        }
        if (!this.page) {
            return;
        }
        this.bookEvent.EchoBookPage.emit({pageNumber: this.page, isRawNumber: false});
        this.hide();
    }

    show() {
        $(`#${this.id}`).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }

    private setCoverLabel() {
        if (!this.page || this.page <= 1) {
            this.page = 'capa';
        }
    }
}

