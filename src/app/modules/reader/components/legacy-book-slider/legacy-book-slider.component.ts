import { LegacyReaderService } from '../../../services/reader/legacy-reader.service';
import { Component, Input, OnDestroy, OnInit, ViewChild, Inject } from '@angular/core';
import * as Hammer from 'hammerjs';
import { PageReaderService } from '../../../services/reader/page-reader.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { BookEvent } from '../../../events/book/book.event';
import { ContentManagerService } from '../../../services/content-manager/content-manager.service';
import { LegacyHotspotContainerComponent } from '../legacy-hotspot-container/legacy-hotspot-container.component';
import { HotspotLegacyService } from '../../../services/reader/hotspot-legacy.service';
import { LegacyBookContract } from '../../../../contracts/legacy-book.contract';
import { UserSettingsService } from '../../../services/user-settings.service';
import { PageMarkerComponent } from '../page-marker/page-marker.component';
import { LogProvider } from '../../../../providers/log.provider';
import { AuthGuardService } from '../../../services/guard/auth-guard.service';
import { PlatformContract } from '../../../services/contracts/platform/platform.contract';
import { DialogService } from '../../../services/mobile/dialog.service';
import { DownloadStopReason } from '../../../services/content-manager/download-manager.service';

@Component({
  selector: 'app-legacy-book-slider',
  templateUrl: './legacy-book-slider.component.html',
  styleUrls: ['./legacy-book-slider.component.css']
})
export class LegacyBookSliderComponent implements OnDestroy, OnInit {

  private pageNumber: number;
  HammerJS: HammerManager;
  bookLedId: string;
  @Input() pageImage: SafeResourceUrl | string = '';
  @Input() pageTeacherImage: SafeResourceUrl | string = '';

  @Input() pageTeacherImageHeight: string = '';

  @Input() isScribble: boolean = false;
  @Input() isPageLoaded: boolean = false;
  @ViewChild(LegacyHotspotContainerComponent) hotspotContainer: LegacyHotspotContainerComponent;
  @ViewChild(PageMarkerComponent) pageMarker: PageMarkerComponent;


  private Log: LogProvider = LogProvider.getInstance('BookSliderComponent');
  private transforms: Array<string> = [];

  private installedBookManifest: any;

  private initialScale: number = 1;
  private initialDeltaX: number = 0;
  private initialDeltaY: number = 0;
  private currentScale = null;
  private currentDeltaX = null;
  private currentDeltaY = null;
  private teacherLayerUserEnabled: boolean = false;
  private loader: any;
  private pageImgElement: HTMLElement;
  private pageTeacherImgElement: HTMLElement;
  private header: any;
  private footer: HTMLElement;
  private eventSubscriptions: Array<any>;
  private tapped: boolean = true;
  private isAudioPlaying = false;

  constructor(private bookEvent: BookEvent,
    @Inject('PlatformService') private platform: PlatformContract,
    private sanitizer: DomSanitizer,
    private PageReader: PageReaderService,
    private legacyReaderService: LegacyReaderService,
    private contentManager: ContentManagerService,
    private hotspotManager: HotspotLegacyService,
    private userSettings: UserSettingsService,
    private auth: AuthGuardService,
    @Inject("DialogService") private dialogService: DialogService) {
    this.eventSubscriptions = Array<any>();
    this.pageImage = '';
    this.pageTeacherImage = '';
    this.bookLedId = this.PageReader.reader.getBookId();
    this.pageNumber = this.userSettings.getLastPage(this.bookLedId);
  }

  ngOnInit() {

    this.loader = $('.fakeloader');

    let user = this.auth.user().led_user_id;
    this.PageReader.load(this.legacyReaderService, this.bookLedId, () => {
        this.installedBookManifest = this.PageReader.reader.getInstalledManifest();

        this.hotspotManager.init(this.installedBookManifest, user).then(() => {
            this.bookEvent.EchoAnnotationsUpdated.emit(true);
            this.askForPage(this.getPageId(this.pageNumber));


          }).catch(error => {
            console.log('Erro na inicialização dos hotspots! (Erro: ' + error.toString() + ')');

            this.bookEvent.EchoAnnotationsUpdated.emit(false);
            // a página poderá ser carregada mesmo que haja algum problema nos hotspots
            this.askForPage(this.getPageId(this.pageNumber));

          });
    });

    this.eventSubscriptions.push(this.bookEvent.EchoBookPage.subscribe((pageInfo) => {
      let pageNumber;
      if (pageInfo.isRawNumber) {
        pageNumber = pageInfo.pageNumber;
      }
      else {
        pageNumber = this.getPageNumberByDisplayNumber(pageInfo.pageNumber.toString());
      }

      if (this.isValidPage(pageNumber)) {
        const pageId = this.getPageId(pageNumber);
        this.askForPage(pageId);
      }
      else {
        this.showToast('Página inválida!');
      }
    }));

    this.eventSubscriptions.push(this.bookEvent.EchoEnableHammerEvents.subscribe((e) => {
      this.setHammerEventsEnabled(e);
    }));
    // this.eventSubscriptions.push(this.bookEvent.EchoAudioPlayerRunning.subscribe((isRunning) => {
    //   this.bookEvent.EchoHideBar.emit(true);
    //   this.HammerJS.get('tap').set({enable: !isRunning});
    // }));

    this.eventSubscriptions.push(this.bookEvent.EchoStartUserHotspot.subscribe(() => {
      this.restoreZoomDefault();

      this.setHammerEventsEnabled(false);

      this.hotspotContainer.toggleHotspotCapture(() => {

        this.setHammerEventsEnabled(true);

      });

      this.showToast('Clique na página e arraste para criar sua marcação!');
    }));

    this.bookEvent.EchoHideBar.emit(this.tapped);

    this.pageImgElement = document.getElementById('page');
    this.pageTeacherImgElement = document.getElementById('pageTlayer');

    this.header = document.getElementsByClassName('navbar-fixed')[0].clientHeight ? document.getElementsByClassName('navbar-fixed')[0] : document.getElementsByClassName('navbar-fixed')[1];
    this.footer = document.getElementsByTagName('footer')[0];
    // Init pageelement

    this.eventSubscriptions.push(this.bookEvent.EchoToggleTeacherLayer.subscribe(isEnabled => {
      this.teacherLayerUserEnabled = isEnabled;
      const currentPageId = this.getPageId(this.pageNumber);
      this.askForPage(this.pageNumber);
    }));

    this.contentManager.getBook(this.bookLedId).then(
      (book: LegacyBookContract) => {

        let allowTeacherLayer =
          this.contentManager.isTeacherLayerInstalled(this.bookLedId) &&
          (book.has_teacher_layer_access == 1);

        this.bookEvent.EchoTeacherLayerAvailable.emit(allowTeacherLayer);

      }).catch((error) => {
        this.bookEvent.EchoTeacherLayerAvailable.emit(false);

      });


    this.eventSubscriptions.push(this.bookEvent.EchoEnableScribble.subscribe(isScribble => {
      this.isScribble = isScribble;
    }));

    this.HammerJS = new Hammer(document.getElementById('page-slider'), {
      domEvents: true,
      touchAction: 'pan-y'
    });

    this.pageImgElement.parentElement.style.top = this.header.offsetHeight + 'px';
    document.getElementById('page-slider').addEventListener('tap', (event: any) => {

      if (event.target.getAttribute('id') === 'audioPlayer' ||
        event.target.getAttribute('id') === 'playIcon' ||
        event.target.getAttribute('id') === 'stopIcon' ||
        event.target.getAttribute('id') === 'closeIcon' ||
        event.target.getAttribute('id') === 'seek' ||
        event.target.getAttribute('id') === 'closePlayerContainer' ||
        event.target.getAttribute('id') === 'playerActionButtons' ||
        event.target.getAttribute('id') === 'player') {
        return;
      }
      if (event.target.getAttribute('id') === 'pageMarker') {
        return;
      }
      if (event.target.getAttribute('class') === 'hotspot') {
        return;
      }
      if (event.target.getAttribute('id') === 'annotationMarker') {
        return;
      }
      this.tapped = !this.tapped;
      this.bookEvent.EchoHideBar.emit(this.tapped);
    });
    this.setupSwipe();
    this.setupZoom();

    window.addEventListener('resize', () => {
      this.pageTeacherImageHeight = document.getElementById('page').getAttribute('height');
    });
  }

  ngOnDestroy() {
    this.pageImage = '';
    this.PageReader.destroy();
    this.bookEvent.EchoKillAudio.emit(true);
    this.bookEvent.EchoKillAudioWithoutControl.emit(true);
    for (let evt of this.eventSubscriptions) {
      evt.unsubscribe();
    }
  }

  setHammerEventsEnabled(enable) {
    this.HammerJS.get('pinch').set({ enable });
    this.HammerJS.get('pan').set({ enable });
    this.HammerJS.get('swipe').set({ enable });
  }

  private setupSwipe() {

    this.HammerJS.get('swipe').set({ enable: true });

    $('.imageContainer').on('swipeleft', e => {
      let pageNumberToGo = this.pageNumber + 1;

      if (!this.isValidPage(pageNumberToGo)) {
        return;
      }

      let pageId = this.getPageId(pageNumberToGo);
      this.askForPage(pageId);
      this.bookEvent.EchoCourtainOnSlide.emit(true);
      this.bookEvent.EchoKillVideoHotspot.emit(true);
      this.bookEvent.EchoKillAudio.emit(false);
      this.bookEvent.EchoKillAudioWithoutControl.emit(true);
      this.bookEvent.EchoKillSearch.emit(true);
    });

    $('.imageContainer').on('swiperight', e => {

      let pageNumberToGo = this.pageNumber - 1;

      if (!this.isValidPage(pageNumberToGo)) {
        return;
      }

      const pageId = this.getPageId(pageNumberToGo);
      this.askForPage(pageId);
      this.bookEvent.EchoCourtainOnSlide.emit(true);
      this.bookEvent.EchoKillVideoHotspot.emit(true);
      this.bookEvent.EchoKillAudio.emit(false);
      this.bookEvent.EchoKillAudioWithoutControl.emit(true);
      this.bookEvent.EchoKillSearch.emit(true);
    });
  }

  private setupZoom() {
    this.HammerJS.get('pinch').set({ enable: true });
    this.HammerJS.get('pan').set({ enable: true });
    this.HammerJS.get('pinch').recognizeWith(this.HammerJS.get('pan'));

    // Handles pinch and pan events/transforming at the same time;
    // Handles pinch and pan events/transforming at the same time;
    $('#page-slider').on('pinch pinchend', (ev: any) => {
      this.HammerJS.set({
        touchAction: 'pan-x pan-y'
      });
      const gesture = ev.originalEvent ? ev.originalEvent.gesture : ev.gesture;
      // Adjusting the current pinch/pan event properties using the previous ones set when they finished touching
      this.currentScale = this.initialScale * gesture.scale;

      this.currentDeltaX =
        this.initialDeltaX +
          gesture.deltaX / this.currentScale;
      this.currentDeltaY =
        this.initialDeltaY +
          gesture.deltaY / this.currentScale;

      this.currentScale =
        parseFloat(this.currentScale) <= 1.0 ? 1.0 : this.currentScale;

      if (this.currentScale > 1) {

        this.HammerJS.get('swipe').set({ enable: false });

        $('#page-slider').scrollLeft(
            gesture.center.x * this.currentScale
        );
        $('#page-slider').scrollTop(
            gesture.center.y * this.currentScale
        );
        if (document.getElementById('userRect')) {
          const userRect = document.getElementsByClassName('hotspot');
          console.log(this.currentScale);
          for (let i = 0; i < userRect.length; i++) {
            const userRectProportion: number = parseFloat(userRect[i]['style'].left.replace('%', '')) + (parseFloat(this.pageImgElement['style'].width) - 100);
            userRect[i]['style'].left = userRectProportion + '%';
          }
        }
        // console.log();
      } else {
        this.HammerJS.get('swipe').set({ enable: true });
        this.resetToSwipeMode();
      }
      Object.assign(this.pageImgElement.style, {
        width: this.currentScale * 100 + '%'
      });
      Object.assign(this.pageTeacherImgElement.style, {
        width: this.currentScale * 100 + '%',
        heigth: '100%'
      });
    });
  }

  private resetToSwipeMode() {
    this.HammerJS.set({
      touchAction: 'pan-y'
    });
    this.HammerJS.get('swipe').set({ enable: true });
  }

  private restoreZoomDefault() {
    this.initialScale = 1;
    this.initialDeltaX = 0;
    this.initialDeltaY = 0;

    if (!this.pageImgElement) return;

    Object.assign(this.pageImgElement.style, {
      width: '100%'
    });

    Object.assign(this.pageTeacherImgElement.style, {
      width: '100%'
    });
  }

  private getDisplayNumberByPageNumber(pageNumber: number) {
    const bookInfo = this.installedBookManifest.book;
    return (
      pageNumber - bookInfo.book_first_numbered_page + bookInfo.book_first_numbered_page_offset
    );
  }

  private getPageNumberByDisplayNumber(displayNumber: string) {
    /*
      código original oriundo da classe PageViewController do LED_Android (LED 1.x);
      trecho que jogava qualquer página de capa para primeira página de capa foi removido
    */
    const bookInfo = this.installedBookManifest.book;
    let pageNumber = 0;
    const lastPageNumber = bookInfo.book_page_count;

    if (displayNumber.toLowerCase() === 'capa') {
      pageNumber = 1;
    } else {
      pageNumber = parseInt(displayNumber);
    }

    return (
      bookInfo.book_first_numbered_page +
      (pageNumber - bookInfo.book_first_numbered_page_offset)
    );
  }

  private getPageId(pageNumber: number) {
    let pageId = this.installedBookManifest.page_id_number_map[pageNumber];
    return pageId;
  }

  private getPageNumber(pageId: number) {
    let pageNumber = this.installedBookManifest.page_number_id_map[pageId];
    return pageNumber;
  }

  private askForPage(pageId_: number) {

    this.restoreZoomDefault();
    this.showPageProgress(undefined);

    this.contentManager.pollPageStatus(this.bookLedId, pageId_,
      (bookId, pageId, status) => {
        this.updatePageStatus(bookId, pageId, status);
      }
    );
  }

  public updatePageStatus(bookId, pageId, status) {
    // TODO: also check for page id
    if (this.bookLedId != bookId) {
      console.log('received page status notificaton from unrelated book!');
      return;
    }
    if (status == 'instalado') {
      this.loadPage(pageId);
    } else {
        this.showPageProgress(status);

        let msg;
        switch (status) {
            case DownloadStopReason.NO_CONNECTION:
                msg = "Esta página não foi ainda baixada, e não é possível baixar agora por falta de conexão. - Você será redirecionado para a página 1";

            break;
            case DownloadStopReason.NO_DISK_SPACE:
                msg = "Esta página não foi ainda baixada, não é possível baixar agora por falta de espaco no dispositivo. Você será redirecionado para a página 1";
            break;
            default:
                // TODO: implement case for Pause
            break;
        }

        this.dialogService.onAlert(msg, "Problema ao baixar página");
        this.loadPage(this.getPageId(1));


    }
  }

  private loadPage(pageId) {
    let shouldLoadTeacherLayer = this.teacherLayerUserEnabled &&
      this.contentManager.isTeacherLayerInstalled(this.bookLedId);
    let startTime = Date.now();

    this.PageReader
      .loadLegacyPageData(pageId, shouldLoadTeacherLayer)
      .then(images => {

        this.pageNumber = this.getPageNumber(pageId);
        this.userSettings.setLastPage(this.bookLedId, this.pageNumber);
        this.bookEvent.EchoCurrentPage.emit({
          internal: this.pageNumber,
          readable: this.getDisplayNumberByPageNumber(this.pageNumber)
        });

        console.log('setting current page to ' + this.pageNumber);

        let pageImgBytes = images[0];

        this.pageImage = this.sanitizer.bypassSecurityTrustResourceUrl(this.PageReader.getPage(pageImgBytes, { type: 'image/jpeg' }));
        $(window).scrollTop(0);
        this.isPageLoaded = true;

        if (shouldLoadTeacherLayer) {
          let teacherLayerImgBytes = images[1];
          this.pageTeacherImage = this.sanitizer.bypassSecurityTrustUrl(
            this.PageReader.getPage(teacherLayerImgBytes, { type: 'image/jpeg' })
          );

          this.pageTeacherImageHeight = document.getElementById('page').getAttribute('height');
          // document.getElementById('pageTlayer').height = document.getElementById('page').height;
          document.getElementById('pageTlayer').hidden = false;
        }
        else {
          document.getElementById('pageTlayer').hidden = true;
        }

        this.pageMarker.setSelectedPage(this.bookLedId, this.pageNumber);
        setTimeout(() => {
            this.loader.hide();
        }, 50);

        this.hotspotManager.setCurrentPageId(pageId).then(() => {
          this.hotspotContainer.loadPageHotspots();
        }).catch((error) => {
          let errorStr = 'Erro carregando hotspots da página! (' + JSON.stringify(error) + ')';
        });

        console.log('page image(s) loaded in ' + (Date.now() - startTime) + 'ms');

      })
      .catch(error => {
        this.showToast('Erro ao carregar página! (' + JSON.stringify(error) + ')');
        this.loader.hide();
      });
  }

  private showPageProgress(status: string) {
    this.loader.show();
  }

  private isValidPage(pageNumber: number) {

    const bookInfo = this.installedBookManifest.book;
    const lastPageNumber = bookInfo.book_page_count;

    if (pageNumber < 1 || pageNumber > lastPageNumber || pageNumber == null) {
      return false;
    }
    else {
      return true;
    }
  }

  private showToast(message: string) {
    $('#toast-container')
      .find('div')
      .remove();

    Materialize.toast(
      message,
      3000
    );
  }
}
