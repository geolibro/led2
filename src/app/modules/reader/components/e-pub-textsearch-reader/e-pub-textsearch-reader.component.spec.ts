import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPubTextsearchReaderComponent } from './e-pub-textsearch-reader.component';

describe('EPubTextsearchReaderComponent', () => {
  let component: EPubTextsearchReaderComponent;
  let fixture: ComponentFixture<EPubTextsearchReaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPubTextsearchReaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPubTextsearchReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
