import { EventEmitter, Injectable } from "@angular/core";

import { ExternalAnnotationService } from "../../services/external-annotation.service";
import { EchoBookReaderContract } from "../../../contracts/echo-book-reader.contract";
import {LegacyBookContract} from "../../../contracts/legacy-book.contract";

@Injectable()
export class BookEvent {
    EchoData: EventEmitter<Array<any>>;
    EchoInstalledManifest: EventEmitter<Array<any>>;
    EchoBooksJsonAddress: EventEmitter<Array<any>>;
    EchoPages: EventEmitter<Array<any>>;
    EchoDiscipline: EventEmitter<Array<string>>;
    EchoProcessPagesBookThread: EventEmitter<Promise<any>>;
    EchoHideBar: EventEmitter<boolean>;
    EchoBookPage: EventEmitter<{ pageNumber: number; isRawNumber: boolean }>;
    EchoEnableHammerEvents: EventEmitter<boolean>;
    EchoExternalData: EventEmitter<string>;
    EchoExternalNotesCount: EventEmitter<number>;
    EchoExternalNotesPlugin: EventEmitter<ExternalAnnotationService>;
    EchoCurrentPage: EventEmitter<{ internal: number; readable: number }>;
    EchoBookList: EventEmitter<Array<any>>;
    EchoBookListEmitted: EventEmitter<Array<any>>;
    EchoBookSort: EventEmitter<string>;
    EchoBookFilterField: EventEmitter<Array<{ display: string; field: string; id: number; alias: string }>>;
    EchoBookDeleted: EventEmitter<string>;
    EchoBookReader: EventEmitter<EchoBookReaderContract>;
    EchoFormatText: EventEmitter<any>;
    EchoFormatTextBookSlider: EventEmitter<any>;
    EchoPageReader: EventEmitter<any>;
    EchoEpubPage: EventEmitter<any>;
    EchoCourtain: EventEmitter<any>;
    EchoCourtainModal: EventEmitter<any>;
    EchoCourtainOnSlide: EventEmitter<any>;
    EchoEnableScribble: EventEmitter<boolean>;
    EchoCanvasSettings: EventEmitter<string>;
    EchoStartUserHotspot: EventEmitter<any>;
    EchoAriaHiddenModal: EventEmitter<any>;
    EchoChangeSearch: EventEmitter<any>;
    EchoKillSearch: EventEmitter<any>;
    EchoToggleTeacherLayer: EventEmitter<boolean>;
    EchoTeacherLayerAvailable: EventEmitter<boolean>;
    EchoAudioPlayer: EventEmitter<any>;
    EchoKillAudio: EventEmitter<boolean>;
    EchoKillAudioWithoutControl: EventEmitter<boolean>;
    EchoAudioPlayerRunning: EventEmitter<boolean>;
    EchoAnnotationsUpdated: EventEmitter<boolean>;
    EchoOpenHotspot: EventEmitter<any>;
    EchoShowWordHotspot: EventEmitter<Array<any>>;
    EchoUpdateBookmarkSummary: EventEmitter<boolean>;
    EchoLoadOEDFromSummary: EventEmitter<{page: number, oed_id: number}>;
    EchoOpenVideoHotspot: EventEmitter<string>;
    EchoKillVideoHotspot: EventEmitter<boolean>;
    EchoResizeEpubWindow: EventEmitter<{}>;
    EchoClickEraser: EventEmitter<boolean>;
    constructor() {
        this.EchoData = new EventEmitter<Array<any>>();
        this.EchoInstalledManifest = new EventEmitter<Array<any>>();
        this.EchoBooksJsonAddress = new EventEmitter<Array<any>>();
        this.EchoPages = new EventEmitter<Array<any>>();
        this.EchoDiscipline = new EventEmitter<Array<string>>();
        this.EchoProcessPagesBookThread = new EventEmitter<Promise<any>>();
        this.EchoHideBar = new EventEmitter<boolean>();
        this.EchoBookPage = new EventEmitter<{ pageNumber: number; isRawNumber: boolean }>();
        this.EchoEnableHammerEvents = new EventEmitter<boolean>();
        this.EchoExternalData = new EventEmitter<string>();
        this.EchoExternalNotesCount = new EventEmitter<number>();
        this.EchoExternalNotesPlugin = new EventEmitter<ExternalAnnotationService>();
        this.EchoCurrentPage = new EventEmitter<{ internal: number; readable: number }>();
        this.EchoBookList = new EventEmitter<Array<any>>();
        this.EchoBookListEmitted = new EventEmitter<Array<any>>();
        this.EchoBookSort = new EventEmitter<string>();
        this.EchoBookFilterField = new EventEmitter<Array<{ display: string; field: string; id: number; alias: string }>>();
        this.EchoBookDeleted = new EventEmitter<string>();
        this.EchoBookReader = new EventEmitter<EchoBookReaderContract>();
        this.EchoFormatText = new EventEmitter();
        this.EchoFormatTextBookSlider = new EventEmitter();
        this.EchoPageReader = new EventEmitter();
        this.EchoEpubPage = new EventEmitter();
        this.EchoCourtain = new EventEmitter();
        this.EchoCourtainModal = new EventEmitter();
        this.EchoCourtainOnSlide = new EventEmitter();
        this.EchoEnableScribble = new EventEmitter<boolean>();
        this.EchoCanvasSettings = new EventEmitter<string>();
        this.EchoStartUserHotspot = new EventEmitter();
        this.EchoAriaHiddenModal = new EventEmitter();
        this.EchoChangeSearch = new EventEmitter();
        this.EchoToggleTeacherLayer = new EventEmitter<boolean>();
        this.EchoKillSearch = new EventEmitter<boolean>();
        this.EchoTeacherLayerAvailable = new EventEmitter<boolean>();
        this.EchoAudioPlayer = new EventEmitter<any>();
        this.EchoKillAudio = new EventEmitter<boolean>();
        this.EchoKillAudioWithoutControl = new EventEmitter<boolean>();
        this.EchoAudioPlayerRunning = new EventEmitter<boolean>();
        this.EchoAnnotationsUpdated = new EventEmitter<boolean>();
        this.EchoOpenHotspot = new EventEmitter<any>();
        this.EchoShowWordHotspot = new EventEmitter<Array<any>>();
        this.EchoUpdateBookmarkSummary = new EventEmitter<boolean>();
        this.EchoLoadOEDFromSummary = new EventEmitter<{page: number, oed_id: number}>();
        this.EchoOpenVideoHotspot = new EventEmitter<string>();
        this.EchoKillVideoHotspot = new EventEmitter<boolean>();
        this.EchoResizeEpubWindow = new EventEmitter<{}>();
        this.EchoClickEraser = new EventEmitter<boolean>();


    }
}
