import { Injectable, EventEmitter } from '@angular/core';
import { DownloadableObject } from '../services/content-manager/downloadable-object';
import { DownloadStopReason } from '../services/content-manager/download-manager.service';
@Injectable()
export class AppEvent {
    EchoVisibleComponent: EventEmitter<{ toolbar: boolean, navbar: boolean }>;
    EchoSearch: EventEmitter<{ search: string }>;
    EchoIntentActivity: EventEmitter<any>;
    EchoProgress: EventEmitter<{ book_id: string, book_status: string, book_progress: number }>;
    EchoDownloadObjectFinish: EventEmitter<DownloadableObject>;
    EchoDownloadObjectStart: EventEmitter<DownloadableObject>;
    EchoAriaHidden: EventEmitter<boolean>;
    EchoShowManagementSpace: EventEmitter<boolean>;
    EchoSpaceRequestForObject: EventEmitter<DownloadableObject>;
    EchoSettingsReady: EventEmitter<any>;
    EchoDownloadStalled: EventEmitter<DownloadStopReason>
    constructor() {
        this.EchoVisibleComponent = new EventEmitter<{ toolbar: boolean, navbar: boolean }>();
        this.EchoSearch = new EventEmitter<{ search: string }>();
        this.EchoIntentActivity = new EventEmitter<any>();
        this.EchoProgress = new EventEmitter<{ book_id: string, book_status: string, book_progress: number }>();
        this.EchoDownloadObjectFinish = new EventEmitter<DownloadableObject>();
        this.EchoDownloadObjectStart = new EventEmitter<DownloadableObject>();
        this.EchoAriaHidden = new EventEmitter<boolean>();
        this.EchoShowManagementSpace = new EventEmitter<boolean>();
        this.EchoSpaceRequestForObject = new EventEmitter<DownloadableObject>();
        this.EchoSettingsReady = new EventEmitter<any>();
        this.EchoDownloadStalled = new EventEmitter<DownloadStopReason>();
    }
}
