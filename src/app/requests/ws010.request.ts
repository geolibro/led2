import { DefaultContractRequest } from './contracts/default.contract.request';
import { BookService } from '../modules/services/book.service';
import { Ws010Contract } from './contracts/ws/ws010.contract';
import * as CryptoJS from 'crypto-js';

export class Ws010Request implements DefaultContractRequest {
  constructor(private request: Ws010Contract, private service?: BookService) {
    // this.decryptUserPassword();
  }

  getHttpRequest(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service
        .notifyBookUninstallation(this.request)
        .then((httpResult: any) => {
          if (httpResult.error_code !== '' && httpResult.error_code !== '0') {
            return reject(httpResult.error_msg);
          }

          return resolve(httpResult);
        })
        .catch((error: any) => {
          return reject(error.message);
        });
    });
  }

  // private decryptUserPassword() {
  //   if (this.request.hasOwnProperty('password')) {
  //     const passwordBytes = CryptoJS.AES.decrypt(
  //       this.request.password,
  //       SessionProvider.getUserCurrentSession().led_user_id
  //     );
  //     const password = passwordBytes.toString(CryptoJS.enc.Utf8);

  //     this.request.password = password;
  //   }
  // }
}
