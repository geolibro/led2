import { DeviceFieldsContract } from './../../../contracts/device-fields.contract';
import { UserAccessFieldsContract } from './../../../contracts/user-access-fields.contract';
import { OperationFieldsContract } from '../../../contracts/operation-fields.contract';
export interface Ws102Contract
  extends OperationFieldsContract,
    UserAccessFieldsContract,
    DeviceFieldsContract {
  book_license_type: string;
  book_led_id: string;
  license: string;
}
