export interface Ws021Contract {
  operation?: string;
  user_app_token: string;
  app_token: string;
  book_publisher_id: string;
  qr_code?: string;
}
