import { OperationFieldsContract } from '../../../contracts/operation-fields.contract';
import { DeviceFieldsContract } from './../../../contracts/device-fields.contract';
import { UserAccessFieldsContract } from './../../../contracts/user-access-fields.contract';

export interface Ws007Contract
  extends OperationFieldsContract,
    UserAccessFieldsContract,
    DeviceFieldsContract {
  supported_book_formats: string[];
}
