export interface EchoBookReaderContract {
    book_title: string;
    summary?: Array<any>;
    bookId: string;
    bookType: string;
}
