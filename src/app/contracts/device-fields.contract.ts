export interface DeviceFieldsContract {
  device_id: string;
  device_name?: string;
  device_info: [{
    os_name: string;
    os_version: string;
    model: string;
    screen_width: number;
    screen_height: number;
  }]
}
