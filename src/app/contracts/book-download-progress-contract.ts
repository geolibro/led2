export interface BookDownloadProgressContract {
    [propName: string]: number
}
