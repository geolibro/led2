import { DeviceFieldsContract } from './device-fields.contract';
import { RememberMeContract } from './rember-me.contract';
import { UserAccessFieldsContract } from './user-access-fields.contract';
import { OperationFieldsContract } from './operation-fields.contract';
export interface UserLoginFieldsContract
  extends OperationFieldsContract,
    UserAccessFieldsContract,
    RememberMeContract,
    DeviceFieldsContract {}
