import { UserAccessFieldsContract } from './user-access-fields.contract';
import { RememberMeContract } from './rember-me.contract';
import { UserMemoryFieldsContract } from "./user-memory-fields.contract";
export interface UserLoggedFieldsContract extends UserAccessFieldsContract, RememberMeContract, UserMemoryFieldsContract {
  led_user_id: string;
  user_name: string;
  token: string;
  user_photo_url: string;
  device_id: string;
  avatar?: string;
  book_key?: string;
}
