export interface ModalContract {
    id: string;
    title: string;
    show()
    hide()
}
