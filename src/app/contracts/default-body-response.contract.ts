import { LegacyBookContract } from "./legacy-book.contract";

export interface DefaultBodyResponseContract {
  error_code: string,
  error_msg: string,
  books?: LegacyBookContract[]
}
