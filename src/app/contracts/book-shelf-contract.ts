export interface BookShelfStatusContract {
    [bookLedId: string]: string;
}
