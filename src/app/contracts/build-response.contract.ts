export interface BuildResponseContract {
    client:     string,
    version:    string,
    created_at: string,
    commit:     string
}