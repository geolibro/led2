export interface WebServiceListContract {
  name: string;
  path: string;
  server: string;
}
