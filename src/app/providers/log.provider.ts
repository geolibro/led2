export class LogProvider {
  private static LogObject: any = [];
  private static place: string;
  static Instance: LogProvider;

  static getInstance(place?: string) {
    LogProvider.place = place ? place : 'LogProvider';

    if (!LogProvider.Instance) {
      LogProvider.Instance = new LogProvider();
    }

    return LogProvider.Instance;
  }

  /**
   * Just print recent logs
   *
   * @returns {string}
   * @memberof LogProvider
   */
  print(): string {
    return JSON.stringify(LogProvider.LogObject);
  }

  /**
   * @param {*} input
   * @memberof LogProvider
   */
  save(input: any, identifier?: string): void {
    const dateTime: Date = new Date();
    const now: string = dateTime.toLocaleDateString();

    if (typeof input === 'object') {
      input = JSON.stringify(input);
    }

    LogProvider.LogObject.push({
      place: LogProvider.place,
      timestamp: now,
      input: input,
      identifier: identifier
    });

    // LedFileSystem.request()
    //   .then((dirEntry: DirectoryEntry) => {
    //     LedFileSystem.writer(
    //       '.logs.app.libro',
    //       dirEntry,
    //       LogProvider.LogObject,
    //       { type: 'application/json' },
    //       { create: true, exclusive: false }
    //     ).then(() => {}, () => {});
    //   })
    //   .catch((error: FileError) => {});
  }

  private constructor() {}
}
