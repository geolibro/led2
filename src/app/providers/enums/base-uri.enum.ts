export enum BaseUriEnum {
  APPLE_STORE,
  ANDROID_STORE,
  ALFACON_NOTES
}

export class BaseUri {
  static readonly ENUMs: Array<string> = ['itms-apps://', 'market://', 'alfaconnotes://'];

  static getEnum(index: number): string {
    return this.ENUMs[index];
  }

  private constructor() {}
}
